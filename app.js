var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
require('dotenv').config('./.env');
var adminRouter = require('./api/admin/admin');
var usersRouter = require('./api/users/users');
var paypal = require('./api/lib/paypal')

const { logger } = require('./config/logger')
//database
const db = require('./config/database')
var membershipExpiration = require("./api/lib/schedule");

// Test DB
db.authenticate()
  .then(() => {
    logger.info('Database connected successfully');
    console.log("Database connected..")
  })
  .catch((err) => {
    console.log("Error: " + err)
    logger.error('Getting error when connecting database');
  })

var cors = require('cors')
var app = express();

app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(function (req, res, next) {
  res.header("Cache-Control", "no-cache, no-store, must-revalidate");
  res.header("Pragma", "no-cache");
  next();
})
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads/profilePicture')));
app.use(express.static(path.join(__dirname, 'uploads/items')));
app.use(express.static(path.join(__dirname, 'uploads/showroom')));

app.use('/profile_picture', express.static('uploads/profilePicture'))
app.use('/items', express.static('uploads/items'))
app.use('/showroom', express.static('uploads/showroom'))

app.use('/admin', adminRouter);
app.use('/user', usersRouter);
app.get('/', function (req, res, next) {
  res.render('welcome', { name: "dwafes", Link: "dwafgs" })
})

app.use('/my-api', paypal);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
