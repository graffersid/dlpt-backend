var express = require('express');
var router = express.Router();
var userAuth = require('./routes/userAuth')
var userDetails = require('./routes/userDetails')
var items = require('./routes/item')
var showroom = require('./routes/showroom')
var like = require('./routes/like')
var follower = require('./routes/follower')
var category = require('./routes/category')
var ranking = require('./routes/ranking')
var membership = require('./routes/membership')

// ->  User Authentication
router.use('/auth', userAuth);

// ->  User Details
router.use('/details', userDetails)

// ->  items
router.use('/item', items)

// ->  showrooms
router.use('/showroom', showroom)

// -> Like
router.use('/like', like)

// -> Follower
router.use('/follower', follower)

// -> category
router.use('/category', category)

// -> category
router.use('/ranking', ranking)

// -> Like
router.use('/membership', membership)


module.exports = router;