const { validationResult } = require('express-validator');
var jwt = require('../../lib/jwt');
const { logger } = require("../../../config/logger");
const rankingService = require('../../services/rankingService')

exports.getRanking = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/ranking/get_ranking -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    rankingService.getRanking(req.query, userId)
        .then(rankingDetails => {
            if (rankingDetails !== null) {
                res.status(200).send({ rankingDetails: rankingDetails, status: true, message: "Ranking details Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the Ranking details." });
            }
        }).catch((error) => {
            logger.error('user/ranking/get_ranking -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });

}