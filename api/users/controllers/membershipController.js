const { validationResult } = require('express-validator');
var jwt = require('../../lib/jwt');
const { logger } = require("../../../config/logger");
const membershipDetailsService = require('../../services/membershipDetailsService')

exports.getMembershipLists = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/membership/get_membership_lists -> GET API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    membershipDetailsService.getAllMemberships(req.query)
        .then((memberships) => {
            res.status(200).send({ memberships, status: true, message: "Memberships list fetch successfully." });
        }).catch((error) => {
            logger.error('user/membership/get_membership_lists -> GET API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getCurrentMembership = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/membership/get_membership -> GET API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    membershipDetailsService.userCurrentMembership(userId)
        .then((membershipDetails) => {
            res.status(200).send({ membershipDetails, status: true, message: "Current membership fetch successfully." });
        }).catch((error) => {
            logger.error('user/membership/get_membership -> GET API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getUserMembershipHistory = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/membership/get_membership_history -> GET API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    membershipDetailsService.getUserMembershipHistory(userId)
        .then((memberships) => {
            res.status(200).send({ memberships, status: true, message: "Membership history fetch successfully." });
        }).catch((error) => {
            logger.error('user/membership/get_membership_history -> GET API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// exports.addUserMembership = async (req, res, next) => {
//     let userId;
//     // -> request validation check
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         res.status(422).json({ errors: errors.array() });
//         return;
//     }

//     // -> Fetching userId from Access Token 
//     await jwt.verifyToken(req.headers['access-token']).then((result) => {
//         if (!result.userId) {
//             res.status(401).send({ status: false, message: "Unauthorized user" });
//         }
//         userId = result.userId;
//     }).catch((error) => {
//         logger.error('user/membership/add_user_membership -> GET API -> Error in fetching userId from token.');
//         res.status(500).send({ status: false, message: error })
//     });

//     membershipDetailsService.addUserMembership(req.body, userId)
//         .then((membershipDetails) => {
//             res.status(200).send({ membershipDetails, status: true, message: "like successfully." });
//         }).catch((error) => {
//             logger.error('user/membership/add_user_membership -> GET API -> Internal server error. ', { obj: error });
//             res.status(500).send({ status: false, message: error })
//         })
// }