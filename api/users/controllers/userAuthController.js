const { validationResult } = require('express-validator');
const crypto = require('crypto');

var jwt = require('../../lib/jwt');
var bcrypt = require('../../lib/bcrypt');
const { logger } = require("../../../config/logger");
//require service
const userDetailsService = require('../../services/userDetailsService')
const userAuthService = require('../../services/userAuthService');
const membershipDetailsService = require('../../services/membershipDetailsService');
const mailer = require("@sendgrid/mail");
mailer.setApiKey(`${process.env.SENDGRID_API_KEY}`);
let ejs = require('ejs');
var fs = require('fs');
var path = require('path');
var resetPasswordTemplate = fs.readFileSync(`${path.join(__dirname, "../../../", "views/email_template/resetPassword.ejs")}`, { encoding: 'utf8', flag: 'r' });
var compiledResetPasswordTemplate = ejs.compile(resetPasswordTemplate)
var welcomeTemplate = fs.readFileSync(`${path.join(__dirname, "../../../", "views/email_template/welcome.ejs")}`, { encoding: 'utf8', flag: 'r' });
var compiledWelcomeTemplate = ejs.compile(welcomeTemplate)
// -> Register User to Database
exports.registerUser = (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const { email, password, userName } = req.body

    const isUserEmailUnique = userDetailsService.isUserEmailUnique(email)
    // -> Check for Unique User name
    const isUsernameUnique = userDetailsService.isUsernameUnique(userName)

    Promise.all([isUserEmailUnique, isUsernameUnique]).then(async (values) => {
        if (values[0]) {
            res.status(400).send({ status: false, message: "Email is already in use." });
        } else if (values[1]) {
            res.status(400).send({ status: false, message: "UserName is already in use." });
        } else {
            let hashedpassword = await bcrypt.hashPassword(password)
            userAuthService.registerUser(email, hashedpassword, userName)
                .then(async result => {
                    if (result !== null) {
                        let usersCount = await userDetailsService.getUsersCount();
                        if (usersCount < 1000) {
                            // console.log("first thousand user", usersCount)
                            //giving basic membership to first 1000 users 
                            await membershipDetailsService.addUserMembership({ membershipId: 2 }, result.user_id)
                        } else {
                            //giving free membership to the users
                            // console.log("after first thousand user", usersCount)
                            await membershipDetailsService.addUserMembership({ membershipId: 1 }, result.user_id)
                        }
                        jwt.generateToken(result.user_id, result.is_admin).then((token) => {
                            userAuthService.setUserAccessToken(token, result.user_id)
                                .then(result => {
                                    welcomeMail(result.user_email, result.user_name).then((result2) => {
                                        res.status(200).send({ 'user_email': result.user_email, 'user_name': result.user_name, access_token: result.access_token, status: true, message: "User registered Successfully" });
                                        // res.status(200).send({ resetPasswordToken: result.reset_password_token, status: true, message: "Reset password link is generated successfully" });
                                    }).catch((error) => {
                                        logger.error('/user/auth/register -> Internal server error.', { obj: error });
                                        res.status(500).send({ status: false, message: error })
                                    })

                                }).catch((error) => {
                                    logger.error('/user/auth/register -> Internal server error.', { obj: error });
                                    res.status(500).send({ status: false, message: error })
                                })
                        });
                    }
                }).catch((error) => {
                    logger.error('/user/auth/register -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                });
        }
    }).catch((error) => {
        logger.error('/user/auth/register -> Internal server error.', { obj: error });
        res.status(500).send({ status: false, message: error })
    });
}

// -> Login User
exports.loginUser = async (req, res, next) => {
    // -> Request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).send({ status: false, errors: errors.array() });
        return;
    }
    const { email, password } = req.body
    userDetailsService.isUserEmailUnique(email)
        .then(user => {
            if (user !== null) {

                const { is_admin, is_profile_completed, user_id, user_password } = user;
                var passwordIsValid = bcrypt.comparePassword(user_password, password);//hashPassword,password
                if (passwordIsValid) {
                    jwt.generateToken(user_id, is_admin).then((token) => {
                        userAuthService.setUserAccessToken(token, user_id)
                            .then(result => {
                                if (!is_profile_completed) {
                                    res.status(403).send({ access_token: result.access_token, isAdmin: result.is_admin, status: false, error: "completeProfile", message: "User profile is incomplete" });
                                    return;
                                }
                                if (!result.is_active) {
                                    res.status(403).send({ status: false, error: "profileBlocked", message: "Your account is blocked by the admin" });
                                    return;
                                }
                                res.status(200).send({ 'is_active': result.is_active, 'user_image': result.user_image, 'user_email': result.user_email, 'user_name': result.user_name, access_token: result.access_token, isAdmin: result.is_admin, status: true, message: "Loggedin successfully" });
                            }).catch((error) => {
                                logger.error('/user/auth/login -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: error })
                            })
                    });
                } else {
                    res.status(401).send({ status: false, message: "Entered wrong password" });
                }
            } else {
                res.status(404).send({ status: false, message: "User doesn't exist" });
            }
        }).catch((error) => {
            logger.error('/user/auth/login -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Login User
exports.logout = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/profile -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userAuthService.logout(userId)
        .then(user => {
            if (user !== null) {
                res.status(200).send({ status: true, message: "Logout Successfully" });
            } else {
                res.status(401).send({ status: false, message: "Something went wrong." });
            }
        }).catch((error) => {
            logger.error('/user/auth/login -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Forgot Password
exports.forgotPassword = async (req, res, next) => {
    logger.info('/user/auth/forgot_password', { obj: req.body });
    // -> Request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    const { email } = req.body

    userDetailsService.isUserEmailUnique(email)
        .then(async user => {
            if (user !== null) {
                const { user_id, name } = user;
                const buf = crypto.randomBytes(20);
                var token = buf.toString('hex');
                console.log(token)
                let tokenExpiryTime = await userAuthService.setTokenExpiryTime();
                userAuthService.setResetPasswordTokenAndTime(token, tokenExpiryTime, user_id)
                    .then(async result => {
                        if (result !== null) {
                            forgotPasswordMail(result.user_email, result.reset_password_token, name).then((result2) => {
                                res.status(200).send({ status: true, message: "You will receive reset password link on your registered email account" });
                                // res.status(200).send({ resetPasswordToken: result.reset_password_token, status: true, message: "Reset password link is generated successfully" });
                            }).catch((error) => {
                                logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
                                res.status(500).send({ status: false, message: "Something went wrong, Internal server error." })
                            })
                        }
                    }).catch((error) => {
                        logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })

            } else {
                res.status(404).send({ status: false, message: "Email doesn't exist" });
            }

        }).catch((error) => {
            logger.error('/user/auth/forgot_password -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Reset Password
exports.resetPassword = async (req, res, next) => {
    //  -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).send({ status: false, errors: errors.array() });
        return;
    }

    let date = await userAuthService.getDate();
    const { token, password } = req.body;
    userAuthService.checkTokenExpiryTime(token, date)
        .then(async user => {
            if (user !== null) {
                const { user_id } = user
                let hashedpassword = await bcrypt.hashPassword(password)
                userAuthService.resetPassword(hashedpassword, user_id)
                    .then(result => {
                        res.status(200).send({ status: true, message: "Password reset successfully" });
                    }).catch((error) => {
                        logger.error('/user/auth/reset_password -> Internal server error.', { obj: error });
                        res.status(500).send({ status: false, message: error })
                    })
            } else {
                res.status(400).send({ status: false, message: 'Reset password link is invalid or has expired.' });
            }

        }).catch((error) => {
            logger.error('/user/auth/reset_password -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}


forgotPasswordMail = async (email, token, name) => {
    return new Promise(async (resolve, reject) => {
        try {
            // Setting configurations 
            const msg = {
                to: `${email}`,
                from: {
                    "email": `${process.env.MAILER_EMAIL_ID}`,
                    "name": "ShowTrove"
                },
                subject: "Reset Password Link",
                html: compiledResetPasswordTemplate({ firstname: `${name}`, Link: `http://showtrove.com/home/reset_password/${token}` })
            };

            // Sending mail 
            mailer.send(msg, function (err, json) {
                if (err) {
                    console.log(err);
                    reject(err)
                } else {
                    resolve()
                }
            });

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

welcomeMail = async (email, name) => {
    return new Promise(async (resolve, reject) => {
        try {
            // Setting configurations 
            const msg = {
                to: `${email}`,
                from: {
                    "email": `${process.env.MAILER_EMAIL_ID}`,
                    "name": "ShowTrove"
                },
                subject: "Welcome to ShowTrove",
                html: compiledWelcomeTemplate({ 'name': `${name}`, Link: `http://showtrove.com/home` })
            };

            // Sending mail 
            mailer.send(msg, function (err, json) {
                if (err) {
                    console.log(err);
                    reject(err)
                } else {
                    resolve()
                }
            });

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}
