const { validationResult } = require('express-validator');
var jwt = require('../../lib/jwt');
const { logger } = require("../../../config/logger");
const likeService = require('../../services/likeService')


// -> like showroom posts
exports.likeShowroom = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/like/like_showroom -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    likeService.likeShowroom(req.body, userId)
        .then((like) => {
            res.status(200).send({ like, status: true, message: "like successfully." });
        }).catch((error) => {
            logger.error('user/like/like_showroom -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> like items
exports.likeItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/like/like_item -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    likeService.likeItem(req.body, userId)
        .then((like) => {
            res.status(200).send({ like, status: true, message: "like successfully." });
        }).catch((error) => {
            logger.error('user/like/like_item -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> unlike item and showroom post
exports.unLike = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/like/:likeId -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    likeService.unLike(req.params)
        .then((like) => {
            if (like > 0) {
                res.status(200).send({ status: true, message: "Unlike successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to unlike." });
            }
        }).catch((error) => {
            logger.error('user/like/:likeId -> Delete API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}