const { validationResult } = require('express-validator');
const crypto = require('crypto');

var formidable = require('formidable');
var path = require('path');
const fs = require('fs');

var jwt = require('../../lib/jwt');
var bcrypt = require('../../lib/bcrypt');
const { logger } = require("../../../config/logger");

const showroomDetailsService = require('../../services/showroomDetailsService')
const membershipDetailsService = require('../../services/membershipDetailsService')
const mediaDetailsService = require('../../services/mediaDetailsService')

//Get Dashboards Showroom Posts
exports.getShowrooms = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/showroom/get_showrooms -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    showroomDetailsService.getShowrooms(userId, req.query)
        .then((showrooms) => {
            res.status(200).send({ 'showrooms': showrooms.showrooms, 'skipNotFollowedUsersShowrooms': showrooms.skipNotFollowedUsersShowrooms, status: true, message: "showrooms fetched successfully." });
        }).catch((error) => {
            logger.error('user/showroom/get_showrooms -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

//Get showroom post details 
exports.getShowroomDetails = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/get_showroom_details -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    showroomDetailsService.getShowroomDetails(req.params, userId)
        .then(showroomDetails => {
            if (showroomDetails !== null) {
                res.status(200).send({ showroomDetails: showroomDetails, status: true, message: "Showroom post details fetched successfully." });
            } else {
                res.status(400).send({ showroomDetails: {}, status: false, message: "Showroom post not Found" });
            }
        }).catch((error) => {
            logger.error('/user/showroom/get_showroom_details -> Get API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

//Add showroom post 
exports.addShowroom = async (req, res, next) => {
    let userId;

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/add_showroom -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let userMembership = await membershipDetailsService.userCurrentMembership(userId);
    let userShowroomCount = await showroomDetailsService.getUserShowroomCount({ 'userId': userId })

    if (userShowroomCount >= userMembership.total_post_showroom) {
        res.status(400).json({ status: false, message: 'Showroom post limit exceed' });
    }

    // -> Uploading Image
    // var form = new formidable.IncomingForm({ multiples: true });
    var files = [];
    var filesName = [];
    var title = "";
    var description = "";

    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    // form.maxFileSize = 3000 * 1024 * 1024;

    form.on('field', function (field, value) {
        if (field == 'title') {
            title = value;
        }
        if (field == 'description') {
            description = value;
        }
    });
    form.on('file', async function (field, file) {
        if (file.name) {
            // console.log("file                ", file)
            let fileType = file.type
            fileType = fileType.split("/", 1)[0]
            if (fileType) {
                if (!(fileType == "image" || fileType == "video")) {
                    res.status(422).json({ status: false, message: 'fileType should be image or video.' });
                    return;
                }
            } else {
                res.status(422).json({ status: false, message: 'Invalid file' });
                return;
            }

            function isImage(fileType) {
                switch (fileType) {
                    case 'image/jpeg':
                    case 'image/gif':
                    case 'image/bmp':
                    case 'image/png':
                    case 'image/x-icon':
                        //etc
                        return true;
                }
                return false;
            }

            function isVideo(fileType) {
                switch (fileType) {
                    case 'video/m4v':
                    case 'video/avi':
                    case 'video/mpg':
                    case 'video/mp4':
                    case 'video/x-matroska':
                        // etc
                        return true;
                }
                return false;
            }
            if (isVideo(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.showroom_video_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload video, max size is ${userMembership.showroom_video_size_limit} MB` });
                }
                files.push({ "file": file, "fileType": fileType })
            }
            if (isImage(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.showroom_image_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload image, max size is ${userMembership.showroom_image_size_limit} MB` });
                }
                files.push({ "file": file, "fileType": fileType })
            }
        }
    });
    form.on('end', async function () {
        if (!title) {
            res.status(422).json({ status: false, message: 'Enter the title.' });
            return;
        }

        if (!description) {
            res.status(422).json({ status: false, message: 'Enter the description.' });
            return;
        }

        if (!(files.length > 0)) {
            res.status(422).json({ status: false, message: 'Upload an image or video.' });
            return;
        }

        await asyncForEach(files, async function (file, index) {
            return new Promise(async (resolve, reject) => {
                var oldpath = file.file.path;
                var extension = path.extname(file.file.name);
                var File = path.basename(file.file.name, extension);
                File = File + '-' + Date.now() + extension;
                filesName.push({ 'media_url': File, 'file_type': file.fileType })
                var newpath = `${path.join(__dirname, "../../../", "uploads/showroom")}/${File}`;
                fs.readFile(oldpath, (err, data) => {
                    fs.writeFile(newpath, data, function (err) {
                        if (err) {
                            logger.error('/admin/items/add_item_media -> Something went wrong!', { obj: err });
                            res.status(500).send({ status: false, message: "Something went wrong!" });
                        }
                    });
                });
                resolve();
            })
        })
        showroomDetailsService.addShowroom({ title, description }, filesName, userId)
            .then(showroomDetails => {
                if (showroomDetails !== null) {
                    res.status(200).send({ showroomDetails: showroomDetails, status: true, message: "Showroom post added successfully." });
                } else {
                    res.status(400).send({ showroomDetails: {}, status: false, message: "Showroom post is not added" });
                }
            }).catch((error) => {
                logger.error('/user/showroom/add_showroom -> Put API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
    form.parse(req);
}

//Edit showroom post 
exports.editShowroom = async (req, res, next) => {
    let userId;
    const { showroomId } = req.body
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/edit_showroom -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let showroomdetails = await showroomDetailsService.getShowroomDetails({ 'showroomId': showroomId })
    if (showroomdetails !== null) {
        if (showroomdetails.user_id !== userId) {
            res.status(400).send({ status: false, message: "Can't edit other user showroom post" });
        }
    } else {
        res.status(400).send({ status: false, message: "Showroom post not found" });
    }

    showroomDetailsService.editShowroom(req.body)
        .then(showroomDetails => {
            if (showroomDetails === 1) {
                res.status(200).send({ status: true, message: "Showroom post updated successfully." });
            } else {
                res.status(400).send({ showroomDetails: {}, status: false, message: "Can't update showroom post" });
            }
        }).catch((error) => {
            logger.error('/user/showroom/edit_showroom -> Put API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

//Pin showroom post 
exports.pinShowroom = async (req, res, next) => {
    let userId;
    const { showroomId } = req.body
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/pin_showroom -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let showroomdetails = await showroomDetailsService.getShowroomDetails({ 'showroomId': showroomId })
    if (showroomdetails !== null) {
        if (showroomdetails.user_id !== userId) {
            res.status(400).send({ status: false, message: "Can't pin other user showroom post" });
        }
    } else {
        res.status(400).send({ status: false, message: "Showroom post not found" });
    }

    showroomDetailsService.pinShowroom(req.body)
        .then(showroomDetails => {
            if (showroomDetails === 1) {
                res.status(200).send({ status: true, message: "Showroom post pinned successfully." });
            } else {
                res.status(400).send({ status: false, message: "Can't pinned showroom post" });
            }
        }).catch((error) => {
            logger.error('/user/showroom/pin_showroom -> Patch API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

//Pin showroom post 
exports.unpinShowroom = async (req, res, next) => {
    let userId;
    const { showroomId } = req.body
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/unpin_showroom -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let showroomdetails = await showroomDetailsService.getShowroomDetails({ 'showroomId': showroomId })
    if (showroomdetails !== null) {
        if (showroomdetails.user_id !== userId) {
            res.status(400).send({ status: false, message: "Can't unpin other user showroom post" });
        }
    } else {
        res.status(400).send({ status: false, message: "Showroom post not found" });
    }

    showroomDetailsService.unpinShowroom(req.body)
        .then(showroomDetails => {
            if (showroomDetails === 1) {
                res.status(200).send({ status: true, message: "Showroom post unpinned successfully." });
            } else {
                res.status(400).send({ showroomDetails: {}, status: false, message: "Can't unpinned showroom post" });
            }
        }).catch((error) => {
            logger.error('/user/showroom/unpin_showroom -> Patch API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

//delete showroom post 
exports.deleteShowroom = async (req, res, next) => {
    let userId;
    const { showroomId } = req.params
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/delete_showroom -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    let showroomdetails = await showroomDetailsService.getShowroomDetails({ 'showroomId': showroomId })
    if (showroomdetails !== null) {
        if (showroomdetails.user_id !== userId) {
            res.status(400).send({ status: false, message: "Can't delete other user showroom post" });
        }
    } else {
        res.status(400).send({ status: false, message: "Showroom post not found" });
    }

    showroomDetailsService.deleteShowroom(req.params)
        .then(showroomDetails => {
            if (showroomDetails === 1) {
                res.status(200).send({ status: true, message: "Showroom post deleted successfully." });
            } else {
                res.status(400).send({ showroomDetails: {}, status: false, message: "Can't delete showroom post" });
            }
        }).catch((error) => {
            logger.error('/user/showroom/delete_showroom -> Put API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

//add showroom media
exports.addShowroomMedia = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/add_showroom_media -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    let userMembership = await membershipDetailsService.userCurrentMembership(userId);
    let userShowroomCount = await showroomDetailsService.getUserShowroomCount({ 'userId': userId })

    if (userShowroomCount >= userMembership.total_post_showroom) {
        res.status(400).json({ status: false, message: 'Showroom post limit exceed' });
    }

    // -> Uploading Image
    // var form = new formidable.IncomingForm({ multiples: true });
    var files = [];
    var filesName = [];
    var showroomId = "";

    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    // form.maxFileSize = 3000 * 1024 * 1024;

    form.on('field', function (field, value) {
        if (field == 'showroomId') {
            showroomId = value;
            console.log(value)
        }
    });
    form.on('file', async function (field, file) {
        if (file.name) {
            // console.log("file                ", file)
            let fileType = file.type
            fileType = fileType.split("/", 1)[0]
            if (fileType) {
                if (!(fileType == "image" || fileType == "video")) {
                    res.status(422).json({ status: false, message: 'fileType should be image or video.' });
                    return;
                }
            } else {
                res.status(422).json({ status: false, message: 'Invalid file' });
                return;
            }

            function isImage(fileType) {
                switch (fileType) {
                    case 'image/jpeg':
                    case 'image/gif':
                    case 'image/bmp':
                    case 'image/png':
                    case 'image/x-icon':
                        //etc
                        return true;
                }
                return false;
            }

            function isVideo(fileType) {
                switch (fileType) {
                    case 'video/m4v':
                    case 'video/avi':
                    case 'video/mpg':
                    case 'video/mp4':
                    case 'video/x-matroska':
                        // etc
                        return true;
                }
                return false;
            }
            if (isVideo(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.showroom_video_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload video, max size is ${userMembership.showroom_video_size_limit} MB` });
                }
                files.push({ "file": file, "fileType": fileType })
            }
            if (isImage(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.showroom_image_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload image, max size is ${userMembership.showroom_image_size_limit} MB` });
                }
                files.push({ "file": file, "fileType": fileType })
            }
        }
    });
    form.on('end', async function () {
        let showroomdetails = await showroomDetailsService.getShowroomDetails({ 'showroomId': showroomId })
        if (showroomdetails !== null) {
            if (showroomdetails.user_id !== userId) {
                res.status(400).send({ status: false, message: "Can't add other user showroom post media" });
            }
        } else {
            res.status(400).send({ status: false, message: "Showroom post not found" });
        }

        if (!(files.length > 0)) {
            res.status(422).json({ status: false, message: 'Upload an image or video.' });
            return;
        }

        await asyncForEach(files, async function (file, index) {
            return new Promise(async (resolve, reject) => {
                var oldpath = file.file.path;
                var extension = path.extname(file.file.name);
                var File = path.basename(file.file.name, extension);
                File = File + '-' + Date.now() + extension;
                filesName.push({ "showroom_id": showroomId, 'media_url': File, 'file_type': file.fileType })
                var newpath = `${path.join(__dirname, "../../../", "uploads/showroom")}/${File}`;
                fs.readFile(oldpath, (err, data) => {
                    fs.writeFile(newpath, data, function (err) {
                        if (err) {
                            logger.error('/admin/items/add_showroom_media -> Something went wrong!', { obj: err });
                            res.status(500).send({ status: false, message: "Something went wrong!" });
                        }
                    });
                });
                resolve();
            })
        })
        mediaDetailsService.addMedia(filesName)
            .then(showroomDetails => {
                if (showroomDetails !== null) {
                    res.status(200).send({ showroomDetails: showroomDetails, status: true, message: "Showroom post media added successfully." });
                } else {
                    res.status(400).send({ showroomDetails: {}, status: false, message: "Showroom post media is not added" });
                }
            }).catch((error) => {
                logger.error('/user/showroom/add_showroom_media -> Put API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
    form.parse(req);

}

//Delete showroom media
exports.deleteShowroomMedia = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/showroom/add_showroom -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    let showroomdetails = await showroomDetailsService.getShowroomDetails(req.params)
    if (showroomdetails !== null) {
        if (showroomdetails.user_id === userId) {
            mediaDetailsService.deleteMedia(req.params)
                .then(deletedMedia => {
                    console.log(deletedMedia.result)
                    if (deletedMedia.result == 1) {
                        var filePath = `${path.join(__dirname, "../../../", "uploads/showroom")}/${deletedMedia.media.media_url}`;
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                res.status(404).send({ status: false, message: "Showroom media not found" });
                                console.error(err)
                                return
                            }
                            res.status(200).send({ status: true, message: "Showroom media deleted successfully" });
                        })
                    } else {
                        res.status(404).send({ status: false, message: "Showroom media not found" });
                    }
                }).catch((error) => {
                    logger.error('/user/showroom/delete_showroom_media -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                });
        } else {
            res.status(400).send({ status: false, message: "Can't delete other user showroom post media" });
        }
    } else {
        res.status(400).send({ status: false, message: "Showroom post not found" });
    }
}

exports.reportShowroom = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/showroom/report_showroom -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    showroomDetailsService.reportShowroom(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ status: true, message: "Showroom is reported successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable report the showroom." });
            }
        }).catch((error) => {
            logger.error('user/showroom/report_showroom -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.searchDashboardShowrooms = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/showroom/search_dashboard_showroom -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    showroomDetailsService.searchDashboardShowrooms(userId, req.query)
        .then((showrooms) => {
            res.status(200).send({ 'showrooms': showrooms.showrooms, status: true, message: "showrooms fetched successfully." });
        }).catch((error) => {
            logger.error('user/showroom/search_dashboard_showroom -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};