var formidable = require('formidable');
const { validationResult } = require('express-validator');
var path = require('path');
const fs = require('fs');
const jwt = require('../../lib/jwt');
var bcrypt = require('../../lib/bcrypt');
const { logger } = require("../../../config/logger");
//require service
const userDetailsService = require('../../services/userDetailsService')
const userAuthService = require('../../services/userAuthService')
const showroomDetailsService = require('../../services/showroomDetailsService');
const notificationService = require('../../services/notificationService')
const users = require('../../models/users');


// -> Edit User Profile Picture
exports.editProfilePicture = async (req, res, next) => {
    let userId;

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/details/edit_profile_picture -> Error in fetching userId from token', { obj: error });
        res.status(500).send({ status: false, message: error })
    });

    // -> Uploading Image
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if ("profilePicture" in files) {
            if (!files.profilePicture.name) {
                logger.error('/user/details/edit_profile_picture -> Upload the image');
                res.status(422).json({ status: false, message: 'Upload the image' });
                return;
            }
        } else {
            logger.error('/user/details/edit_profile_picture -> Upload the image');
            res.status(422).json({ status: false, message: 'Upload the image' });
            return;
        }
        var oldpath = files.profilePicture.path;
        var extension = path.extname(files.profilePicture.name);
        var file = path.basename(files.profilePicture.name, extension);
        file = file + '-' + Date.now() + extension;

        var newpath = `${path.join(__dirname, "../../../", "uploads/profilePicture")}/${file
            }`;
        var rawData = fs.readFileSync(oldpath)

        fs.writeFile(newpath, rawData, function (err) {
            if (err) {
                logger.error('/user/details/edit_profile_picture -> Something went wrong!', { obj: err });
                res.status(400).send({ status: false, message: "Something went wrong!" });
            }
            userDetailsService.updateUserImage(file, userId)
                .then(user => {
                    if (user !== null) {
                        res.status(200).send({ userDetails: user, status: true, message: "profile picture updated successfully" });
                    } else {
                        res.status(400).send({ status: false, message: "profile picture is not Updated" });
                    }
                }).catch((error) => {
                    logger.error('/user/details/edit_profile_picture -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        });
    })
}

// -> change user password
exports.changePassword = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/change_password -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    const { password } = req.body;
    let hashedpassword = await bcrypt.hashPassword(password)
    userAuthService.resetPassword(hashedpassword, userId)
        .then(result => {
            res.status(200).send({ status: true, message: "Password changed successfully" });
        }).catch((error) => {
            logger.error('/user/details/change_password ->Patch API -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> change user password
exports.searchUser = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/search_user/:email -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.searchUser(req.params)
        .then(users => {
            if (users !== null) {
                res.status(200).send({ 'users': users, status: true, message: "Fetched users successfully." });
            } else {
                res.status(400).send({ status: false, message: "unable to Fetched the users" });
            }
        }).catch((error) => {
            logger.error('user/details/profile -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

// -> Get User Profile Details
exports.getProfileDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    const verifyToken = jwt.verifyToken(req.headers['access-token'])

    Promise.all([verifyToken]).then((values) => {
        if (!values[0].userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        } else {
            userId = values[0].userId;

            userDetailsService.getUserProfileDetails(userId)
                .then(user => {
                    if (user !== null) {
                        res.status(200).send({ userDetails: user, status: true, message: "Fetched profile details successfully." });
                    } else {
                        res.status(400).send({ status: false, message: "unable to get the profile Details" });
                    }
                }).catch((error) => {
                    logger.error('user/details/profile -> Get API -> Internal server error. ', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        }
    }).catch((error) => {
        logger.error('user/details/profile -> Get API -> Internal server error. ', { obj: error });
        res.status(500).send({ status: false, message: error })
    })

}

// -> Get Guest User Profile Details
exports.getGuestUserProfileDetails = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/profile/:userId -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.getUserProfileDetails(req.params.userId, userId)
        .then(user => {
            if (user !== null) {
                res.status(200).send({ userDetails: user, status: true, message: "Fetched profile details successfully." });
            } else {
                res.status(400).send({ status: false, message: "unable to get the profile Details" });
            }
        }).catch((error) => {
            logger.error('user/details/profile/:userId -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}


// -> Set User Profile Details
exports.setProfileDetails = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    const { userName, name, dateOfBirth, email, gender, country, zipCode } = req.body;

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/profile -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    // -> Check for Unique User email
    const isUserEmailUnique = userDetailsService.isUserEmailUnique(email, userId)
    // -> Check for Unique User name
    const isUsernameUnique = userDetailsService.isUsernameUnique(userName, userId)

    Promise.all([isUserEmailUnique, isUsernameUnique]).then((values) => {
        if (values[0]) {
            res.status(400).send({ status: false, message: "Email is already in use." });
        } else if (values[1]) {
            res.status(400).send({ status: false, message: "UserName is already in use." });
        } else {
            // -> Saving the new details of user 
            userDetailsService.setUserProfileDetails(userId, req.body)
                .then(user => {
                    if (user !== null) {
                        res.status(200).send({ userDetails: user, status: true, message: "user profile updated successfully." });
                    } else {
                        res.status(400).send({ status: false, message: "user profile is not updated." });
                    }
                }).catch((error) => {
                    logger.error('user/details/profile -> Post API -> Internal server error. ', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        }
    }).catch((error) => {
        logger.error('user/details/profile -> Post API -> Internal server error. ', { obj: error });
        res.status(500).send({ status: false, message: error })
    })
}

exports.getInterests = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/interests -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.getInterests(userId)
        .then(userInterests => {
            if (userInterests !== null) {
                res.status(200).send({ userInterests: userInterests, status: true, message: "User interests fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetch user interests." });
            }
        }).catch((error) => {
            logger.error('user/details/interests -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.addInterests = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/interests -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.addInterests(userId, req.body)
        .then(userInterests => {
            if (userInterests !== null) {
                res.status(200).send({ userInterests: userInterests, status: true, message: "User interests added successfully." });
            } else {
                res.status(400).send({ status: false, message: "User interests is not added." });
            }
        }).catch((error) => {
            logger.error('user/details/interests -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.editInterests = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/interests -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
    });

    userDetailsService.editInterests(req.body)
        .then(userInterests => {
            if (userInterests == 1) {
                res.status(200).send({ userInterests: userInterests, status: true, message: "User interests updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "User interests is not updated." });
            }
        }).catch((error) => {
            logger.error('user/details/interests -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        })
}

exports.deleteInterests = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/interests -> delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
    });
    let deleteInterestsId = JSON.parse(req.query.interestId)

    userDetailsService.deleteInterests(deleteInterestsId)
        .then(userInterests => {
            if (userInterests > 0) {
                res.status(200).send({ deletedCount: userInterests, status: true, message: "User interests deleted successfully." });
            } else {
                res.status(400).send({ status: false, message: "User interests is not found." });
            }
        }).catch((error) => {
            logger.error('user/details/interests -> delete API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}



exports.getUserCurrentCollection = async (req, res, next) => {
    let userId;
    let data;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/get_user_current_collection -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    if ("userId" in req.params) {
        data = {
            "userId": req.params.userId,
            "itemPrivacy": false
        }
    } else {
        data = { userId };
    }

    userDetailsService.getUserCurrentCollection(data, userId, req.query)
        .then(userCurrentCollection => {
            if (userCurrentCollection !== null) {
                res.status(200).send({ userCurrentCollection: userCurrentCollection.items, status: true, message: "User current collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User current collection." });
            }
        }).catch((error) => {
            logger.error('user/details/get_user_current_collection -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getUserpastCollection = async (req, res, next) => {
    let userId;
    let data;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/get_user_past_collection -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    if ("userId" in req.params) {
        data = {
            "userId": req.params.userId,
            "itemPrivacy": false
        }
    } else {
        data = { userId };
    }
    userDetailsService.getUserpastCollection(data, userId, req.query)
        .then(userPastCollection => {
            if (userPastCollection !== null) {
                res.status(200).send({ userPastCollection: userPastCollection.items, status: true, message: "User past collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User past collection." });
            }
        }).catch((error) => {
            logger.error('user/details/get_user_past_collection -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getUserShowrooms = async (req, res, next) => {
    let userId;
    let data;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/get_user_showrooms -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    if ("userId" in req.params) {
        data = req.params;
    } else {
        data = { userId };
    }

    showroomDetailsService.getUserShowrooms(data, userId, req.query)
        .then(userShowrooms => {
            if (userShowrooms !== null) {
                res.status(200).send({ userShowrooms: userShowrooms.showrooms, status: true, message: "User showrooms fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User showrooms." });
            }
        }).catch((error) => {
            logger.error('user/details/get_user_showrooms -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getUserCollectionRequests = async (req, res, next) => {
    let userId;
    let data = {};
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/get_user_item_request -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    data = { "userId": userId };

    userDetailsService.getUserCollectionRequests(data)
        .then(userCollectionRequests => {
            if (userCollectionRequests !== null) {
                res.status(200).send({ userCollectionRequests: userCollectionRequests, status: true, message: "User current collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User current collection." });
            }
        }).catch((error) => {
            logger.error('/user/details/get_user_item_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.reportUser = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/report_user -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.reportUser(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ status: true, message: "User is reported successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable report the user." });
            }
        }).catch((error) => {
            logger.error('user/details/report_user -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.searchDashboardUser = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/search_dashboard_user -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    userDetailsService.searchDashboardUser(userId, req.query)
        .then((users) => {
            if (users !== null) {
                res.status(200).send({ "users": users, status: true, message: "User fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the user." });
            }
        }).catch((error) => {
            logger.error('user/details/search_dashboard_user -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getUserNotifications = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/get_user_notifications -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    notificationService.getUserNotification(userId)
        .then((notifications) => {
            if (notifications !== null) {
                res.status(200).send({ "notifications": notifications, status: true, message: "Notifications fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the notifications." });
            }
        }).catch((error) => {
            logger.error('user/details/get_user_notifications -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}