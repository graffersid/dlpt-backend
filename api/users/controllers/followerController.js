const { validationResult } = require('express-validator');
var jwt = require('../../lib/jwt');
const { logger } = require("../../../config/logger");
const followerService = require('../../services/followerService')

// -> Follow user
exports.followUser = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/follower/ -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    followerService.followUser(req.body, userId)
        .then((follower) => {
            res.status(200).send({ follower, status: true, message: "follow successfully." });
        }).catch((error) => {
            logger.error('user/follower/ -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Unfollow user
exports.unfollowUser = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/follower/:followerId -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    followerService.unfollowUser(req.params, userId)
        .then((follower) => {
            if (follower > 0) {
                res.status(200).send({ status: true, message: "Unfollow successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to Unfollow." });
            }
        }).catch((error) => {
            logger.error('user/follower/:followerId -> Delete API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}