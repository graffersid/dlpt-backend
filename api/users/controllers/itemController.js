const { validationResult } = require('express-validator');
const crypto = require('crypto');

var formidable = require('formidable');
var path = require('path');
const fs = require('fs');

var jwt = require('../../lib/jwt');
var bcrypt = require('../../lib/bcrypt');
const { logger } = require("../../../config/logger");

const itemDetailsService = require('../../services/itemDetailsService')
const membershipDetailsService = require('../../services/membershipDetailsService')
const mediaDetailsService = require('../../services/mediaDetailsService')

// get dashboard items
exports.getItems = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/get_items -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.getItems(userId, req.query)
        .then((items) => {
            res.status(200).send({ 'items': items.items, 'skipNotFollowedUsersItems': items.skipNotFollowedUsersItems, status: true, message: "Items fetched successfully." });
        }).catch((error) => {
            logger.error('user/item/get_items -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// Search dashboard items
exports.searchDashboardItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/search_dashboard_item -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.searchDashboardItem(userId, req.query)
        .then((items) => {
            res.status(200).send({ 'items': items.items, status: true, message: "Items fetched successfully." });
        }).catch((error) => {
            logger.error('user/items/search_dashboard_item -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> Get Item details
exports.getItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/details/profile -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.getItem(req.params, userId)
        .then((items) => {
            res.status(200).send({ 'items': items.items, status: true, message: "Items fetched successfully." });
        }).catch((error) => {
            logger.error('user/details/profile -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.searchItemsByName = async (req, res, next) => {

    itemDetailsService.searchItemsByName(req.params)
        .then(items => {
            if (items.length > 0) {
                res.status(200).send({ items: items, status: true, message: "Items Fetched successfully." });
            } else {
                res.status(200).send({ items: [], status: false, message: "Items not found." });
            }
        }).catch((error) => {
            logger.error('/admin/items/get_all_items -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

// -> add item
exports.addItem = async (req, res, next) => {

    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/item/add_item_media -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    let userMembership = await membershipDetailsService.userCurrentMembership(userId);
    if (userMembership == null) {
        res.status(400).json({ status: false, message: `User doesn't have any membership` });
    }
    let userItemCount = await itemDetailsService.getUserItemCount({ 'userId': userId })
    if (userItemCount >= userMembership.total_item_inventory) {
        res.status(400).json({ status: false, message: `Item limit exceed, can't add more item` });
    }
    // -> Uploading Image
    // var form = new formidable.IncomingForm({ multiples: true });
    var imageFiles = [];
    var videoFiles = [];
    var files = [];
    var filesName = [];

    var itemDetails = {};

    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    // form.maxFileSize = 3000 * 1024 * 1024;

    form.on('field', function (field, value) {
        if (field == 'itemName') {
            itemDetails.itemName = value;
        }
        if (field == 'itemDescription') {
            itemDetails.itemDescription = value;
        }
        if (field == 'userFieldValues') {
            itemDetails.userFieldValues = value;
        }
        if (field == 'itemFieldValues') {
            itemDetails.itemFieldValues = value;
        }
        if (field == 'makeItemPrivate') {
            itemDetails.makeItemPrivate = value;
        }
        if (field == 'categoryId') {
            itemDetails.categoryId = value;
        }
    });
    form.on('file', async function (field, file) {
        if (file.name) {
            // console.log("file                ", file)
            let fileType = file.type
            fileType = fileType.split("/", 1)[0]
            if (fileType) {
                if (!(fileType == "image" || fileType == "video")) {
                    res.status(422).json({ status: false, message: 'fileType should be image or video.' });
                    return;
                }
            } else {
                res.status(422).json({ status: false, message: 'Invalid file' });
                return;
            }

            function isImage(fileType) {
                switch (fileType) {
                    case 'image/jpeg':
                    case 'image/gif':
                    case 'image/bmp':
                    case 'image/png':
                    case 'image/x-icon':
                        //etc
                        return true;
                }
                return false;
            }

            function isVideo(fileType) {
                switch (fileType) {
                    case 'video/m4v':
                    case 'video/avi':
                    case 'video/mpg':
                    case 'video/mp4':
                    case 'video/x-matroska':
                        // etc
                        return true;
                }
                return false;
            }
            if (isVideo(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.inventory_video_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload video, max size is ${userMembership.inventory_video_size_limit} MB` });
                }

                videoFiles.push({ "file": file, "fileType": fileType })
            }
            if (isImage(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.inventory_image_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload image, max size is ${userMembership.inventory_image_size_limit} MB` });
                }
                imageFiles.push({ "file": file, "fileType": fileType })
            }
        }
    });
    form.on('end', async function () {
        if (!(imageFiles.length > 0 || videoFiles.length > 0)) {
            res.status(422).json({ status: false, message: 'Upload an image or video.' });
            return;
        }

        if (!(userMembership.pics_per_item_inventory >= imageFiles.length)) {
            res.status(400).json({ status: false, message: `Can upload only ${userMembership.pics_per_item_inventory} images per item` });
            return;
        }
        if (!(userMembership.videos_per_item_inventory >= videoFiles.length)) {
            res.status(400).json({ status: false, message: `Can upload only ${userMembership.videos_per_item_inventory} videos per item` });
            return;
        }

        await asyncForEach(imageFiles, async function (imageFile, index) {
            return new Promise(async (resolve, reject) => {
                files.push(imageFile)
                resolve();
            })
        })

        await asyncForEach(videoFiles, async function (videoFile, index) {
            return new Promise(async (resolve, reject) => {
                files.push(videoFile)
                resolve();
            })
        })


        await asyncForEach(files, async function (file, index) {
            return new Promise(async (resolve, reject) => {
                var oldpath = file.file.path;
                var extension = path.extname(file.file.name);
                var File = path.basename(file.file.name, extension);
                File = File + '-' + Date.now() + extension;
                filesName.push({ 'media_url': File, 'file_type': file.fileType })
                var newpath = `${path.join(__dirname, "../../../", "uploads/items")}/${File}`;
                fs.readFile(oldpath, (err, data) => {
                    fs.writeFile(newpath, data, function (err) {
                        if (err) {
                            logger.error('/user/item/add_item -> Something went wrong!', { obj: err });
                            res.status(500).send({ status: false, message: "Something went wrong!" });
                        }
                    });
                });
                resolve();
            })
        })
        itemDetailsService.addItem(itemDetails, userId, filesName)
            .then(itemDetails => {
                if (itemDetails !== null) {
                    res.status(200).send({ itemDetails: itemDetails, status: true, message: "item added successfully." });
                } else {
                    res.status(400).send({ status: false, message: "item is not added" });
                }
            }).catch((error) => {
                logger.error('/user/item/add_item -> Post API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
    form.parse(req);


}

// -> update item
exports.editItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/edit_item -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.editUserItem(req.body, userId)
        .then(itemDetails => {
            if ("currentUser" in itemDetails) {
                res.status(400).send({ status: false, message: "Can't edit other user item" });
            }
            if (itemDetails.status == 1) {
                res.status(200).send({ status: true, message: "Item updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Item not updated." });
            }
        }).catch((error) => {
            logger.error('user/item/edit_item -> Internal server error.');
            res.status(500).send({ status: false, message: error })
        });
}

// -> Delete user Items
exports.deleteUserItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/delete_user_item -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.deleteUserItem(req.body, userId)
        .then((items) => {
            if (items > 0) {
                res.status(200).send({ status: true, message: "Item deleted successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to deleted the item." });
            }
        }).catch((error) => {
            logger.error('user/item/delete_user_item -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> change Item Privacy
exports.changeItemPrivacy = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/change_item_privacy -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.changeItemPrivacy(req.body, userId)
        .then((items) => {
            if (items > 0) {
                res.status(200).send({ status: true, message: "Item privacy changed successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to change the item privacy." });
            }
        }).catch((error) => {
            logger.error('user/item/change_item_privacy -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> move item To Past Collection
exports.moveToPastCollection = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/move_to_past_collection -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.moveToPastCollection(req.body, userId)
        .then((items) => {
            if (items > 0) {
                res.status(200).send({ status: true, message: "Item move to past collection successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to move the item into past collection." });
            }
        }).catch((error) => {
            logger.error('user/item/move_to_past_collection -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// -> transfer Item
exports.transferItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/transfer_item -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.transferItem(req.body, userId)
        .then((transferRequest) => {
            if (transferRequest !== null) {
                if ("alreadyCreated" in transferRequest) {
                    res.status(400).send({ status: false, message: "already created item tranfer request" });
                    return;
                }
                res.status(200).send({ 'transferRequestDetails': transferRequest, status: true, message: "Item transfer request generated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Can't transfer this item" });
            }
        }).catch((error) => {
            logger.error('user/item/transfer_item -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

// accept items trasfer request
exports.acceptItemRequest = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/accept_item_request -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.acceptItemRequest(req.body, userId)
        .then((acceptItemRequest) => {
            if (acceptItemRequest !== null) {
                if ("cannotAccept" in acceptItemRequest) {
                    res.status(400).send({ status: false, message: "The item request is no longer active" });
                    return;
                }
                res.status(200).send({ status: true, message: "Item request is accepted successfully" });
            } else {
                res.status(400).send({ status: false, message: "Can't transfer this item" });
            }
        }).catch((error) => {
            logger.error('user/item/accept_item_request -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.addItemMedia = async (req, res, next) => {

    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/item/add_item_media -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    let userMembership = await membershipDetailsService.userCurrentMembership(userId);
    if (userMembership == null) {
        res.status(400).json({ status: false, message: `User doesn't have any membership` });
    }
    // -> Uploading Image
    // var form = new formidable.IncomingForm({ multiples: true });
    var imageFiles = [];
    var videoFiles = [];
    var files = [];
    var filesName = [];
    var itemId = "";

    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    // form.maxFileSize = 3000 * 1024 * 1024;

    form.on('field', function (field, value) {
        if (field == 'itemId') {
            itemId = value;
        }
    });
    form.on('file', async function (field, file) {
        if (file.name) {
            // console.log("file                ", file)
            let fileType = file.type
            fileType = fileType.split("/", 1)[0]
            if (fileType) {
                if (!(fileType == "image" || fileType == "video")) {
                    res.status(422).json({ status: false, message: 'fileType should be image or video.' });
                    return;
                }
            } else {
                res.status(422).json({ status: false, message: 'Invalid file' });
                return;
            }

            function isImage(fileType) {
                switch (fileType) {
                    case 'image/jpeg':
                    case 'image/gif':
                    case 'image/bmp':
                    case 'image/png':
                    case 'image/x-icon':
                        //etc
                        return true;
                }
                return false;
            }

            function isVideo(fileType) {
                switch (fileType) {
                    case 'video/m4v':
                    case 'video/avi':
                    case 'video/mpg':
                    case 'video/mp4':
                    case 'video/x-matroska':
                        // etc
                        return true;
                }
                return false;
            }
            if (isVideo(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.inventory_video_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload video, max size is ${userMembership.inventory_video_size_limit} MB` });
                }

                videoFiles.push({ "file": file, "fileType": fileType })
            }
            if (isImage(file.type)) {
                if (file.size / Math.pow(1024, 2) >= userMembership.inventory_image_size_limit) {
                    res.status(400).json({ status: false, message: `Can't upload image, max size is ${userMembership.inventory_image_size_limit} MB` });
                }
                imageFiles.push({ "file": file, "fileType": fileType })
            }
        }
    });
    form.on('end', async function () {
        let itemDetails = await itemDetailsService.checkItemCurrentUser({ 'itemId': itemId })
        if (itemDetails !== null) {
            if (itemDetails.current_user_id !== userId) {
                res.status(400).send({ status: false, message: "Can't add media to other user item post" });
            }
        } else {
            res.status(400).send({ status: false, message: "item not found" });
        }

        if (!(imageFiles.length > 0 || videoFiles.length > 0)) {
            res.status(422).json({ status: false, message: 'Upload an image or video.' });
            return;
        }
        let itemImageCount = await mediaDetailsService.getMediaCount({ item_id: itemId, file_type: "image" })
        let itemVideocount = await mediaDetailsService.getMediaCount({ item_id: itemId, file_type: "video" })


        if (!(userMembership.pics_per_item_inventory >= imageFiles.length + itemImageCount)) {
            res.status(400).json({ status: false, message: `Can upload only ${userMembership.pics_per_item_inventory} images per item` });
            return;
        }
        if (!(userMembership.videos_per_item_inventory >= videoFiles.length + itemVideocount)) {
            res.status(400).json({ status: false, message: `Can upload only ${userMembership.videos_per_item_inventory} videos per item` });
            return;
        }

        await asyncForEach(imageFiles, async function (imageFile, index) {
            return new Promise(async (resolve, reject) => {
                files.push(imageFile)
                resolve();
            })
        })

        await asyncForEach(videoFiles, async function (videoFile, index) {
            return new Promise(async (resolve, reject) => {
                files.push(videoFile)
                resolve();
            })
        })


        await asyncForEach(files, async function (file, index) {
            return new Promise(async (resolve, reject) => {
                var oldpath = file.file.path;
                var extension = path.extname(file.file.name);
                var File = path.basename(file.file.name, extension);
                File = File + '-' + Date.now() + extension;
                filesName.push({ "item_id": itemId, 'media_url': File, 'file_type': file.fileType })
                var newpath = `${path.join(__dirname, "../../../", "uploads/items")}/${File}`;
                fs.readFile(oldpath, (err, data) => {
                    fs.writeFile(newpath, data, function (err) {
                        if (err) {
                            logger.error('/user/item/add_item_media -> Something went wrong!', { obj: err });
                            res.status(500).send({ status: false, message: "Something went wrong!" });
                        }
                    });
                });
                resolve();
            })
        })
        mediaDetailsService.addMedia(filesName)
            .then(itemDetails => {
                if (itemDetails !== null) {
                    res.status(200).send({ itemDetails: itemDetails, status: true, message: "item media added successfully." });
                } else {
                    res.status(400).send({ status: false, message: "item media is not added" });
                }
            }).catch((error) => {
                logger.error('/user/item/add_item_media -> Post API -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
    form.parse(req);


}

exports.deleteItemMedia = async (req, res, next) => {
    let userId;

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('/user/item/delete_item_media/:mediaId -> Delete API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    // let showroomdetails = await showroomDetailsService.getShowroomDetails(req.params)
    let itemDetails = await itemDetailsService.checkItemCurrentUser({ 'itemId': req.params.itemId })
    if (itemDetails !== null) {
        if (itemDetails.current_user_id === userId) {
            mediaDetailsService.deleteMedia(req.params)
                .then(deletedMedia => {
                    console.log(deletedMedia.result)
                    if (deletedMedia.result == 1) {
                        var filePath = `${path.join(__dirname, "../../../", "uploads/items")}/${deletedMedia.media.media_url}`;
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                res.status(404).send({ status: false, message: "item media not found" });
                                console.error(err)
                                return
                            }
                            res.status(200).send({ status: true, message: "item media deleted successfully" });
                        })
                    } else {
                        res.status(404).send({ status: false, message: "item media not found" });
                    }
                }).catch((error) => {
                    logger.error('/user/item/delete_item_media/:mediaId -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                });
        } else {
            res.status(400).send({ status: false, message: "Can't delete other user item media" });
        }
    } else {
        res.status(400).send({ status: false, message: "item not found" });
    }
}

// reject item transfer request
exports.rejectItemRequest = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/reject_item_request -> Patch API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.rejectItemRequest(req.body, userId)
        .then((rejectItemRequest) => {
            if (rejectItemRequest !== null) {
                if ("notUpdated" in rejectItemRequest) {
                    res.status(400).send({ status: false, message: "Can't reject the item transfer request" });
                }
                res.status(200).send({ status: true, message: "item transfer request rejected successfully" });
            } else {
                res.status(400).send({ status: false, message: "Can't reject the item transfer request" });
            }
        }).catch((error) => {
            logger.error('user/item/reject_item_request -> Patch API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getBulkAddSampleSheet = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/get_bulk_add_sample_sheet -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });
    itemDetailsService.getBulkAddSampleSheet(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ "items": items, status: true, message: "sample sheet data fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetch the sample sheet data." });
            }
        }).catch((error) => {
            logger.error('user/item/get_bulk_add_sample_sheet -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.bulkAddItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/bulk_add_item -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.bulkAddItem(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ "items": items, status: true, message: "Items added successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to add the items." });
            }
        }).catch((error) => {
            logger.error('user/item/bulk_add_item -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.editIncompleteItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/edit_incomplete_item -> Put API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.editIncompleteItem(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ "items": items, status: true, message: "Items edited successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to edit the items." });
            }
        }).catch((error) => {
            logger.error('user/item/edit_incomplete_item -> Put API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.reportItem = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/item/report_item -> Post API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    itemDetailsService.reportItem(req.body, userId)
        .then((items) => {
            if (items !== null) {
                res.status(200).send({ status: true, message: "Item is reported successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable report the item." });
            }
        }).catch((error) => {
            logger.error('user/item/report_item -> Post API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};