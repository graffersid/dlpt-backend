const { validationResult } = require('express-validator');
var jwt = require('../../lib/jwt');
var bcrypt = require('../../lib/bcrypt');
const { logger } = require("../../../config/logger");

const categoryDetailsService = require('../../services/categoryDetailsService')

exports.getCategoryDetails = async (req, res, next) => {
    let userId;
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Fetching userId from Access Token 
    await jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        }
        userId = result.userId;
    }).catch((error) => {
        logger.error('user/category/get_category_details -> Get API -> Error in fetching userId from token.');
        res.status(500).send({ status: false, message: error })
    });

    categoryDetailsService.getCategoryDetails(req.query)
        .then(categoryDetails => {
            if (categoryDetails !== null) {
                res.status(200).send({ categoryDetails: categoryDetails, status: true, message: "Category details Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the categories details." });
            }
        }).catch((error) => {
            logger.error('user/category/get_category_details -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });

}
