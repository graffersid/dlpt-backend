var express = require('express');
var router = express.Router();
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const categoryController = require('../controllers/categoryController')
const categoryDetailsValidator = require('../../middleware/validators/categoryDetailsValidator')


// -> get category details
router.get('/get_category_details', userAuthValidator.validate("headers"), categoryDetailsValidator.validate("getCategoryDetails"),
    userAuthCheck.isUserValid, categoryController.getCategoryDetails);

module.exports = router;