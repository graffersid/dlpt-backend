var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const showroomController = require('../controllers/showroomController')
const showroomValidator = require('../../middleware/validators/showroomDetailsValidator')

//Get Dashboards Showroom Posts
router.get('/get_showrooms', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, showroomController.getShowrooms)

//Get showroom post details 
router.get('/get_showroom_details/:showroomId', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, showroomValidator.validate("showroomId"), showroomController.getShowroomDetails)

// add showroom post
router.post('/add_showroom', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, showroomController.addShowroom)

//edit showroom post details 
router.patch('/edit_showroom', userAuthvalidator.validate("headers"), showroomValidator.validate("editShowroom"), userAuthCheck.isUserValid, showroomController.editShowroom)

//pin showroom post  
router.patch('/pin_showroom', userAuthvalidator.validate("headers"), showroomValidator.validate("pinShowroom"), userAuthCheck.isUserValid, showroomController.pinShowroom)

//unpin showroom post  
router.patch('/unpin_showroom', userAuthvalidator.validate("headers"), showroomValidator.validate("pinShowroom"), userAuthCheck.isUserValid, showroomController.unpinShowroom)

//delete showroom post 
router.delete('/delete_showroom/:showroomId', userAuthvalidator.validate("headers"), showroomValidator.validate("showroomId"), userAuthCheck.isUserValid, showroomController.deleteShowroom)

//add showroom media
router.post('/add_showroom_media', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, showroomController.addShowroomMedia)

//delete showroom media
router.delete('/delete_showroom_media/:mediaId/:showroomId', userAuthvalidator.validate("headers"), showroomValidator.validate("deleteShowroomMedia"),
    userAuthCheck.isUserValid, showroomController.deleteShowroomMedia)

// Report showroom 
router.post('/report_showroom', userAuthvalidator.validate("headers"), showroomValidator.validate("reportShowroom"), userAuthCheck.isUserValid, showroomController.reportShowroom);

// search dashboard showroom 
router.get('/search_dashboard_showroom', userAuthvalidator.validate("headers"), showroomValidator.validate("searchDashboardShowrooms"), userAuthCheck.isUserValid, showroomController.searchDashboardShowrooms);


module.exports = router;