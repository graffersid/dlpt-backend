var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware/validators/userAuthValidator')
const itemDetailsValidator = require('../../middleware/validators/itemDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const itemController = require('../controllers/itemController')

// get dashboard items
router.get('/get_items', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.getItems);

router.get('/search_items_by_name/:itemName', userAuthvalidator.validate("headers"),
    userAuthCheck.isUserValid, itemDetailsValidator.validate("searchItemsByName"), itemController.searchItemsByName);

//get item details
router.get('/get_item_details/:userItemId', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.getItem);

// add items
router.post('/add_item', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.addItem);

// edit items
router.put('/edit_item', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("editUserItem"), userAuthCheck.isUserValid, itemController.editItem);

// delete user items
router.patch('/delete_user_item', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("userItemId"), userAuthCheck.isUserValid, itemController.deleteUserItem);

// change item privacy
router.patch('/change_item_privacy', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("changeItemPrivacy"), userAuthCheck.isUserValid, itemController.changeItemPrivacy);

// move item pass collection
router.patch('/move_to_past_collection', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("userItemId"), userAuthCheck.isUserValid, itemController.moveToPastCollection);

// transfer item to other user
router.post('/transfer_item', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("transferItem"), userAuthCheck.isUserValid, itemController.transferItem);

// accept items trasfer request
router.patch('/accept_item_request', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("acceptItemRequest"), userAuthCheck.isUserValid, itemController.acceptItemRequest);

// reject items trasfer request
router.patch('/reject_item_request', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("rejectItemRequest"), userAuthCheck.isUserValid, itemController.rejectItemRequest);

// add item media
router.post('/add_item_media', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.addItemMedia);

// delete Item media
router.delete('/delete_item_media/:mediaId/:itemId', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("deleteItemMedia"), userAuthCheck.isUserValid, itemController.deleteItemMedia);
// get_bulk_add_sample_sheet
router.get('/get_bulk_add_sample_sheet', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.getBulkAddSampleSheet);

// bulk_add_item
router.post('/bulk_add_item', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.bulkAddItem);

// edit incomplete item
router.put('/edit_incomplete_item', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, itemController.editIncompleteItem);

// Report item 
router.post('/report_item', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("reportItem"), userAuthCheck.isUserValid, itemController.reportItem);

// search dashboard item 
router.get('/search_dashboard_item', userAuthvalidator.validate("headers"), itemDetailsValidator.validate("searchDashboardItem"),
    userAuthCheck.isUserValid, itemController.searchDashboardItem);

module.exports = router;