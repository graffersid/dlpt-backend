var express = require('express');
var router = express.Router();
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const rankingController = require('../controllers/rankingController')

// -> get category details
router.get('/get_ranking', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, rankingController.getRanking);

module.exports = router;