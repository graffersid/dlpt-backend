var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const likeController = require('../controllers/likeController')
const likeValidator = require('../../middleware/validators/likeValidator')

router.post('/like_showroom', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, likeValidator.validate("likeShowroom"), likeController.likeShowroom)

router.post('/like_item', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, likeValidator.validate("likeItem"), likeController.likeItem)

router.delete('/:likeId', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, likeValidator.validate("unLike"), likeController.unLike)

module.exports = router;