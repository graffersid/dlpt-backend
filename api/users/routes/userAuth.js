var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware//validators/userAuthValidator')
const userAuthController = require('../controllers/userAuthController')

const userAuthCheck = require('../../middleware/userAuthCheck')


// -> Register User to Database
router.post('/register', userAuthvalidator.validate("registerUser"),
    userAuthController.registerUser
);

// -> Login User
router.post('/login', userAuthvalidator.validate("loginUser"),
    userAuthController.loginUser
);

// -> logout User
router.get('/logout', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid,
    userAuthController.logout
);

// -> Forgot Password
router.post('/forgot_password', userAuthvalidator.validate("forgotPassword"),
    userAuthController.forgotPassword
);

// -> Reset Password
router.post('/reset_password', userAuthvalidator.validate("resetPassword"),
    userAuthController.resetPassword
);

module.exports = router;