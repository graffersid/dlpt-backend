var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const followerController = require('../controllers/followerController')
const followerValidator = require('../../middleware/validators/followerValidator')

//follow User 
router.post('/', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, followerValidator.validate("followUser"), followerController.followUser)

//Unfollow User
router.delete('/:followerId', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, followerValidator.validate("unfollowUser"), followerController.unfollowUser)

module.exports = router;