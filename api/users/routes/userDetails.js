var express = require('express');
var router = express.Router();
const userDetailsValidator = require('../../middleware/validators/userDetailsValidator')
const userAuthValidator = require('../../middleware//validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
const userDetailsController = require('../controllers/userDetailsController')

// -> (set,get) User Profile details
router.route('/profile')
    .get(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsController.getProfileDetails)
    .post(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsValidator.validate("editProfile"),
        userDetailsController.setProfileDetails
    )

router.route('/profile/:userId')
    .get(userAuthValidator.validate("headers"), userDetailsValidator.validate("UserId"), userAuthCheck.isUserValid,
        userDetailsController.getGuestUserProfileDetails)

// -> Edit User Profile Picture
router.post('/edit_profile_picture', userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
    userDetailsController.editProfilePicture
);

// -> change user password
router.patch('/change_password', userAuthValidator.validate("headers"), userDetailsValidator.validate("changePassword"), userAuthCheck.isUserValid,
    userDetailsController.changePassword
);

// -> change user password
router.get('/search_user/:userName', userAuthValidator.validate("headers"), userDetailsValidator.validate("searchUser"), userAuthCheck.isUserValid,
    userDetailsController.searchUser
);

// -> User interest
router.route('/interests')
    .get(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsController.getInterests)
    .post(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsValidator.validate("addInterests"), userDetailsController.addInterests)
    .patch(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsValidator.validate("editInterests"), userDetailsController.editInterests)
    .delete(userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
        userDetailsValidator.validate("deleteInterests"), userDetailsController.deleteInterests)

// -> Get user current collections 
router.get('/get_user_current_collection', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userDetailsController.getUserCurrentCollection);

// -> Get other user current collections 
router.get('/get_user_current_collection/:userId', userAuthValidator.validate("headers"), userDetailsValidator.validate("UserId"),
    userAuthCheck.isUserValid, userDetailsController.getUserCurrentCollection);

// -> Get user past collections
router.get('/get_user_past_collection', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userDetailsController.getUserpastCollection);

// -> Get other user past collections
router.get('/get_user_past_collection/:userId', userAuthValidator.validate("headers"), userDetailsValidator.validate("UserId"),
    userAuthCheck.isUserValid, userDetailsController.getUserpastCollection);

// -> Get user showrooms
router.get('/get_user_showrooms', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userDetailsController.getUserShowrooms);

// -> Get other user showrooms
router.get('/get_user_showrooms/:userId', userAuthValidator.validate("headers"), userDetailsValidator.validate("UserId"),
    userAuthCheck.isUserValid, userDetailsController.getUserShowrooms);

// -> Get user collection requests
router.get('/get_user_item_request', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userDetailsController.getUserCollectionRequests);

// Report item 
router.post('/report_user', userAuthValidator.validate("headers"), userDetailsValidator.validate("reportUser"), userAuthCheck.isUserValid, userDetailsController.reportUser);

// Report item 
router.get('/search_dashboard_user', userAuthValidator.validate("headers"), userDetailsValidator.validate("searchDashboardUser"), userAuthCheck.isUserValid, userDetailsController.searchDashboardUser);

router.get('/get_user_notifications', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userDetailsController.getUserNotifications);

module.exports = router;