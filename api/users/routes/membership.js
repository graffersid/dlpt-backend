var express = require('express');
var router = express.Router();
const userAuthvalidator = require('../../middleware/validators/userAuthValidator');
const userAuthCheck = require('../../middleware/userAuthCheck');

const membershipController = require('../controllers/membershipController');
const membershipDetailsValidator = require('../../middleware/validators/membershipDetailsValidator')

router.get('/get_membership_lists', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, membershipController.getMembershipLists)

router.get('/get_current_membership', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, membershipController.getCurrentMembership)

router.get('/get_user_membership_history', userAuthvalidator.validate("headers"), userAuthCheck.isUserValid, membershipController.getUserMembershipHistory)

// router.post('/add_user_membership', userAuthvalidator.validate("headers"), membershipDetailsValidator.validate("addUserMembership"), userAuthCheck.isUserValid, membershipController.addUserMembership)

module.exports = router;