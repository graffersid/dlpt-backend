const bcrypt = require('bcrypt');
const { promiseImpl } = require('ejs');

/**
 * Hash Password Method
 */
exports.hashPassword = (password) => {
    return new promiseImpl((resolve) => {
        resolve(bcrypt.hashSync(password, bcrypt.genSaltSync(8)))
    })
}

/**
 * comparePassword
 */
exports.comparePassword = (hashPassword, password) => {
    return bcrypt.compareSync(password, hashPassword);
}

/**
 * isValidEmail helper method
 */
exports.isValidEmail = (email) => {
    return /\S+@\S+\.\S+/.test(email);
}

