var express = require('express');
var router = express.Router();
var request = require('request');
const membershipDetailsService = require('../services/membershipDetailsService')
const { validationResult } = require('express-validator');
var jwt = require('../lib/jwt');
const { logger } = require("../../config/logger");
// Add your credentials:
// Add your client ID and secret
var CLIENT = (process.env.NODE_ENV == "production" ? process.env.LIVE_CLIENTID : process.env.SANDBOX_CLIENTID)
var SECRET = (process.env.NODE_ENV == "production" ? process.env.LIVE_SECRETID : process.env.SANDBOX_SECRETID)
var PAYPAL_API = (process.env.NODE_ENV == "production" ? 'https://api-m.paypal.com' : 'https://api-m.sandbox.paypal.com')

router.post('/create-payment', async (req, res, next) => {
    // 2. Call /v1/payments/payment to set up the payment
    let membershipDetails = await membershipDetailsService.getAllMemberships(req.body)
    request.post(PAYPAL_API + '/v1/payments/payment',
        {
            auth:
            {
                user: CLIENT,
                pass: SECRET
            },
            body:
            {
                intent: 'sale',
                payer:
                {
                    payment_method: 'paypal'
                },
                transactions: [
                    {
                        amount:
                        {
                            total: membershipDetails[0].membership_plan_cost,
                            currency: 'USD'
                        }
                    }],
                redirect_urls:
                {
                    return_url: 'https://example.com',
                    cancel_url: 'https://example.com'
                }
            },
            json: true
        }, function (err, response) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            // 3. Return the payment ID to the client
            res.json(
                {
                    id: response.body.id,
                    membershipId: membershipDetails[0].membership_id
                });
        });
});

router.post('/execute-payment', async (req, res, next) => {
    // 2. Get the payment ID and the payer ID from the request body.
    var paymentID = req.body.paymentID;
    var payerID = req.body.payerID;
    let membershipDetails = await membershipDetailsService.getAllMemberships(req.body)
    // 3. Call /v1/payments/payment/PAY-XXX/execute to finalize the payment.
    request.post(PAYPAL_API + '/v1/payments/payment/' + paymentID +
        '/execute',
        {
            auth:
            {
                user: CLIENT,
                pass: SECRET
            },
            body:
            {
                payer_id: payerID,
                transactions: [
                    {
                        amount:
                        {
                            total: membershipDetails[0].membership_plan_cost,
                            currency: 'USD'
                        }
                    }]
            },
            json: true
        },
        async (err, response) => {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            let userId;
            // -> request validation check
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(422).json({ errors: errors.array() });
                return;
            }

            // -> Fetching userId from Access Token 
            await jwt.verifyToken(req.body.accessToken).then((result) => {
                if (!result.userId) {
                    res.status(401).send({ status: false, message: "Unauthorized user" });
                }
                userId = result.userId;
            }).catch((error) => {
                logger.error('my-api/execute-payment/add_membership -> Post API -> Error in fetching userId from token.');
                res.status(500).send({ status: false, message: error })
            });
            // 4. Return a success response to the client
            await membershipDetailsService.addUserMembership(req.body, userId)
            res.json(
                {
                    status: 'success'
                });
        });
});

module.exports = router;