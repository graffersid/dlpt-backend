const jwt = require('jsonwebtoken');

exports.generateToken = async (id, isAdmin) => {
    const token = jwt.sign({ 'userId': id, 'isAdmin': isAdmin }, process.env.SECRET);
    return token;
}

/**
 * Verify Token
 */
exports.verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        try {
            const decoded = jwt.verify(token, process.env.SECRET);
            if (decoded.userId) {
                resolve(decoded);
            } else {
                resolve(false);
            }
        } catch (error) {
            console.log("error:   ", error)
            reject(error)
        }
    });
}
