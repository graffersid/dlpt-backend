const { Op, fn, col } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { Items, ItemFields, UserFields, UserFieldValues, UserItems, ReportToAdmin, ItemFieldValues, Categories, Media, Users, UserMemberships, Followers, Likes, OwnershipTransfer } = require('../models');

const notificationService = require('../services/notificationService')


exports.getAllItems = (query) => {
    return new Promise(async (resolve, reject) => {

        var whereStatement = {};
        var categorySearch = {};
        var categoryFilter = {}

        //category filter
        if (query.categoryId) {
            categoryFilter.category_id = query.categoryId;
        }
        if (query.searchString) {
            if (isNaN(parseInt(query.searchString))) {
                whereStatement.item_name = { [Op.like]: '%' + query.searchString + '%' }
                categorySearch.category_name = { [Op.like]: '%' + query.searchString + '%' }
            } else {
                whereStatement.item_id = parseInt(query.searchString)
            }
        }

        var categorySearchlist = [];
        if (Object.keys(categorySearch).length > 0) {
            await Categories.findAll({
                where: categorySearch, raw: true,
                order: [
                    ['category_id', 'ASC']
                ],
            }).then((result) => {

                if (result.length > 0) {
                    categorySearchlist = result;
                    categorySearchlist = categorySearchlist.map((category) => {
                        return category.category_id
                    })
                }
            }).catch((error) => { console.log(error) })
        }

        var finalSearch = {}

        if (categorySearchlist.length > 0) {
            finalSearch = { [Op.or]: [{ item_name: { [Op.like]: '%' + query.searchString + '%' } }, { category_id: { [Op.in]: categorySearchlist } }] }
        } else {
            finalSearch = whereStatement
        }

        var allItems;
        await Items.findAll({
            where: finalSearch,
            include: ['itemMedia', {
                model: Categories,
                as: "category",
                where: categoryFilter
            }],
            order: [
                ['item_id', 'ASC']
            ],
        }).then((items) => {
            allItems = JSON.parse(JSON.stringify(items))
        }).catch((error) => {
            console.log(error)
            reject(error)
        })

        await asyncForEach(allItems, async function (item, index) {
            return new Promise(async (resolve, reject) => {
                let category_id = item.category.category_id
                var subcategories = [];
                await getSubcategory(category_id)
                async function getSubcategory(categoryId) {
                    var whereStatement = {};
                    if (categoryId) { whereStatement.category_id = categoryId }

                    Categories.findOne({
                        where: whereStatement, raw: true
                    }).then((items) => {
                        if (items.parent_category_id && items.parent_category_id != 0) {
                            subcategories.push(items)
                            getSubcategory(items.parent_category_id)
                            return;
                        } else {
                            subcategories.push(items)
                            subcategories.reverse();
                            allItems[index].categories = subcategories;
                            resolve();
                        }
                    }).catch((error) => reject(error))
                }
            })
        })
        resolve(allItems)
    })
}

exports.searchItemsByName = (params) => {
    return new Promise(async (resolve, reject) => {
        var whereStatement = {};
        whereStatement.item_name = { [Op.like]: '%' + params.itemName + '%' }
        await Items.findAll({
            where: whereStatement,
            attributes: [
                [sequelize.fn('DISTINCT', sequelize.col('item_name')), 'item_name'],
            ]
        }).then((items) => {
            resolve(items)
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })
}

exports.checkItemCurrentUser = (query) => {
    return new Promise(async (resolve, reject) => {
        Items.findOne({ where: { item_id: query.itemId }, attributes: ['current_user_id'] })
            .then((items) => {
                resolve(items)
            }).catch((error) => {
                console.log(error)
                reject(error)
            });
    });
}

exports.getItemDetails = (query) => {
    return new Promise(async (resolve, reject) => {
        var item;
        await Items.findOne({
            where: { item_id: query.itemId },
            include: [
                {
                    model: Users,
                    as: "currentUser",
                    attributes: ['is_active'],
                    include: [{
                        model: UserMemberships,
                        as: "userMemberships",
                        where: { 'is_active': true },
                    }]
                },
                {
                    model: Media,
                    as: "itemMedia"
                },
                {
                    model: UserItems,
                    as: "itemUsers",
                    // attributes: ['created_on'],
                    include: [
                        {
                            model: Users,
                            as: "itemUserDetails",
                            attributes: ['user_id', 'user_name']
                        }]
                },
                {
                    model: Categories,
                    as: "category",
                    include: [
                        {
                            model: ItemFields,
                            as: "itemFields",
                            where: { category_id: { [Op.col]: 'category.category_id' } },
                            include: [
                                {
                                    model: ItemFieldValues,
                                    as: "itemFieldValue",
                                    where: { item_id: query.itemId },
                                }
                            ],
                        }
                    ]
                }
            ],
            order: [
                ['item_id', 'ASC'],
                [{ model: Categories, as: "category" }, { model: ItemFields, as: "itemFields" }, 'item_field_id', 'ASC'],
                [{ model: UserItems, as: "itemUsers" }, 'created_on', 'ASC'],
                [{ model: Media, as: "itemMedia" }, 'created_on', 'ASC']
            ],
        }).then((items) => {
            item = JSON.parse(JSON.stringify(items))
            if (item !== null) {
                let category_id = item.category.category_id
                getSubcategory(category_id)
            } else {
                resolve(item);
            }

        }).catch((error) => {
            console.log(error)
            reject(error)
        })

        var subcategories = [];
        async function getSubcategory(categoryId) {
            var whereStatement = {};
            if (categoryId) { whereStatement.category_id = categoryId }

            Categories.findOne({
                where: whereStatement, raw: true
            }).then((items) => {
                if (items.parent_category_id && items.parent_category_id != 0) {
                    subcategories.push(items)
                    getSubcategory(items.parent_category_id)
                    return;
                } else {
                    subcategories.push(items)
                    subcategories.reverse();
                    item.categories = subcategories;
                    resolve(item)
                }
            }).catch((error) => reject(error))
        }
    });

}

exports.editItem = async (body) => {
    return new Promise(async (resolve, reject) => {
        var item = await Items.findOne({ where: { item_id: body.itemId } })
        const t = await Sequelize.transaction();
        try {
            if (item !== null) {

                if (item.category_id !== body.categoryId) {
                    let query1 = {
                        item_name: body.itemName,
                        item_description: body.itemDescription,
                        category_id: body.categoryId
                    }
                    await Items.update(query1, { where: { item_id: body.itemId }, returning: true, plain: true, transaction: t });
                    await t.commit();
                    await notificationService.correctedItemNotification({ userId: item.current_user_id, itemId: body.itemId })
                    resolve(1);
                } else {
                    let query2 = {
                        item_name: body.itemName,
                        item_description: body.itemDescription,
                    }

                    await Items.update(query2, { where: { item_id: body.itemId }, transaction: t })
                    await asyncForEach(body.itemFieldValues, async function (itemFieldValue, index) {
                        await ItemFieldValues.update({ 'item_field_value': itemFieldValue.item_field_value }, { where: { 'item_field_value_id': itemFieldValue.item_field_value_id }, returning: true, plain: true, transaction: t })
                    })
                    await notificationService.correctedItemNotification({ userId: item.current_user_id, itemId: body.itemId })
                    await t.commit();
                    resolve(1)
                }

            } else {
                resolve(0)
            }
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.blockItem = (body) => {
    return new Promise((resolve, reject) => {
        const { itemId, isActive } = body
        Items.update({ 'is_active': isActive }, { where: { 'item_id': itemId }, returning: true, plain: true })
            .then(async (result) => {
                if (!result[1].is_active) {
                    await notificationService.blockItemNotification({ userId: result[1].current_user_id, itemId: result[1].item_id })
                }
                resolve(result[1]);
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}

// user portal Api services starts

// get dashboard items 
exports.getItems = (userId, paginate) => {
    return new Promise(async (resolve, reject) => {
        try {
            let followings = await Followers.findAll({ where: { 'followed_by_user_id': userId }, attributes: ['following_user_id'] })
            followingUserId = [];
            await asyncForEach(followings, async function (following, index) {
                return new Promise(async (resolve, reject) => {
                    followingUserId.push(following.following_user_id)
                    resolve();
                })
            })
            let items = await UserItems.findAll({
                where: { 'user_id': followingUserId, 'is_past_item': false, 'is_private': false, 'is_active': true },
                include: [
                    {
                        model: Users,
                        as: "itemUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    },
                    {
                        model: Items,
                        as: "itemsDetails",
                        where: { is_active: true },
                        attributes: ['item_name', 'item_id', 'category_id'],
                        include: [
                            {
                                model: Categories,
                                as: "category",
                                attributes: ['category_name']
                            },
                            {
                                model: Media,
                                as: "itemMedia",
                                attributes: ['media_id', 'media_url', 'file_type']
                            }
                        ]
                    }
                ],
                order: [
                    ['created_on', 'DESC'],
                    [{ model: Items, as: "itemsDetails" }, { model: Media, as: "itemMedia" }, 'created_on', 'ASC']
                ],
                attributes: ['user_item_id', 'user_id', 'total_views_count', "created_on"],
                offset: 10 * paginate.skip,
                limit: 10,
            })

            if (items.length > 0) {
                let Item = JSON.parse(JSON.stringify(items))
                await asyncForEach(Item, async function (item, index) {
                    const likesCount = Likes.count({ where: { 'user_item_id': item.user_item_id }, raw: true });
                    const isLiked = Likes.findOne({ where: { 'user_item_id': item.user_item_id, 'liked_by': userId }, raw: true });
                    return new Promise(async (resolve, reject) => {
                        Promise.all([likesCount, isLiked])
                            .then(async (values) => {
                                Item[index].likesCount = values[0]
                                if (values[1] !== null) {
                                    Item[index].isLiked = true;
                                    Item[index].likeId = values[1].like_id;
                                } else {
                                    Item[index].isLiked = false;
                                    Item[index].likeId = null;
                                }
                                resolve(Item[index])
                            }).catch(async (error) => {
                                reject(error)
                            })
                    })
                })
                resolve({ items: Item, skipNotFollowedUsersItems: false })
            } else {
                followingUserId.push(userId)
                let items = await UserItems.findAll({
                    where: { 'user_id': { [Op.not]: followingUserId }, 'is_past_item': false, 'is_private': false, 'is_active': true },
                    include: [
                        {
                            model: Users,
                            as: "itemUserDetails",
                            attributes: ['user_id', 'user_name', 'user_image']
                        },
                        {
                            model: Items,
                            as: "itemsDetails",
                            where: { current_user_id: { [Op.col]: 'user_items.user_id' }, is_active: true },
                            attributes: ['item_name', 'item_id', 'category_id'],
                            include: [
                                {
                                    model: Categories,
                                    as: "category",
                                    attributes: ['category_name']
                                },
                                {
                                    model: Media,
                                    as: "itemMedia",
                                    attributes: ['media_id', 'media_url', 'file_type']
                                }
                            ]
                        }
                    ],
                    order: [
                        ['created_on', 'DESC'],
                        [{ model: Items, as: "itemsDetails" }, { model: Media, as: "itemMedia" }, 'created_on', 'ASC']
                    ],
                    attributes: ['user_item_id', 'user_id', 'total_views_count', "created_on"],
                    offset: 10 * paginate.skipNotFollowedUsersItems,
                    limit: 10,
                })
                if (items.length > 0) {
                    let Item = JSON.parse(JSON.stringify(items))
                    await asyncForEach(Item, async function (item, index) {
                        const likesCount = Likes.count({ where: { 'user_item_id': item.user_item_id }, raw: true });
                        const isLiked = Likes.findOne({ where: { 'user_item_id': item.user_item_id, 'liked_by': userId }, raw: true });
                        return new Promise(async (resolve, reject) => {
                            Promise.all([likesCount, isLiked])
                                .then(async (values) => {
                                    Item[index].likesCount = values[0]
                                    if (values[1] !== null) {
                                        Item[index].isLiked = true;
                                        Item[index].likeId = values[1].like_id;
                                    } else {
                                        Item[index].isLiked = false;
                                        Item[index].likeId = null;
                                    }
                                    resolve(Item[index])
                                }).catch(async (error) => {
                                    reject(error)
                                })
                        })
                    })
                    resolve({ items: Item, skipNotFollowedUsersItems: true })
                } else {
                    resolve({ items, skipNotFollowedUsersItems: null })
                }
            }
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })

}

// item details for user portal
exports.getItem = (params, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await UserItems.findOne({ where: { 'user_item_id': params.userItemId }, attributes: ['item_id'] })
            let data1 = Items.findOne({
                where: { item_id: data.item_id },
                include: [
                    {
                        model: UserItems,
                        as: "itemUsers",
                        attributes: ['created_on'],
                        include: [
                            {
                                model: Users,
                                as: "itemUserDetails",
                                attributes: ['user_id', 'user_name']
                            }]
                    },
                    {
                        model: Media,
                        as: "itemMedia",
                        attributes: ['media_id', 'media_url', 'file_type']
                    },
                    {
                        model: Categories,
                        as: "category",
                        include: [
                            {
                                model: ItemFields,
                                as: "itemFields",
                                include: [
                                    {
                                        model: ItemFieldValues,
                                        as: "itemFieldValue",
                                        where: { item_id: data.item_id },
                                    }
                                ],
                            },
                            {
                                model: UserFields,
                                as: "userFields",
                                include: [
                                    {
                                        model: UserFieldValues,
                                        as: "userFieldvalue",
                                        where: { 'user_item_id': params.userItemId },
                                    }
                                ],
                            }

                        ]
                    }
                ],
                order: [
                    ['item_id', 'ASC'],
                    [{ model: Categories, as: "category" }, { model: ItemFields, as: "itemFields" }, 'item_field_id', 'ASC'],
                    [{ model: Categories, as: "category" }, { model: UserFields, as: "userFields" }, 'user_field_id', 'ASC'],
                    [{ model: UserItems, as: "itemUsers" }, 'created_on', 'ASC'],
                    [{ model: Media, as: "itemMedia" }, 'created_on', 'ASC']
                ],
                attributes: ['item_name', 'item_description', 'item_id', 'category_id'],
            })

            let items = UserItems.findOne({
                where: { 'user_item_id': params.userItemId },
                include: [
                    {
                        model: Users,
                        as: "itemUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image'],
                        include: [{
                            model: UserMemberships,
                            as: "userMemberships",
                            where: { 'is_active': true },
                        }]
                    },
                ],
                attributes: ['user_item_id', 'item_id', 'is_private', 'user_id', 'total_views_count', "created_on"],
            })

            Promise.all([data1, items])
                .then(async (values) => {
                    let Item = JSON.parse(JSON.stringify(values[1]))
                    Item.itemsDetails = values[0];
                    await UserItems.update({ 'total_views_count': (++Item.total_views_count) }, { where: { 'user_item_id': Item.user_item_id } });
                    const likesCount = Likes.count({ where: { 'user_item_id': Item.user_item_id }, raw: true });
                    const isLiked = Likes.findOne({ where: { 'user_item_id': Item.user_item_id, 'liked_by': userId }, raw: true });
                    Promise.all([likesCount, isLiked])
                        .then(async (values) => {
                            Item.likesCount = values[0];
                            if (values[1] !== null) {
                                Item.isLiked = true;
                                Item.likeId = values[1].like_id;
                            } else {
                                Item.isLiked = false;
                                Item.likeId = null;
                            }
                            getSubcategory(Item.itemsDetails.category_id)
                            var subcategories = [];
                            async function getSubcategory(categoryId) {
                                var whereStatement = {};
                                if (categoryId) { whereStatement.category_id = categoryId }

                                Categories.findOne({
                                    where: whereStatement, raw: true
                                }).then((items) => {
                                    if (items.parent_category_id && items.parent_category_id != 0) {
                                        subcategories.push(items)
                                        getSubcategory(items.parent_category_id)
                                        return;
                                    } else {
                                        subcategories.push(items)
                                        subcategories.reverse();
                                        Item.categories = subcategories;
                                        resolve({ items: Item })
                                    }
                                }).catch((error) => reject(error))
                            }

                            // resolve({ items: Item })
                        }).catch(async (error) => {
                            reject(error)
                        })
                }).catch(async (error) => {
                    reject(error)
                })

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })

}

exports.addItem = async (body, userId, media) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            let userItem;
            let item = await Items.create({ 'category_id': body.categoryId, 'current_user_id': userId, 'item_name': body.itemName, 'item_description': body.itemDescription }, { transaction: t })
            if (!body.makeItemPrivate) {
                userItem = await UserItems.create({ 'item_id': item.item_id, 'user_id': item.current_user_id, is_item_completed: true }, { transaction: t });
            } else {
                userItem = await UserItems.create({ 'item_id': item.item_id, 'user_id': item.current_user_id, is_item_completed: true, is_private: true }, { transaction: t });
            }

            let userFieldValues = JSON.parse(body.userFieldValues);
            let itemFieldValues = JSON.parse(body.itemFieldValues);
            let media1 = media;

            await asyncForEach(userFieldValues, async function (userFieldValue, index) {
                return new Promise(async (resolve, reject) => {
                    userFieldValues[index].user_item_id = userItem.user_item_id;
                    userFieldValues[index].item_id = userItem.item_id;
                    resolve()
                });
            });

            await asyncForEach(itemFieldValues, async function (itemFieldValue, index) {
                return new Promise(async (resolve, reject) => {
                    itemFieldValues[index].item_id = userItem.item_id;
                    resolve();
                })
            })

            await asyncForEach(media1, async function (file, index) {
                return new Promise(async (resolve, reject) => {
                    media1[index].item_id = item.item_id;
                    resolve();
                });
            });

            let userFieldValue = UserFieldValues.bulkCreate(userFieldValues, { transaction: t });
            let itemFieldValue = ItemFieldValues.bulkCreate(itemFieldValues, { transaction: t });
            let itemMedia = Media.bulkCreate(media1, { transaction: t });

            Promise.all([userFieldValue, itemFieldValue, itemMedia])
                .then(async (values) => {
                    await t.commit();
                    resolve(item);
                }).catch(error => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.getUserItemCount = async (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            UserItems.count({ where: { 'user_id': body.userId, 'is_active': true, 'is_past_item': false } })
                .then(userItems => {
                    resolve(userItems)
                }).catch((error) => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.editUserItem = async (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            var item = await Items.findOne({ where: { item_id: body.itemId }, attributes: ['current_user_id'] })

            if (item.current_user_id !== userId) {
                console.log(item.current_user_id);
                resolve({ currentUser: false })
            }

            let query2 = {
                item_name: body.itemName,
                item_description: body.itemDescription,
            }

            await Items.update(query2, { where: { item_id: body.itemId }, transaction: t })
            await asyncForEach(body.userFieldValues, async function (userFieldValue, index) {
                await UserFieldValues.update({ 'user_field_value': userFieldValue.user_field_value }, { where: { 'user_field_value_id': userFieldValue.user_field_value_id }, returning: true, plain: true, transaction: t })
            })
            await t.commit();
            resolve({ status: 1 })
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.deleteUserItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            UserItems.update({ 'is_active': false }, { where: { 'user_item_id': body.userItemId, 'user_id': userId } })
                .then(async (result) => {
                    resolve(result[0])
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.changeItemPrivacy = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            UserItems.update({ 'is_private': body.private }, { where: { 'user_item_id': body.userItemId, 'user_id': userId } })
                .then(async (result) => {
                    resolve(result[0])
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.moveToPastCollection = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            UserItems.update({ 'is_past_item': true }, { where: { 'user_item_id': body.userItemId, 'user_id': userId } })
                .then(async (result) => {
                    resolve(result[0])
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.transferItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {

            let item = Items.findOne({ where: { "item_id": body.itemId, current_user_id: userId } });
            let ownershipTransfer = OwnershipTransfer.findOne({ where: { 'ownership_transfer_to_user': body.transferToUserId, 'ownership_transfer_by_user': userId, 'item_id': body.itemId } });

            Promise.all([item, ownershipTransfer]).then((values) => {
                if (values[0] === null) {
                    resolve(result);
                } else if (values[1] !== null) {
                    resolve({ alreadyCreated: true });
                    return;
                } else {
                    OwnershipTransfer.create({ 'ownership_transfer_to_user': body.transferToUserId, 'ownership_transfer_by_user': userId, 'item_id': body.itemId, 'request_status_id': 1 })
                        .then(async (result) => {
                            resolve(result)
                        })
                        .catch(error => {
                            console.log(error)
                            reject(error)
                        })
                }
            }).catch(error => {
                console.log(error)
                reject(error)
            })

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.acceptItemRequest = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const t = await Sequelize.transaction();
            let item = Items.update({ 'current_user_id': userId }, { where: { 'item_id': body.itemId, 'current_user_id': body.ownershipTransferByUser }, transaction: t });
            let userItem = UserItems.update({ 'is_past_item': true }, { where: { 'item_id': body.itemId, 'user_id': body.ownershipTransferByUser, 'is_past_item': false }, transaction: t });
            let ownershiptransfer = OwnershipTransfer.update({ 'request_status_id': 2 }, { where: { 'ownership_transfer_id': body.ownershipTransferId, 'ownership_transfer_by_user': body.ownershipTransferByUser, 'request_status_id': 1 }, transaction: t });

            Promise.all([item, userItem, ownershiptransfer]).then(async (values) => {
                if (values[0][0] === 0) {
                    await t.rollback();
                    resolve({ cannotAccept: true })
                } else if (values[1][0] === 0) {
                    await t.rollback();
                    resolve({ cannotAccept: true })
                } else if (values[2][0] === 0) {
                    await t.rollback();
                    resolve({ cannotAccept: true })
                } else {
                    let existingUserItem = await UserItems.findOne({ where: { 'item_id': body.itemId, 'user_id': userId } })
                    if (existingUserItem !== null) {
                        let userItem = await UserItems.update({ 'is_past_item': false }, { where: { 'user_item_id': existingUserItem.user_item_id }, transaction: t });
                        await OwnershipTransfer.destroy({ where: { 'ownership_transfer_by_user': body.ownershipTransferByUser, 'item_id': body.itemId, 'request_status_id': 1 }, transaction: t })
                        if (userItem[0] === 1) {
                            await t.commit();
                            resolve(userItem)
                        } else {
                            await t.rollback();
                            resolve({ cannotAccept: true })
                        }

                    } else {
                        let userItem = await UserItems.create({ 'item_id': body.itemId, 'user_id': userId }, { transaction: t })
                        await OwnershipTransfer.destroy({ where: { 'ownership_transfer_by_user': body.ownershipTransferByUser, 'item_id': body.itemId, 'request_status_id': 1 }, transaction: t })
                        if (userItem[0] === 1) {
                            await t.commit();
                            resolve(userItem)
                        } else {
                            await t.rollback();
                            resolve({ cannotAccept: true })
                        }
                    }
                }
            }).catch(error => {
                console.log(error)
                reject(error)
            })

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.rejectItemRequest = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let ownershiptransfer = await OwnershipTransfer.update({ 'request_status_id': 3 }, { where: { 'ownership_transfer_id': body.ownershipTransferId, 'ownership_transfer_to_user': userId } });
            if (ownershiptransfer !== 0) {
                resolve(ownershiptransfer)
            } else {
                resolve({ notUpdated: true })
            }
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.getBulkAddSampleSheet = () => {
    return new Promise(async (resolve, reject) => {
        await Categories.findAll({
            where: { "is_last_child": true },
            attributes: ['category_id', 'category_name'],
            include: [
                {
                    model: UserFields,
                    as: 'userFields',
                    attributes: ['user_field_id', 'user_field_name']
                },
                {
                    model: ItemFields,
                    as: 'itemFields',
                    attributes: ['item_field_id', 'item_field_name']
                }
            ]
        }).then((items) => {
            // categoryDetails = items;
            resolve(items)
        }).catch((error) => {
            reject(error)
        })
    })
}

exports.bulkAddItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            let items = body.items
            await asyncForEach(items, async function (Item, index) {
                return new Promise(async (resolve, reject) => {
                    console.log(Item)
                    let item = await Items.create({ 'current_user_id': userId, 'item_name': Item.itemName, 'item_description': Item.itemDescription }, { transaction: t })
                    if (!Item.makeItemPrivate) {
                        userItem = await UserItems.create({ 'item_id': item.item_id, 'user_id': item.current_user_id, is_item_completed: false }, { transaction: t });
                    } else {
                        userItem = await UserItems.create({ 'item_id': item.item_id, 'user_id': item.current_user_id, is_item_completed: false, is_private: true }, { transaction: t });
                    }
                    resolve();
                })
            })
            await t.commit();
            resolve()
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.editIncompleteItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            let query2 = {
                item_name: body.itemName,
                item_description: body.itemDescription,
                category_id: body.categoryId,
                is_item_completed: true
            }

            await Items.update(query2, { where: { item_id: body.itemId }, transaction: t })
            await UserFieldValues.bulkCreate(body.userFieldValues, { transaction: t });
            await ItemFieldValues.bulkCreate(body.itemFieldValues, { transaction: t });
            await t.commit();
            resolve()
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.reportItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { reason, itemId } = body
        ReportToAdmin.create({ "reason": reason, "reported_item_id": itemId, "reported_by_user": userId })
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })
    })
}

exports.searchDashboardItem = (userId, query) => {
    return new Promise(async (resolve, reject) => {
        var whereStatement = { 'item_name': { [Op.like]: '%' + query.itemName + '%' }, 'is_active': true }
        //category filter
        if (query.categoryId) {
            whereStatement.category_id = query.categoryId;
        }
        let items = await UserItems.findAll({
            where: { 'user_id': { [Op.not]: userId }, 'is_past_item': false, 'is_private': false, 'is_active': true },
            include: [
                {
                    model: Users,
                    as: "itemUserDetails",
                    attributes: ['user_id', 'user_name', 'user_image']
                },
                {
                    model: Items,
                    as: "itemsDetails",
                    where: whereStatement,
                    attributes: ['item_name', 'item_id', 'category_id'],
                    include: [
                        {
                            model: Media,
                            as: "itemMedia",
                            attributes: ['media_id', 'media_url', 'file_type']
                        }
                    ]
                }
            ],
            order: [
                ['created_on', 'DESC'],
                [{ model: Items, as: "itemsDetails" }, { model: Media, as: "itemMedia" }, 'created_on', 'ASC']
            ],
            attributes: ['user_item_id', 'user_id', 'total_views_count', "created_on"],
            offset: 10 * query.skip,
            limit: 10,
        })

        if (items.length > 0) {
            let Item = JSON.parse(JSON.stringify(items))
            await asyncForEach(Item, async function (item, index) {
                const likesCount = Likes.count({ where: { 'user_item_id': item.user_item_id }, raw: true });
                const isLiked = Likes.findOne({ where: { 'user_item_id': item.user_item_id, 'liked_by': userId }, raw: true });
                return new Promise(async (resolve, reject) => {
                    Promise.all([likesCount, isLiked])
                        .then(async (values) => {
                            Item[index].likesCount = values[0]
                            if (values[1] !== null) {
                                Item[index].isLiked = true;
                                Item[index].likeId = values[1].like_id;
                            } else {
                                Item[index].isLiked = false;
                                Item[index].likeId = null;
                            }
                            resolve(Item[index])
                        }).catch(async (error) => {
                            reject(error)
                        })
                })
            })
            resolve({ items: Item, skip: true })
        } else {
            resolve({ items: items, skip: false })
        }
    })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
