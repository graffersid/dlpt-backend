const { Op, fn, col } = require('sequelize');
const sequelize = require('../../config/database');
const Sequelize = require('../../config/database');
const { Memberships, MarketingFields, UserMemberships, Users } = require('../models');
var moment = require('moment');
// const memberships = require('../models/memberships');


exports.getAllMemberships = (query) => {
    return new Promise((resolve, reject) => {

        var whereStatement = {};

        if (query.membershipId) {
            whereStatement.membership_id = query.membershipId
        }

        Memberships.findAll({
            where: whereStatement,
            include: [
                {
                    model: MarketingFields,
                    as: 'marketingFields',
                    attributes: ['membership_id', 'marketing_field_id', 'marketing_field_name']
                },
            ],
            order: [
                ['membership_id', 'ASC']
            ],
        }).then((memberships) => {
            resolve(memberships);
        }).catch((error) => reject(error))
    })
}

exports.addMembership = (body) => {
    return new Promise(async (resolve, reject) => {
        const { membershipDetails, marketingFieldDetails } = body
        async function changeMarketingFieldsMembershipId(membershipId) {
            return new Promise((resolve, reject) => {
                marketingFieldDetails.map((marketingField) => {
                    return marketingField.membership_id = membershipId
                });
                resolve();
            });
        }

        const t = await sequelize.transaction();
        try {
            let membership = await Memberships.create(membershipDetails, { raw: true, transaction: t });
            if (membership.membership_id) {
                await changeMarketingFieldsMembershipId(membership.membership_id)
                let marketing = await MarketingFields.bulkCreate(marketingFieldDetails, { raw: true, transaction: t });
                if (marketing) {
                    await t.commit();
                    resolve(membership)
                } else {
                    await t.rollback();
                    reject();
                }
            } else {
                await t.rollback();
                reject();
            }
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.updateMembership = (body) => {
    return new Promise(async (resolve, reject) => {
        let marketingFields = []
        let membership;
        const { membershipDetails, marketingFieldDetails } = body
        const t = await sequelize.transaction();
        try {
            membership = await Memberships.update(membershipDetails, { where: { 'membership_id': membershipDetails.membership_id }, returning: true, plain: true, transaction: t })
            if (membership[1].membership_id) {
                await asyncForEach(marketingFieldDetails, async function (marketingField, index) {
                    if (marketingField.marketing_field_id == null) {
                        await MarketingFields.create({ 'membership_id': marketingField.membership_id, 'marketing_field_name': marketingField.marketing_field_name }, { raw: true, transaction: t })
                    } else {
                        await MarketingFields.update(marketingField, { where: { 'marketing_field_id': marketingField.marketing_field_id }, returning: true, plain: true, transaction: t })
                            .then((result) => {
                                marketingFields.push(result[1])
                            })
                    }
                })
                await t.commit();
                resolve();

            } else {
                await t.rollback();
                reject();
            }
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}


exports.banMembership = (body) => {
    return new Promise((resolve, reject) => {
        const { membership_id, is_active } = body
        Memberships.update({ 'is_active': is_active }, { where: { 'membership_id': membership_id }, returning: true, plain: true })
            .then((result) => {
                resolve(result[1])
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })

}

exports.userCurrentMembership = (userId) => {
    return new Promise((resolve, reject) => {
        UserMemberships.findOne({ where: { 'is_active': true, 'user_id': userId } })
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}

exports.getUserMembershipHistory = (userId) => {
    return new Promise((resolve, reject) => {
        UserMemberships.findAll({
            where: { 'user_id': userId },
            order: [
                ['created_on', 'DESC']]
        }).then((result) => {
            resolve(result)
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })
}

exports.addUserMembership = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            var whereStatement = {};

            if (body.membershipId) {
                whereStatement.membership_id = body.membershipId;
            }

            let membership = await Memberships.findOne({
                where: whereStatement, attributes: { exclude: ['created_on', 'updated_on', 'is_active', 'description'] }
            })
            membership = JSON.parse(JSON.stringify(membership));
            membership.user_id = userId;
            let membershipExpirationDate = await setMembershipExpiryTime(membership.membership_plan_duration);
            membership.membership_expiration_date = membershipExpirationDate;
            UserMemberships.findOne({ where: { 'is_active': true, 'user_id': userId } })
                .then(async (currentMembership) => {
                    try {
                        if (currentMembership != null) {
                            let oldmembership = await UserMemberships.update({ "is_active": false }, { where: { "user_membership_id": currentMembership.user_membership_id }, transaction: t })
                        }
                        let newMembership = await UserMemberships.create(membership, { transaction: t });
                        await t.commit();
                        resolve(newMembership);
                    } catch (error) {
                        await t.rollback();
                        console.log(error)
                        reject(error)
                    }
                })
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.expiredMembershipUsers = () => {
    //membership id 1 is belongs to free membership
    return new Promise(async (resolve, reject) => {
        let expiredMembershipUsersArray = [];
        let usersIdArray = [];
        const t = await Sequelize.transaction();
        try {
            let updateExpiredMembership = UserMemberships.update({ is_active: false }, {
                where: {
                    is_active: true,
                    'membership_expiration_date': { [Op.lte]: await getDate() },
                    'membership_id': { [Op.not]: 1 }// membership not equal to free
                },
                transaction: t
            })

            let expiredMembershipUsers = UserMemberships.findAll({
                where: {
                    is_active: true,
                    'membership_expiration_date': { [Op.lte]: await getDate() },
                    'membership_id': { [Op.not]: 1 }
                },
                include: [
                    {
                        model: Users,
                        as: 'membershipUserDetails',
                        attributes: ['user_email', "user_id"]
                    },
                ],
                attributes: []
            })
            Promise.all([updateExpiredMembership, expiredMembershipUsers])
                .then(async (values) => {
                    await asyncForEach(values[1], async function (expMembershipUser, index) {
                        expiredMembershipUsersArray.push(expMembershipUser.membershipUserDetails.user_email);
                        usersIdArray.push(expMembershipUser.membershipUserDetails.user_id)
                    })
                    await asyncForEach(usersIdArray, async function (userId, index) {
                        return new Promise(async (resolve, reject) => {
                            await UserMemberships.findOne({ where: { 'user_id': userId, 'membership_id': 1 }, attributes: ['user_id', 'is_active', 'user_membership_id'] }).then(async (result) => {
                                if (result == null) {
                                    let whereStatement = {};
                                    whereStatement.membership_id = 1;// id '1' is for free membership
                                    let membership = await Memberships.findOne({
                                        where: whereStatement, attributes: { exclude: ['created_on', 'updated_on', 'is_active', 'description'] }
                                    })
                                    membership = JSON.parse(JSON.stringify(membership));
                                    membership.user_id = userId;
                                    let newMembership = await UserMemberships.create(membership, { transaction: t });
                                } else {
                                    await UserMemberships.update({ 'is_active': true }, { where: { 'user_membership_id': result.user_membership_id }, transaction: t })
                                }
                            })
                            resolve();
                        })
                    })
                    await t.commit()
                    resolve(expiredMembershipUsersArray)
                    // console.log(expiredMembershipUsersArray)
                }).catch(async (error) => {
                    reject(error)
                })

        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

async function setMembershipExpiryTime(duration) {
    return new Promise((resolve, reject) => {
        let date = new Date();
        date.setDate(date.getDate() + duration);
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}

async function getDate() {
    return new Promise((resolve, reject) => {
        let date = new Date();
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}