const { Op } = require('sequelize');
const { Media } = require('../models');

exports.addMedia = (media) => {
    return new Promise(async (resolve, reject) => {
        // var query = {}
        // if ("itemId" in value) {
        //     query = {
        //         item_id: value.itemId,
        //         media_url: value.file,
        //         file_type: value.fileType
        //     }
        // }
        Media.bulkCreate(media).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    })

}

exports.getMediaCount = (data) => {
    return new Promise(async (resolve, reject) => {
        // var query = {}
        // if ("itemId" in value) {
        //     query = {
        //         item_id: value.itemId,
        //         media_url: value.file,
        //         file_type: value.fileType
        //     }
        // }
        Media.count({ where: data }).then((result) => {
            resolve(result)
        }).catch((error) => {
            reject(error)
        })
    })
}

exports.deleteMedia = (params) => {
    return new Promise(async (resolve, reject) => {
        let media = await Media.findOne({ where: { media_id: params.mediaId } });
        if (media !== null) {
            Media.destroy({
                where: { media_id: params.mediaId }
            }).then((result) => {
                resolve({ result, media })
            }).catch((error) => {
                reject(error)
            })
        } else {
            resolve({ result: 0 })
        }
    })
}