const { Op, fn, col } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { Notifications, Media, Items, Showrooms } = require('../models');


exports.getUserNotification = (userId) => {
    return new Promise(async (resolve, reject) => {
        Notifications.findAll({
            where: {
                "user_id": userId
            },
            include: [
                {
                    model: Items,
                    as: "notificationItemDetails",
                    attributes: ['item_name', 'item_id'],
                    include: [{
                        model: Media,
                        as: "itemMedia",
                        attributes: ['media_id', 'media_url', 'file_type']
                    }]
                },
                {
                    model: Showrooms,
                    as: "notificationShowroomDetails",
                    attributes: ['showroom_title', 'showroom_id'],
                    include: [{
                        model: Media,
                        as: 'showroomMedia',
                        attributes: ["media_id", "media_url", "file_type"],
                    }]
                }
            ]
        }).then((result) => {
            resolve(result)
        }).catch(async (error) => {
            console.log(error)
            reject(error)
        });
    });
}

exports.blockItemNotification = (data) => {
    return new Promise(async (resolve, reject) => {
        let value = {
            user_id: data.userId,
            item_id: data.itemId,
            notification_type: "item blocked",
            notification_message: "Inventory item is blocked by admin"
        }
        Notifications.create(value)
            .then((result) => {
                resolve(result)
            }).catch(async (error) => {
                console.log(error)
                reject(error)
            });
    });
}

exports.correctedItemNotification = (data) => {
    return new Promise(async (resolve, reject) => {
        let value = {
            user_id: data.userId,
            item_id: data.itemId,
            notification_type: "item corrected",
            notification_message: "Inventory item is corrected due to violation of rules"
        }
        Notifications.create(value)
            .then((result) => {
                resolve(result)
            }).catch(async (error) => {
                console.log(error)
                reject(error)
            });
    });
}

// exports.createShowroomNotification = (query, userId) => {
//     return new Promise(async (resolve, reject) => {
//         // Notifications.
//     });
// }

// exports.createShowroomNotification = (query, userId) => {
//     return new Promise(async (resolve, reject) => {
//         // Notifications.
//     });
// }

// exports.setNotification = (query, userId) => {
//     return new Promise(async (resolve, reject) => {

//     })
// }