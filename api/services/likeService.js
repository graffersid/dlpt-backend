const { Op, fn, col } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { Likes } = require('../models');


exports.likeShowroom = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { userItemId, showroomId } = body
        var data = {
            showroom_id: showroomId,
            liked_by: userId
        }
        Likes.create(data)
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}

exports.likeItem = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { userItemId, showroomId } = body
        var data = {
            user_item_id: userItemId,
            liked_by: userId
        }
        Likes.create(data)
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}

exports.unLike = (params) => {
    return new Promise(async (resolve, reject) => {
        Likes.destroy({ where: { like_id: params.likeId } })
            .then((result) => {
                console.log(result)
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}