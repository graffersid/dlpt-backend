const { Users } = require('../models');
const { Op } = require('sequelize')
var moment = require('moment');

exports.registerUser = (email, password, userName) => {
    return new Promise((resolve, reject) => {
        Users.create({ 'user_email': email.toLowerCase(), 'user_password': password, 'user_name': userName })
            .then((result) => {
                resolve(result)
            })
            .catch((error) => reject(error))
    })
}

exports.logout = (userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'access_token': "" }, { where: { 'user_id': userId }, plain: true })
            .then(user => {
                resolve(user[1])
            }).catch((error) => reject(error))
    })
}

exports.setUserAccessToken = (token, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'access_token': token }, { where: { 'user_id': userId }, returning: ['access_token', 'is_admin', 'is_active', 'user_image', 'user_email', 'user_name'], plain: true })
            .then(user => {
                resolve(user[1])
            }).catch((error) => reject(error))
    });
}

exports.setResetPasswordTokenAndTime = (token, tokenExpiryTime, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'reset_password_token': token, 'reset_password_expires': tokenExpiryTime, },
            { where: { 'user_id': userId }, returning: ['user_email', 'reset_password_token'], plain: true })
            .then(user => {
                resolve(user[1])
            }).catch((error) => reject(error))
    });
}

exports.checkTokenExpiryTime = (token, date) => {
    return new Promise((resolve, reject) => {
        Users.findOne({
            where: { 'reset_password_token': token, 'reset_password_expires': { [Op.gte]: date } },
            attributes: ["user_id"], raw: true
        }).then(user => {
            resolve(user)
        }).catch((error) => reject(error))
    })
}

exports.resetPassword = (hashedpassword, userId) => {
    // console.log(hashedpassword, userId)
    return new Promise((resolve, reject) => {
        Users.update({ 'user_password': hashedpassword, 'reset_password_token': "" }, {
            where: { 'user_id': userId },
            returning: ['user_email'], plain: true
        }).then(user => {
            resolve(user[1])
        }).catch((error) => reject(error))
    });
}

exports.setTokenExpiryTime = () => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        date.setMinutes(date.getMinutes() + 30);
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}


exports.getDate = () => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        resolve(moment(date).format("YYYY-MM-DD HH:mm:ss.SSSSSS"));
    });
}