const { Op, fn, col } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { UserItems, Followers, Items, Users } = require('../models');

exports.getRanking = (query, userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let followers = await Followers.findAll({ where: { 'following_user_id': userId }, attributes: ['followed_by_user_id'] })
            followerUserId = [];
            followerUserId.push(userId)
            await asyncForEach(followers, async function (following, index) {
                return new Promise(async (resolve, reject) => {
                    followerUserId.push(following.followed_by_user_id)
                    resolve();
                })
            })

            let categoryWhereStatement = { 'is_active': true }
            let userWhereStatement = {};
            if (query.categoryId) {
                categoryWhereStatement = { 'category_id': query.categoryId };
            }
            if (query.city) {
                userWhereStatement.user_city = { [Op.like]: '%' + query.city + '%' };
            }

            let cityRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [
                    {
                        model: Users,
                        as: "itemUserDetails",
                        where: userWhereStatement,
                        attributes: ['user_id', 'user_name', 'user_image']
                    }
                ],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ],
                limit: 3
            })
            let yourCityRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [
                    {
                        model: Users,
                        as: "itemUserDetails",
                        where: userWhereStatement,
                        attributes: ['user_id', 'user_name', 'user_image']
                    }
                ],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ]
            })

            let networkRanking = UserItems.findAll({
                where: { 'user_id': followerUserId, 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [{
                    model: Users,
                    as: "itemUserDetails",
                    attributes: ['user_id', 'user_name', 'user_image']
                }],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ],
                limit: 3
            })
            let yourNetworkRanking = UserItems.findAll({
                where: { 'user_id': followerUserId, 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [{
                    model: Users,
                    as: "itemUserDetails",
                    attributes: ['user_id', 'user_name', 'user_image']
                }],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ]
            })

            let categoryRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [
                    {
                        model: Items,
                        as: "itemsDetails",
                        where: categoryWhereStatement,
                        attributes: []
                    }, {
                        model: Users,
                        as: "itemUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    }
                ],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ],
                limit: 3
            })

            let yourCategoryRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [
                    {
                        model: Items,
                        as: "itemsDetails",
                        where: categoryWhereStatement,
                        attributes: []
                    }, {
                        model: Users,
                        as: "itemUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    }
                ],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ]
            })

            let overallPlatformRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [{
                    model: Users,
                    as: "itemUserDetails",
                    attributes: ['user_id', 'user_name', 'user_image']
                }],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ],
                limit: 3
            })

            let yourOverallPlatformRanking = UserItems.findAll({
                where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                include: [{
                    model: Users,
                    as: "itemUserDetails",
                    attributes: ['user_id', 'user_name', 'user_image']
                }],
                group: ['user_items.user_id', "itemUserDetails.user_id"],
                attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                order: [
                    [Sequelize.literal('item_count'), 'DESC']
                ]
            })

            let followerCount = Followers.count({ where: { 'following_user_id': userId } })
            let userCount = Users.count({ where: { 'is_admin': false } })


            Promise.all([cityRanking, networkRanking, categoryRanking, overallPlatformRanking, yourCityRanking, yourNetworkRanking, yourCategoryRanking, yourOverallPlatformRanking, followerCount, userCount])
                .then(async (values) => {
                    let CityRanking = {}
                    let NetworkRanking = {}
                    let CategoryRanking = {}
                    let OverallPlatformRanking = {}
                    CityRanking.rankings = JSON.parse(JSON.stringify(values[0]))
                    NetworkRanking.rankings = JSON.parse(JSON.stringify(values[1]))
                    NetworkRanking.totalFollowersCount = values[8];
                    CategoryRanking.rankings = JSON.parse(JSON.stringify(values[2]))
                    OverallPlatformRanking.rankings = JSON.parse(JSON.stringify(values[3]))
                    OverallPlatformRanking.totalUsersCount = values[9]

                    values[4].find((element, index) => {
                        if (element.user_id == userId) {
                            let yourCityRanking = JSON.parse(JSON.stringify(element));
                            yourCityRanking.rank = index + 1;
                            CityRanking.yourRanking = yourCityRanking;
                            return;
                        }
                    })
                    values[5].find((element, index) => {
                        if (element.user_id == userId) {
                            let yourNetworkRanking = JSON.parse(JSON.stringify(element));
                            yourNetworkRanking.rank = index + 1;
                            NetworkRanking.yourRanking = yourNetworkRanking;
                            return;
                        }
                    })
                    values[6].find((element, index) => {
                        if (element.user_id == userId) {
                            let yourCategoryRanking = JSON.parse(JSON.stringify(element));
                            yourCategoryRanking.rank = index + 1;
                            CategoryRanking.yourRanking = yourCategoryRanking;
                            return;
                        }
                    })
                    values[7].find((element, index) => {
                        if (element.user_id == userId) {
                            let yourOverallPlatformRanking = JSON.parse(JSON.stringify(element));
                            yourOverallPlatformRanking.rank = index + 1;
                            OverallPlatformRanking.yourRanking = yourOverallPlatformRanking;
                            return;
                        }
                    })

                    resolve({
                        'cityRanking': CityRanking,
                        'networkRanking': NetworkRanking,
                        'categoryRanking': CategoryRanking,
                        'overallPlatformRanking': OverallPlatformRanking
                    })
                }).catch(async (error) => {
                    console.log(error)
                    reject(error)
                })

        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};