const { Followers } = require('../models');

exports.followUser = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { followingUserId } = body
        var data = {
            following_user_id: followingUserId,
            followed_by_user_id: userId
        }
        Followers.create(data)
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}

exports.unfollowUser = (params) => {
    return new Promise(async (resolve, reject) => {
        Followers.destroy({ where: { follower_id: params.followerId } })
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}