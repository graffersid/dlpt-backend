const { Op } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { Users, UserMemberships, ReportToAdmin, Followers, Categories, Items, Likes, UserInterests, UserItems, Media, OwnershipTransfer, UserFields, UserFieldValues, OwnershipTransferStatus } = require('../models')

exports.getAllUsers = (query) => {
    return new Promise((resolve, reject) => {
        var whereStatement = { is_admin: false };

        if (query.searchString) {
            if (isNaN(parseInt(query.searchString))) {
                whereStatement.user_name = { [Op.like]: '%' + query.searchString + '%' }
            } else {
                whereStatement.user_id = parseInt(query.searchString)
            }
        }
        Users.findAll({
            where: whereStatement,
            include: [
                {
                    model: Items,
                    as: 'userItems',
                    attributes: []
                }, {
                    model: UserMemberships,
                    where: {
                        is_active: true
                    },
                    as: "userMemberships",
                    attributes: ['membership_name', 'membership_id']
                }],
            order: [
                ['user_id', 'ASC']
            ],
            attributes: {
                exclude: ['access_token', 'user_password', 'reset_password_token',
                    'reset_password_expires', 'created_on', 'updated_on'],
                include: [[sequelize.fn('COUNT', sequelize.col('current_user_id')), 'item_count']]
            },
            group: ['users.user_id', 'userItems.current_user_id', 'userMemberships.user_membership_id']
        }).then(user => {
            resolve(user)
        }).catch((error) => reject(error))
    })

}

exports.getUserCurrentCollection = (query, userId, search) => {
    return new Promise(async (resolve, reject) => {
        try {
            let whereStatement = { 'user_id': query.userId, 'is_past_item': false, 'is_active': true }
            if ("itemPrivacy" in query) {
                whereStatement = { 'user_id': query.userId, 'is_private': query.itemPrivacy, 'is_past_item': false, 'is_active': true }
            }
            let itemsearch = {};
            let categorySearch = {};
            var finalSearch = {}

            if ('searchString' in search) {
                if (search.searchString) {
                    itemsearch.item_name = { [Op.like]: '%' + search.searchString + '%' }
                    categorySearch.category_name = { [Op.like]: '%' + search.searchString + '%' }
                }
            }

            var categorySearchlist = [];
            if (Object.keys(categorySearch).length > 0) {
                await Categories.findAll({
                    where: categorySearch, raw: true,
                    order: [
                        ['category_id', 'ASC']
                    ],
                }).then((result) => {

                    if (result.length > 0) {
                        categorySearchlist = result;
                        categorySearchlist = categorySearchlist.map((category) => {
                            return category.category_id
                        })
                    }
                }).catch((error) => { console.log(error) })
            }

            var finalSearch = {}
            if (categorySearchlist.length > 0) {
                finalSearch = { current_user_id: { [Op.col]: 'user_items.user_id' }, is_active: true, [Op.or]: [{ item_name: { [Op.like]: '%' + search.searchString + '%' } }, { category_id: { [Op.in]: categorySearchlist } }] }
            } else if (search.searchString) {
                finalSearch = { current_user_id: { [Op.col]: 'user_items.user_id' }, is_active: true, item_name: { [Op.like]: '%' + search.searchString + '%' } }
            } else {
                finalSearch = { current_user_id: { [Op.col]: 'user_items.user_id' }, is_active: true }
            }

            let items = await UserItems.findAll({
                where: whereStatement,
                include: [
                    {
                        model: Items,
                        as: "itemsDetails",
                        where: finalSearch,
                        attributes: ['category_id', 'item_name', 'item_description'],
                        include: [
                            {
                                model: Categories,
                                as: "category",
                                attributes: ["category_name"]
                            },
                            {
                                model: Media,
                                as: 'itemMedia',
                                attributes: ["media_id", "media_url", "file_type"],
                            }
                        ]
                    }
                ]
            })

            let Item = JSON.parse(JSON.stringify(items))
            await asyncForEach(Item, async function (item, index) {
                const likesCount = Likes.count({ where: { 'user_item_id': item.user_item_id }, raw: true });
                const isLiked = Likes.findOne({ where: { 'user_item_id': item.user_item_id, 'liked_by': userId }, raw: true });
                return new Promise(async (resolve, reject) => {
                    Promise.all([likesCount, isLiked])
                        .then(async (values) => {
                            Item[index].likesCount = values[0]
                            if (values[1] !== null) {
                                Item[index].isLiked = true;
                                Item[index].likeId = values[1].like_id;
                            } else {
                                Item[index].isLiked = false;
                                Item[index].likeId = null;
                            }
                            resolve(Item[index])
                        }).catch(async (error) => {
                            reject(error)
                        })
                })
            })
            resolve({ items: Item })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

exports.getUserpastCollection = (query, userId, search) => {
    return new Promise(async (resolve, reject) => {
        let whereStatement = { 'user_id': query.userId, 'is_past_item': true, 'is_active': true }
        if ("itemPrivacy" in query) {
            whereStatement = { 'user_id': query.userId, 'is_private': query.itemPrivacy, 'is_past_item': true, 'is_active': true }
        }

        let itemsearch = {};
        let categorySearch = {};
        var finalSearch = {}

        if ('searchString' in search) {
            if (search.searchString) {
                itemsearch.item_name = { [Op.like]: '%' + search.searchString + '%' }
                categorySearch.category_name = { [Op.like]: '%' + search.searchString + '%' }
            }
        }

        var categorySearchlist = [];
        if (Object.keys(categorySearch).length > 0) {
            await Categories.findAll({
                where: categorySearch, raw: true,
                order: [
                    ['category_id', 'ASC']
                ],
            }).then((result) => {

                if (result.length > 0) {
                    categorySearchlist = result;
                    categorySearchlist = categorySearchlist.map((category) => {
                        return category.category_id
                    })
                }
            }).catch((error) => { console.log(error) })
        }

        var finalSearch = {}

        if (categorySearchlist.length > 0) {
            finalSearch = { is_active: true, [Op.or]: [{ item_name: { [Op.like]: '%' + search.searchString + '%' } }, { category_id: { [Op.in]: categorySearchlist } }] }
        } else if (search.searchString) {
            finalSearch = { is_active: true, item_name: { [Op.like]: '%' + search.searchString + '%' } }
        } else {
            finalSearch = { is_active: true }
        }

        let items = await UserItems.findAll({
            where: whereStatement,
            include: [
                {
                    model: Items,
                    as: "itemsDetails",
                    where: finalSearch,
                    attributes: ['category_id', 'item_name', 'item_description'],
                    include: [
                        {
                            model: Categories,
                            as: "category",
                            attributes: ["category_name"]
                        },
                        {
                            model: Media,
                            as: 'itemMedia',
                            attributes: ["media_id", "media_url", "file_type"],
                        }
                    ]
                }
            ]
        })

        let Item = JSON.parse(JSON.stringify(items))
        await asyncForEach(Item, async function (item, index) {
            const likesCount = Likes.count({ where: { 'user_item_id': item.user_item_id }, raw: true });
            const isLiked = Likes.findOne({ where: { 'user_item_id': item.user_item_id, 'liked_by': userId }, raw: true });
            return new Promise(async (resolve, reject) => {
                Promise.all([likesCount, isLiked])
                    .then(async (values) => {
                        Item[index].likesCount = values[0]
                        if (values[1] !== null) {
                            Item[index].isLiked = true;
                            Item[index].likeId = values[1].like_id;
                        } else {
                            Item[index].isLiked = false;
                            Item[index].likeId = null;
                        }
                        resolve(Item[index])
                    }).catch(async (error) => {
                        reject(error)
                    })
            })
        })
        resolve({ items: Item })
    })
}

exports.getUserCollectionRequests = (query) => {
    return new Promise((resolve, reject) => {

        OwnershipTransfer.findAll({
            where: { 'ownership_transfer_to_user': query.userId, 'request_status_id': 1 },
            include: [
                {
                    model: OwnershipTransferStatus,
                    as: 'requestStatus',

                }, {
                    model: Items,
                    as: "requestItem",
                    include: [
                        {
                            model: Categories,
                            as: "category",
                            attributes: ['category_name']
                        },
                        {
                            model: Users,
                            as: "currentUser",
                            attributes: ['user_id', 'user_name', 'user_image']
                        },
                        {
                            model: Media,
                            as: "itemMedia",
                            attributes: ['media_id', 'media_url', 'file_type']
                        }
                    ]
                }],
        }).then(userCollectionRequests => {
            resolve(userCollectionRequests)
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })

}

exports.getUserProfileDetails = (userId, id) => {
    return new Promise(async (resolve, reject) => {

        let totalPurchasePrice = 0;
        let totalItemValue = 0;
        await UserItems.findAll({
            where: { 'user_id': userId, 'is_active': true, 'is_item_completed': true },
            include: [
                {
                    model: Items,
                    as: "itemsDetails",
                    attributes: ['category_id']
                }
            ],
            attributes: ['user_item_id']
        }).then(async (results) => {

            await asyncForEach(results, async function (result, index) {
                return new Promise(async (resolve, reject) => {

                    let pricePaid = await UserFields.findOne({
                        where: {
                            'category_id': result.itemsDetails.category_id, 'user_field_name': 'PricePaid'
                        }, include: [
                            {
                                model: UserFieldValues,
                                as: "userFieldvalue",
                                where: { 'user_item_id': result.user_item_id },
                            }
                        ],
                    })
                    let soldprice = await UserFields.findOne({
                        where: {
                            'category_id': result.itemsDetails.category_id, 'user_field_name': 'Sold-Amount'
                        },
                        include: [
                            {
                                model: UserFieldValues,
                                as: "userFieldvalue",
                                where: { 'user_item_id': result.user_item_id },
                            }
                        ],
                    })
                    totalPurchasePrice = totalPurchasePrice + parseInt(pricePaid.userFieldvalue[0].user_field_value);
                    totalItemValue = totalItemValue + parseInt(soldprice.userFieldvalue[0].user_field_value);
                    resolve()
                })
            })
        }).catch((error) => {
            console.log(error)
        })


        let isFollowed;
        let rank = null;
        let userDetails = Users.findOne({
            where: { 'user_id': userId }, attributes: {
                exclude: ['access_token', 'reset_password_token',
                    'reset_password_expires', 'created_on', 'updated_on', 'user_password', 'is_profile_completed']
            },
            include: [
                {
                    model: UserMemberships,
                    as: "userMemberships",
                    where: { "is_active": true }
                }
            ],

        });

        let followerCount = Followers.count({ where: { 'following_user_id': userId } });
        let followingCount = Followers.count({ where: { 'followed_by_user_id': userId } });

        let overallPlatformRanking = UserItems.findAll({
            where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
            include: [{
                model: Users,
                as: "itemUserDetails",
                attributes: ['user_id', 'user_name', 'user_image']
            }],
            group: ['user_items.user_id', "itemUserDetails.user_id"],
            attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
            order: [
                [Sequelize.literal('item_count'), 'DESC']
            ]
        })

        if (id) {
            isFollowed = await Followers.findOne({ where: { 'following_user_id': userId, 'followed_by_user_id': id }, attributes: ['follower_id'] });
        }

        Promise.all([userDetails, followerCount, followingCount, overallPlatformRanking])
            .then(async (values) => {
                let userdetails = JSON.parse(JSON.stringify(values[0]))
                if (userdetails.userMemberships[0].ranking) {
                    values[3].find((element, index) => {
                        if (element.user_id == userId) {
                            rank = index + 1;
                            return;
                        }
                    })
                }

                if (id) {
                    if (isFollowed !== null) {
                        userdetails.isFollowed = true;
                        userdetails.follow_id = isFollowed.follower_id;
                    } else {
                        userdetails.isFollowed = false;
                        userdetails.follow_id = null;
                    }
                }
                userdetails.followerCount = values[1];
                userdetails.followingCount = values[2];
                userdetails.overAllRanking = rank;
                userdetails.totalPurchasePrice = totalPurchasePrice;
                userdetails.totalItemValue = totalItemValue;

                resolve(userdetails)

            }).catch(async (error) => {
                console.log(error)
                reject(error)
            });
    });

}

exports.blockUser = (body) => {
    return new Promise((resolve, reject) => {
        const { userId, isActive } = body
        Users.update({ 'is_active': isActive }, { where: { 'user_id': userId }, returning: true, plain: true })
            .then((result) => {
                resolve(result[1])
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })

}

exports.setUserProfileDetails = (userId, values) => {
    return new Promise((resolve, reject) => {
        const { userName, name, dateOfBirth, email, gender, country, city, zipCode } = values;
        var query = {
            user_name: userName,
            name: name.toLowerCase(),
            user_date_of_birth: dateOfBirth,
            user_email: email,
            user_gender: gender.toLowerCase(),
            user_country: country.toLowerCase(),
            user_city: city,
            user_zip_code: zipCode
        }
        Users.update(query, {
            where: { 'user_id': userId }, returning: ["user_id", "user_name", "name", "user_email", "user_image",
                "user_gender", "user_date_of_birth", "user_city", "user_country", "user_zip_code", "is_active",
                "is_admin"], plain: true
        }).then(user => {
            resolve(user[1])
        }).catch((error) => reject(error))
    });

}

exports.updateUserImage = (file, userId) => {
    return new Promise((resolve, reject) => {
        Users.update({ 'user_image': file }, { where: { 'user_id': userId }, returning: ['user_image'], plain: true })
            .then(user => {
                resolve(user[1])
            }).catch((error) => reject(error))
    });
}

exports.isUserEmailUnique = (email, userId) => {
    return new Promise((resolve, reject) => {

        Users.findOne({ attributes: ['user_id', 'name', 'user_email', 'user_password', "is_admin", "is_profile_completed"], where: { 'user_email': email.toLowerCase() }, raw: true })
            .then(user => {
                if (userId) {
                    if (user !== null) {
                        if (user.user_id != userId) {
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    } else {
                        resolve(false)
                    }
                } else {
                    resolve(user)
                }
            }).catch((error) => reject(error))
    })

}

exports.isUsernameUnique = (userName, userId) => {
    return new Promise((resolve, reject) => {

        Users.findOne({ attributes: ['user_id', 'user_name'], where: { 'user_name': userName }, raw: true })
            .then(user => {
                if (user !== null) {
                    if (user.user_id != userId) {
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                } else {
                    resolve(false)
                }
            }).catch((error) => reject(error))
    })
}

exports.getInterests = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let userInterests = await UserInterests.findAll({
                where: { user_id: userId }, attributes: ['interest_id', 'category_id'], order: [['created_on', 'ASC']], raw: true
            })
            await asyncForEach(userInterests, async function (userInterest, index) {
                return new Promise(async (resolve, reject) => {
                    getSubcategory(userInterest.category_id)
                    var subcategories = [];
                    function getSubcategory(categoryId) {
                        var whereStatement = {};
                        if (categoryId) { whereStatement.category_id = categoryId }

                        Categories.findOne({
                            where: whereStatement, raw: true
                        }).then((items) => {

                            if (items.parent_category_id && items.parent_category_id != 0) {
                                subcategories.push(items)
                                getSubcategory(items.parent_category_id)
                            } else {
                                subcategories.push(items)
                                subcategories.reverse();
                                userInterests[index].categories = subcategories;
                                resolve({ categories: subcategories })
                            }
                        }).catch((error) => reject(error))
                    }
                })
            })
            resolve(userInterests)
        } catch (error) {
            console.log(error)
        }
    })
}

exports.addInterests = (userId, body) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            let userInterests = []
            await asyncForEach(body.interestsArray, async function (interests, index) {
                userInterests.push({ category_id: interests.categoryId, user_id: userId })
            })
            UserInterests.bulkCreate(userInterests, { transaction: t })
                .then((result) => {
                    Users.update({ 'is_profile_completed': true }, { where: { user_id: userId }, transaction: t }).then(async (result1) => {
                        if (result1) {
                            await t.commit();
                            resolve(result)
                        }
                    })
                })
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.editInterests = (body) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            await asyncForEach(body.interestsArray, async function (interests, index) {
                await UserInterests.update({ 'category_id': interests.category_id }, { where: { 'interest_id': interests.interest_id }, returning: true, plain: true, transaction: t })
            })
            await t.commit();
            resolve(1)
        } catch (error) {
            console.log(error)
            await t.rollback();
            reject(error)
        }
    })
}

exports.deleteInterests = (deleteInterestsId) => {
    return new Promise(async (resolve, reject) => {
        UserInterests.destroy({ where: { interest_id: deleteInterestsId } })
            .then((result) => {
                resolve(result)
            }).catch((error) => reject(error))
    })
}

exports.searchUser = (params) => {
    return new Promise(async (resolve, reject) => {
        var whereStatement = {
            is_admin: false,
            user_name: { [Op.like]: '%' + params.userName + '%' }
        };
        Users.findAll({
            where: whereStatement,
            attributes: ['user_id', 'user_name']
        }).then(user => {
            resolve(user)
        }).catch((error) => reject(error))
    })
}

exports.searchDashboardUser = (userId, query) => {
    return new Promise(async (resolve, reject) => {
        var whereStatement = {
            'is_admin': false,
            'user_id': { [Op.not]: userId },
            'name': { [Op.like]: '%' + query.userName + '%' }
        };
        let SearchedUsers = await Users.findAll({
            where: whereStatement,
            attributes: ['user_id', 'name', 'user_name', 'user_image', 'user_city', 'user_country'],
            include: [
                {
                    model: UserMemberships,
                    as: "userMemberships",
                    where: { "is_active": true }
                }
            ],
            offset: 10 * query.skip,
            limit: 10,
        })
        let searchedUsers = JSON.parse(JSON.stringify(SearchedUsers))
        await asyncForEach(searchedUsers, async function (searchedUser, index) {
            return new Promise(async (resolve, reject) => {
                let followerCount = Followers.count({
                    where: { following_user_id: searchedUser.user_id }
                })
                let isFollowed = Followers.findOne({ where: { 'following_user_id': searchedUser.user_id, 'followed_by_user_id': userId }, attributes: ['follower_id'] })
                let overallPlatformRanking = UserItems.findAll({
                    where: { 'is_active': true, 'is_private': false, 'is_past_item': false },
                    include: [{
                        model: Users,
                        as: "itemUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    }],
                    group: ['user_items.user_id', "itemUserDetails.user_id"],
                    attributes: ['user_id', [Sequelize.fn('count', Sequelize.col("user_items.user_id")), 'item_count']],
                    order: [
                        [Sequelize.literal('item_count'), 'DESC']
                    ]
                })
                Promise.all([followerCount, isFollowed, overallPlatformRanking])
                    .then(async (values) => {
                        let rank = null;
                        if (searchedUser.userMemberships[0].ranking) {
                            values[2].find((element, index) => {
                                if (element.user_id == searchedUser.user_id) {
                                    rank = index + 1;
                                    return;
                                }
                            })
                        }
                        if (values[1] !== null) {
                            searchedUsers[index].isFollowed = true;
                            searchedUsers[index].follow_id = values[1].follower_id;
                        } else {
                            searchedUsers[index].isFollowed = false;
                            searchedUsers[index].follow_id = null;
                        }

                        searchedUsers[index].followerCount = values[0];
                        searchedUsers[index].overAllRanking = rank;
                        resolve();
                    }).catch(async (error) => {
                        console.log(error)
                        reject(error)
                    });

            });
        })
        resolve(searchedUsers)
    })
}

exports.getUsersCount = () => {
    return new Promise(async (resolve, reject) => {
        Users.count({
            where: { is_admin: false }
        }).then(usersCount => {
            resolve(usersCount)
        }).catch((error) => reject(error))
    })
}

exports.reportUser = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { reason, user_id } = body
        ReportToAdmin.create({ "reason": reason, "reported_user_id": user_id, "reported_by_user": userId })
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })
    })
}



async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};