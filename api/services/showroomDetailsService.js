const { Op } = require('sequelize');
const sequelize = require('sequelize')
const Sequelize = require('../../config/database');
const { Showrooms, ReportToAdmin, Users, Likes, Followers, Media } = require('../models')
const mediaDetailsService = require('../services/mediaDetailsService')

exports.getUserShowrooms = (query, userId, data) => {
    return new Promise(async (resolve, reject) => {
        let whereStatement = { user_id: query.userId }
        if ("showroomTitle" in data) {
            whereStatement.showroom_title = { [Op.like]: '%' + data.showroomTitle + '%' }
        }

        let showrooms = await Showrooms.findAll({
            where: whereStatement,
            include: [
                {
                    model: Media,
                    as: 'showroomMedia',
                    attributes: ["media_id", "media_url", "file_type"],

                }],
        })
        let Showroom = JSON.parse(JSON.stringify(showrooms))
        await asyncForEach(Showroom, async function (showroom, index) {
            const likesCount = Likes.count({ where: { 'showroom_id': showroom.showroom_id }, raw: true });
            const isLiked = Likes.findOne({ where: { 'showroom_id': showroom.showroom_id, 'liked_by': userId }, raw: true });
            return new Promise(async (resolve, reject) => {
                Promise.all([likesCount, isLiked])
                    .then(async (values) => {
                        Showroom[index].likesCount = values[0]
                        if (values[1] !== null) {
                            Showroom[index].isLiked = true;
                            Showroom[index].likeId = values[1].like_id;
                        } else {
                            Showroom[index].isLiked = false;
                            Showroom[index].likeId = null;
                        }
                        resolve(Showroom[index])
                    }).catch(async (error) => {
                        reject(error)
                    })
            })
        })
        resolve({ showrooms: Showroom })
    })
}

exports.getShowroomDetails = (query, userId) => {
    return new Promise((resolve, reject) => {

        Showrooms.findOne({
            where: { showroom_id: query.showroomId },
            include: [
                {
                    model: Media,
                    as: 'showroomMedia',
                    attributes: ["media_id", "showroom_id", "media_url", "file_type"],

                },
                {
                    model: Users,
                    as: 'showroomUserDetails',
                    attributes: ["user_id", "user_name", "name", "user_email", "user_image"],
                }
            ],
        }).then(async showroomDetails => {
            let UserId
            if (userId) {
                UserId = userId;
            } else {
                UserId = 0;
            }
            let showroom = JSON.parse(JSON.stringify(showroomDetails))
            await Showrooms.update({ 'total_views_count': (++showroom.total_views_count) }, { where: { 'showroom_id': showroom.showroom_id } })
            const likesCount = Likes.count({ where: { 'showroom_id': showroom.showroom_id }, raw: true });
            const isLiked = Likes.findOne({ where: { 'showroom_id': showroom.showroom_id, 'liked_by': UserId }, raw: true });
            Promise.all([likesCount, isLiked])
                .then(async (values) => {
                    showroom.likesCount = values[0]
                    if (values[1] !== null) {
                        showroom.isLiked = true;
                        showroom.likeId = values[1].like_id;
                    } else {
                        showroom.isLiked = false;
                        showroom.likeId = null;
                    }
                    resolve(showroom)
                }).catch(async (error) => {
                    reject(error)
                })
        }).catch((error) => {
            console.log(error)
            reject(error)
        })
    })

}

// User portal api start

//Get dashboard showrooms
exports.getShowrooms = (userId, paginate) => {
    return new Promise(async (resolve, reject) => {
        try {
            let followings = await Followers.findAll({ where: { 'followed_by_user_id': userId }, attributes: ['following_user_id'] })
            followingUserId = [];
            await asyncForEach(followings, async function (following, index) {
                return new Promise(async (resolve, reject) => {
                    followingUserId.push(following.following_user_id)
                    resolve();
                })
            })
            let showrooms = await Showrooms.findAll({
                where: { 'user_id': followingUserId },
                include: [
                    {
                        model: Users,
                        as: "showroomUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    },
                    {
                        model: Media,
                        as: 'showroomMedia',
                        attributes: ["media_id", "media_url", "file_type"],

                    }],
                order: [
                    ['created_on', 'DESC'],
                    [{ model: Media, as: "showroomMedia" }, 'created_on', 'ASC']
                ],
                attributes: { exclude: ['updated_on'] },
                offset: 10 * paginate.skip,
                limit: 10,
            })
            if (showrooms.length > 0) {
                let Showrooms = JSON.parse(JSON.stringify(showrooms))
                await asyncForEach(Showrooms, async function (showroom, index) {
                    const likesCount = Likes.count({ where: { 'showroom_id': showroom.showroom_id }, raw: true });
                    const isLiked = Likes.findOne({ where: { 'showroom_id': showroom.showroom_id, 'liked_by': userId }, raw: true });
                    return new Promise(async (resolve, reject) => {
                        Promise.all([likesCount, isLiked])
                            .then(async (values) => {
                                Showrooms[index].likesCount = values[0]
                                if (values[1] !== null) {
                                    Showrooms[index].isLiked = true;
                                    Showrooms[index].likeId = values[1].like_id;
                                } else {
                                    Showrooms[index].isLiked = false;
                                    Showrooms[index].likeId = null;
                                }
                                resolve(Showrooms[index])
                            }).catch(async (error) => {
                                reject(error)
                            })
                    })
                })
                resolve({ showrooms: Showrooms, skipNotFollowedUsersShowrooms: false })
            } else {
                followingUserId.push(userId)
                let showrooms = await Showrooms.findAll({
                    where: { 'user_id': { [Op.not]: followingUserId } },
                    include: [
                        {
                            model: Users,
                            as: "showroomUserDetails",
                            attributes: ['user_id', 'user_name', 'user_image']
                        },
                        {
                            model: Media,
                            as: 'showroomMedia',
                            attributes: ["media_id", "media_url", "file_type"],

                        }],
                    order: [
                        ['created_on', 'DESC'],
                        [{ model: Media, as: "showroomMedia" }, 'created_on', 'ASC']
                    ],
                    attributes: { exclude: ['updated_on'] },
                    offset: 10 * paginate.skipNotFollowedUsersShowrooms,
                    limit: 10,
                })
                if (showrooms.length > 0) {
                    let Showrooms = JSON.parse(JSON.stringify(showrooms))
                    await asyncForEach(Showrooms, async function (showroom, index) {
                        const likesCount = Likes.count({ where: { 'showroom_id': showroom.showroom_id }, raw: true });
                        const isLiked = Likes.findOne({ where: { 'showroom_id': showroom.showroom_id, 'liked_by': userId }, raw: true });
                        return new Promise(async (resolve, reject) => {
                            Promise.all([likesCount, isLiked])
                                .then(async (values) => {
                                    Showrooms[index].likesCount = values[0]
                                    if (values[1] !== null) {
                                        Showrooms[index].isLiked = true;
                                        Showrooms[index].likeId = values[1].like_id;
                                    } else {
                                        Showrooms[index].isLiked = false;
                                        Showrooms[index].likeId = null;
                                    }
                                    resolve(Showrooms[index])
                                }).catch(async (error) => {
                                    reject(error)
                                })
                        })
                    })
                    resolve({ showrooms: Showrooms, skipNotFollowedUsersShowrooms: true })
                } else {
                    resolve({ showrooms, skipNotFollowedUsersShowrooms: null })
                }
            }
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

// Edit showroom posts
exports.editShowroom = (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            Showrooms.update({ 'showroom_title': body.title, 'showroom_description': body.description }, { where: { 'showroom_id': body.showroomId } })
                .then((result) => {
                    resolve(result[0])
                }).catch(async (error) => {
                    reject(error)
                })
        } catch (error) {
            reject(error)
        }
    })
}

// pin showroom posts
exports.pinShowroom = (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            Showrooms.update({ 'is_pinned': true }, { where: { 'showroom_id': body.showroomId } })
                .then((result) => {
                    resolve(result[0])
                }).catch(async (error) => {
                    reject(error)
                })
        } catch (error) {
            reject(error)
        }
    })
}

// pin showroom posts
exports.unpinShowroom = (body) => {
    return new Promise(async (resolve, reject) => {
        try {
            Showrooms.update({ 'is_pinned': false }, { where: { 'showroom_id': body.showroomId } })
                .then((result) => {
                    resolve(result[0])
                }).catch(async (error) => {
                    reject(error)
                })
        } catch (error) {
            reject(error)
        }
    })
}

// get showroom user count
exports.getUserShowroomCount = (query) => {
    return new Promise(async (resolve, reject) => {
        try {
            Showrooms.count({ where: { user_id: query.userId } })
                .then(userShowrooms => {
                    resolve(userShowrooms)
                }).catch((error) => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

// Add showroom
exports.addShowroom = (data, media, userId) => {
    return new Promise(async (resolve, reject) => {
        const t = await Sequelize.transaction();
        try {
            let showroom = await Showrooms.create({ 'user_id': userId, "showroom_title": data.title, "showroom_description": data.description }, { transaction: t });
            await asyncForEach(media, async function (element, index) {
                return new Promise(async (resolve, reject) => {
                    media[index].showroom_id = showroom.showroom_id
                    resolve();
                })
            })
            Media.bulkCreate(media, { transaction: t }).then(async (result) => {
                await t.commit();
                resolve(showroom);
            }).catch((error) => {
                reject(error)
            })
        } catch (error) {
            await t.rollback();
            reject(error)
        }
    })
}

//delete showroom
exports.deleteShowroom = (params) => {
    return new Promise(async (resolve, reject) => {
        try {
            Showrooms.destroy({ where: { "showroom_id": params.showroomId } })
                .then(async (result) => {
                    resolve(result);
                }).catch((error) => {
                    console.log(error)
                    reject(error)
                })
        } catch (error) {
            reject(error)
        }
    })
}

//report showroom
exports.reportShowroom = (body, userId) => {
    return new Promise(async (resolve, reject) => {
        const { reason, showroomId } = body
        ReportToAdmin.create({ "reason": reason, "reported_showroom_id": showroomId, "reported_by_user": userId })
            .then((result) => {
                resolve(result)
            }).catch((error) => {
                reject(error)
            })
    })
}

//search Dashboard Showrooms
exports.searchDashboardShowrooms = (userId, query) => {
    return new Promise(async (resolve, reject) => {
        try {
            let showrooms = await Showrooms.findAll({
                where: { 'showroom_title': { [Op.like]: '%' + query.showroomTitle + '%' }, 'user_id': { [Op.not]: userId } },
                include: [
                    {
                        model: Users,
                        as: "showroomUserDetails",
                        attributes: ['user_id', 'user_name', 'user_image']
                    },
                    {
                        model: Media,
                        as: 'showroomMedia',
                        attributes: ["media_id", "media_url", "file_type"],

                    }],
                order: [
                    ['created_on', 'DESC'],
                    [{ model: Media, as: "showroomMedia" }, 'created_on', 'ASC']
                ],
                attributes: { exclude: ['updated_on'] },
                offset: 10 * query.skip,
                limit: 10,
            })
            if (showrooms.length > 0) {
                let Showrooms = JSON.parse(JSON.stringify(showrooms))
                await asyncForEach(Showrooms, async function (showroom, index) {
                    const likesCount = Likes.count({ where: { 'showroom_id': showroom.showroom_id }, raw: true });
                    const isLiked = Likes.findOne({ where: { 'showroom_id': showroom.showroom_id, 'liked_by': userId }, raw: true });
                    return new Promise(async (resolve, reject) => {
                        Promise.all([likesCount, isLiked])
                            .then(async (values) => {
                                Showrooms[index].likesCount = values[0]
                                if (values[1] !== null) {
                                    Showrooms[index].isLiked = true;
                                    Showrooms[index].likeId = values[1].like_id;
                                } else {
                                    Showrooms[index].isLiked = false;
                                    Showrooms[index].likeId = null;
                                }
                                resolve(Showrooms[index])
                            }).catch(async (error) => {
                                reject(error)
                            })
                    })
                })
                resolve({ showrooms: Showrooms })
            } else {
                resolve({ showrooms: showrooms })
            }
        } catch (error) {
            console.log(error)
            reject(error)
        }

    })
}


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};