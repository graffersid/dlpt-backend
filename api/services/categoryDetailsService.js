const { Op } = require('sequelize');
const { Categories, ItemFields, UserFields } = require('../models');
const sequelize = require('../../config/database')

exports.getAllCategories = (query) => {
    return new Promise((resolve, reject) => {
        var whereStatement = {};

        if (query.parentCategoryId) {
            whereStatement.parent_category_id = query.parentCategoryId
        }

        if (query.categoryId) {
            whereStatement = {
                [Op.or]: [
                    { category_id: query.categoryId },
                    { parent_category_id: query.categoryId }
                ]
            }
        }

        if (query.searchString) {
            whereStatement.category_name = { [Op.like]: '%' + query.searchString + '%' }
        }

        if (query.categoryId && query.searchString) {
            whereStatement = {
                category_name: { [Op.like]: '%' + query.searchString + '%' },
                [Op.or]: [
                    { category_id: query.categoryId },
                    { parent_category_id: query.categoryId }
                ]
            }

        }
        Categories.findAll({
            where: whereStatement,
            include: ["parentCategory"],
            order: [
                ['category_id', 'ASC']
            ]
        }).then((categories) => {
            resolve(categories);
        }).catch((error) => reject(error))

    })
}

exports.addCategory = (body) => {
    return new Promise(async (resolve, reject) => {

        categories = body.categories;
        itemFields = body.itemFields;
        userFields = body.userFields;

        var categoryArr = [];
        var categoryObj = {};

        async function setcategory(category) {
            return new Promise((resolve, reject) => {
                Object.keys(category).every(function (key) {
                    if (key == 'subcategory') {
                        categoryArr.push(categoryObj);
                        categoryObj = {};
                        resolve(setcategory(category[key]))
                        return false;
                    }
                    categoryObj[key] = category[key];
                    return true
                })
                if (!('subcategory' in category)) {
                    categoryArr.push(categoryObj);
                    console.log(categoryArr)
                    resolve(categoryArr)
                }
            })
        }

        async function changeItemFieldCatId(parentId) {
            return new Promise((resolve, reject) => {
                itemFields.map((itemField) => {
                    return itemField.category_id = parentId
                });
                resolve();
            });
        }
        async function changeUserFieldCatId(parentId) {
            return new Promise((resolve, reject) => {
                userFields.map((userField) => {
                    return userField.category_id = parentId
                });
                resolve();
            });
        }

        let categorydata = await setcategory(categories)
        console.log(categorydata)

        let parentId = categorydata[0].parent_category_id
        const t = await sequelize.transaction();

        try {

            await asyncForEach(categorydata, async function (category, index) {
                category.parent_category_id = parentId
                await Categories.create(category, { raw: true, transaction: t }).then((result) => {
                    parentId = result.category_id;
                })
            })

            await changeItemFieldCatId(parentId)
            await changeUserFieldCatId(parentId)

            const insertItemField = ItemFields.bulkCreate(itemFields, { raw: true, transaction: t });
            const insertUserField = UserFields.bulkCreate(userFields, { raw: true, transaction: t });

            Promise.all([insertItemField, insertUserField])
                .then(async (values) => {
                    if (values[0] !== null && values[1] !== null) {
                        await t.commit();
                        resolve(values);
                    } else {
                        await t.rollback();
                        reject();
                    }
                }).catch(async (error) => {
                    console.log("inside")
                    console.log(error)
                    await t.rollback();
                    reject(error)
                })
        } catch (error) {
            console.log("inside")
            reject(error)
            await t.rollback();
        }
    });
}

exports.getCategoryDetails = (query) => {
    return new Promise(async (resolve, reject) => {
        var whereStatement = {};
        var categoryDetails;
        if (query.categoryId) {
            whereStatement.category_id = query.categoryId
        }

        await Categories.findOne({
            where: whereStatement,
            include: [
                {
                    model: UserFields,
                    as: 'userFields',
                    attributes: ['user_field_id', 'user_field_name']
                },
                {
                    model: ItemFields,
                    as: 'itemFields',
                    attributes: ['item_field_id', 'item_field_name']
                }
            ]
        }).then((items) => {
            categoryDetails = items
        }).catch(() => { })

        getSubcategory(query.categoryId)
        var subcategories = [];

        async function getSubcategory(categoryId) {

            var whereStatement = {};
            if (categoryId) { whereStatement.category_id = categoryId }

            Categories.findOne({
                where: whereStatement, raw: true
            }).then((items) => {

                if (items.parent_category_id && items.parent_category_id != 0) {
                    subcategories.push(items)
                    getSubcategory(items.parent_category_id)
                } else {
                    subcategories.push(items)
                    subcategories.reverse();
                    resolve({ categories: subcategories, userFields: categoryDetails.userFields, itemFields: categoryDetails.itemFields })
                }
            }).catch((error) => reject(error))
        }

    })
}

exports.editCategory = async (body) => {
    return new Promise(async (resolve, reject) => {
        const t = await sequelize.transaction();

        var itemFields
        var userFields

        try {
            if (body.itemFields.length > 0) {
                itemFields = await ItemFields.bulkCreate(body.itemFields, { raw: true, transaction: t });
            }
            if (body.userFields.length > 0) {
                userFields = await UserFields.bulkCreate(body.userFields, { raw: true, transaction: t });
            }
            await t.commit();
            resolve()

        } catch (error) {
            console.log(error)
            console.log("inside")
            await t.rollback();
            reject(error)

        }
    })

    // Promise.all([insertItemField, insertUserField])
    //     .then(async (values) => {
    //         if (values[0] !== null && values[1] !== null) {
    //             await t.commit();
    //             resolve(values);
    //         } else {
    //             await t.rollback();
    //             reject();
    //         }
    //     }).catch(async (error) => {
    //         console.log("inside")
    //         console.log(error)
    //         await t.rollback();
    //         reject(error)
    //     })

}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};