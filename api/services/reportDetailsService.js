const { Op } = require('sequelize');
const sequelize = require('sequelize')
const { Users, Items, Showrooms, Categories, Media, ReportToAdmin } = require('../models')


exports.getAllReportedUsers = () => {
    return new Promise((resolve, reject) => {
        ReportToAdmin.findAll({
            where: { reported_showroom_id: null, reported_item_id: null },
            include: [
                {
                    model: Users,
                    as: "reportedUserDetails",
                    attributes: ['user_id', 'name', 'user_name']
                },
                {
                    model: Users,
                    as: "reportedByUserDetails",
                    attributes: ['user_id', 'name', 'user_name']
                }
            ],
            attributes: {
                exclude: ['reported_showroom_id', 'reported_item_id']
            },
            order: [
                ['created_on', 'DESC']
            ]
        }).then(reportedUsers => {
            resolve(reportedUsers)
        }).catch((error) => reject(error))
    })
}

exports.getAllReportedItems = () => {
    return new Promise((resolve, reject) => {
        ReportToAdmin.findAll({
            where: { reported_showroom_id: null, reported_user_id: null },
            include: [
                {
                    model: Items,
                    as: "reportedItemDetails",
                    attributes: ['item_id', 'category_id', 'item_name', 'item_description'],
                    include: [
                        {
                            model: Categories,
                            as: "category",
                            attributes: ['category_id', 'category_name', 'parent_category_id', 'is_last_child'],
                        },
                        {
                            model: Media,
                            as: 'itemMedia',
                            attributes: ["media_id", "media_url", "file_type"],
                        }
                    ]
                },
                {
                    model: Users,
                    as: "reportedByUserDetails",
                    attributes: ['user_id', 'name', 'user_name']
                }
            ],
            attributes: {
                exclude: ['reported_showroom_id', 'reported_user_id']
            },
            order: [
                ['created_on', 'DESC']
            ]
        }).then(reportedItems => {
            resolve(reportedItems)
        }).catch((error) => reject(error))
    })
}

exports.getAllReportedShowrooms = () => {
    return new Promise((resolve, reject) => {
        ReportToAdmin.findAll({
            where: { reported_item_id: null, reported_user_id: null },
            include: [
                {
                    model: Showrooms,
                    as: "reportedShowroomDetails",
                    attributes: ['showroom_id', 'showroom_title', 'showroom_description'],
                    include: [
                        {
                            model: Media,
                            as: 'showroomMedia',
                            attributes: ["media_id", "media_url", "file_type"],
                        }
                    ]
                },
                {
                    model: Users,
                    as: "reportedByUserDetails",
                    attributes: ['user_id', 'name', 'user_name']
                }
            ],
            attributes: {
                exclude: ['reported_item_id', 'reported_user_id']
            },
            order: [
                ['created_on', 'DESC']
            ],
        }).then(reportedShowrooms => {
            resolve(reportedShowrooms)
        }).catch((error) => reject(error))
    })
}

exports.changeReportStatus = (body) => {
    return new Promise((resolve, reject) => {
        const { report_id, status } = body
        ReportToAdmin.update({ 'status': status }, { where: { 'report_id': report_id }, plain: true })
            .then((result) => {
                console.log(result[0])
                resolve(result[0])
            }).catch((error) => {
                console.log(error)
                reject(error)
            })
    })
}