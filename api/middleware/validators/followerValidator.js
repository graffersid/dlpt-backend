const { query, param, body } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'followUser': {
            return [
                body('followingUserId')
                    .exists().withMessage('followingUserId does not exists')
                    .notEmpty().withMessage('followingUserId is empty')
            ]
        }
        case 'unfollowUser': {
            return [
                param('followerId')
                    .exists().withMessage('followerId does not exists')
                    .notEmpty().withMessage('followerId is empty')
            ]
        }
    }
}