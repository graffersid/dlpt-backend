const { query, param, body } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'likeShowroom': {
            return [
                body('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
            ]
        }
        case 'likeItem': {
            return [
                body('userItemId')
                    .exists().withMessage('userItemId does not exists')
                    .notEmpty().withMessage('userItemId is empty')
            ]
        }
        case 'unLike': {
            return [
                param('likeId')
                    .exists().withMessage('likeId does not exists')
                    .notEmpty().withMessage('likeId is empty')
            ]
        }
    }
}