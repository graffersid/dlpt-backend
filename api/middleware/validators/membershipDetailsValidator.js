const { body } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'addMembership': {
            return [
                body('membershipDetails', 'membershipDetails does not exists').notEmpty(),
                body('membershipDetails.membership_name', 'membership_name does not exists').notEmpty(),
                body('membershipDetails.membership_plan_cost')
                    .exists().withMessage('membership_plan_cost does not exists')
                    .notEmpty().withMessage('membership_plan_cost is empty')
                    .isNumeric().withMessage('membership_plan_cost is not a number'),

                body('membershipDetails.membership_plan_duration')
                    .exists().withMessage('membership_plan_duration does not exists')
                    .notEmpty().withMessage('membership_plan_duration is empty')
                    .isNumeric().withMessage('membership_plan_duration is not a number'),

                body('membershipDetails.pics_per_item_inventory')
                    .exists().withMessage('pics_per_item_inventory does not exists')
                    .notEmpty().withMessage('pics_per_item_inventory is empty')
                    .isNumeric().withMessage('pics_per_item_inventory is not a number'),

                body('membershipDetails.videos_per_item_inventory')
                    .exists().withMessage('videos_per_item_inventory does not exists')
                    .notEmpty().withMessage('videos_per_item_inventory is empty')
                    .isNumeric().withMessage('videos_per_item_inventory is not a number'),

                body('membershipDetails.total_item_inventory')
                    .exists().withMessage('total_item_inventory does not exists')
                    .notEmpty().withMessage('total_item_inventory is empty')
                    .isNumeric().withMessage('total_item_inventory is not a number'),

                body('membershipDetails.inventory_image_size_limit')
                    .exists().withMessage('inventory_image_size_limit does not exists')
                    .notEmpty().withMessage('inventory_image_size_limit is empty')
                    .isNumeric().withMessage('inventory_image_size_limit is not a number'),

                body('membershipDetails.inventory_video_size_limit')
                    .exists().withMessage('inventory_video_size_limit does not exists')
                    .notEmpty().withMessage('inventory_video_size_limit is empty')
                    .isNumeric().withMessage('inventory_video_size_limit is not a number'),

                body('membershipDetails.total_post_showroom')
                    .exists().withMessage('total_post_showroom does not exists')
                    .notEmpty().withMessage('total_post_showroom is empty')
                    .isNumeric().withMessage('total_post_showroom is not a number'),

                body('membershipDetails.showroom_image_size_limit')
                    .exists().withMessage('showroom_image_size_limit does not exists')
                    .notEmpty().withMessage('showroom_image_size_limit is empty')
                    .isNumeric().withMessage('showroom_image_size_limit is not a number'),

                body('membershipDetails.showroom_video_size_limit')
                    .exists().withMessage('showroom_video_size_limit does not exists')
                    .notEmpty().withMessage('showroom_video_size_limit is empty')
                    .isNumeric().withMessage('showroom_video_size_limit is not a number'),

                body('membershipDetails.ranking')
                    .exists().withMessage('ranking does not exists')
                    .notEmpty().withMessage('ranking is empty')
                    .isBoolean().withMessage('ranking is not a boolean'),

                // body('membershipDetails.is_active')
                //     .exists().withMessage('is_active does not exists')
                //     .notEmpty().withMessage('is_active is empty')
                //     .isBoolean().withMessage('is_active is not a boolean'),

                body('marketingFieldDetails').custom(async (value, { req }) => {
                    if ("marketingFieldDetails" in req.body) {
                        if (value.length > 0) {
                            return true;
                        } else {
                            throw new Error('marketingFieldDetails is empty');
                        }
                    } else {
                        throw new Error('marketingFieldDetails does not exists')
                    }
                })
            ]
        }

        case 'updateMembership': {
            return [
                body('membershipDetails.membership_id')
                    .exists().withMessage('membership_id does not exists')
                    .notEmpty().withMessage('membership_id is empty')
                    .isNumeric().withMessage('membership_id is not a number'),
            ]
        }
        case 'banMembership': {
            return [
                body('membership_id')
                    .exists().withMessage('membership_id does not exists')
                    .notEmpty().withMessage('membership_id is empty')
                    .isNumeric().withMessage('membership_id is not a number'),
                body('is_active')
                    .exists().withMessage('is_active does not exists')
                    .notEmpty().withMessage('is_active is empty')
                    .isBoolean().withMessage('is_active is not a boolean'),
            ]
        }
        case 'addUserMembership': {
            return [
                body('membershipId')
                    .exists().withMessage('membershipId does not exists')
                    .notEmpty().withMessage('membershipId is empty')
                    .isNumeric().withMessage('membershipId is not a number')
            ]
        }
    }
}
