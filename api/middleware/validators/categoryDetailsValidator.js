const { body, query } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'addCategory': {
            return [
                body('categories').custom((value, { req }) => {
                    if ("categories" in req.body) {
                        if (Object.keys(value).length >= 3) {
                            return true;
                        } else {
                            throw new Error('categories is empty');
                        }
                    } else {
                        throw new Error('categories does not exists')
                    }
                }),
                body('itemFields').custom((value, { req }) => {
                    if ("itemFields" in req.body) {
                        if (value.length > 0) {
                            return true;
                        } else {
                            throw new Error('itemFields is empty');
                        }
                    } else {
                        throw new Error('itemFields does not exists')
                    }
                }),
                body('userFields').custom((value, { req }) => {
                    if ("userFields" in req.body) {
                        if (value.length > 0) {
                            return true;
                        } else {
                            throw new Error('userFields is empty');
                        }
                    } else {
                        throw new Error('userFields does not exists')
                    }
                })
            ]
        }
        case 'getCategoryDetails': {
            return [
                query('categoryId', 'categoryId does not exists').not().isEmpty().isNumeric(),
            ]
        }

        case 'editCategory': {
            return [
                body('userFields')
                    .exists().withMessage('userFields does not exists')
                    .notEmpty().withMessage('userFields is empty')
                    .isArray().withMessage('userFields is not a array'),

                body('itemFields')
                    .exists().withMessage('itemFields does not exists')
                    .notEmpty().withMessage('itemFields is empty')
                    .isArray().withMessage('itemFields is not a array'),
            ]
        }
    }
}
