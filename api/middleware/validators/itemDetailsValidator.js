const { query, param, body } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'getItemDetails': {
            return [
                query('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
            ]
        }
        case 'deleteItemDetails': {
            return [
                param('mediaId')
                    .exists().withMessage('mediaId does not exists')
                    .notEmpty().withMessage('mediaId is empty')
                    .isNumeric().withMessage('mediaId should be a number')
            ]
        }
        case 'searchItemsByName': {
            return [
                param('itemName')
                    .exists().withMessage('itemName does not exists')
                    .notEmpty().withMessage('itemName is empty')
            ]
        }
        case 'editItem': {
            return [
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
                body('itemName')
                    .exists().withMessage('itemName does not exists')
                    .notEmpty().withMessage('itemName is empty')
                    .isLength({ max: 65 }).withMessage('itemName should only 65 char long'),
                body('itemDescription')
                    .exists().withMessage('itemDescription does not exists')
                    .isLength({ max: 200 }).withMessage('itemDescription should only 200 char long')
                    .notEmpty().withMessage('itemDescription is empty'),
                body('categoryId')
                    .exists().withMessage('categoryId does not exists')
                    .notEmpty().withMessage('categoryId is empty')
                    .isNumeric().withMessage('categoryId is not a number'),
                body('itemFieldValues')
                    .exists().withMessage('itemFieldValues does not exists')
                    .notEmpty().withMessage('itemFieldValues is empty')
                    .isArray({ min: 1 }).withMessage('itemFieldValues is not a array')
            ]
        }

        case 'editUserItem': {
            return [
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
                body('itemName')
                    .exists().withMessage('itemName does not exists')
                    .notEmpty().withMessage('itemName is empty')
                    .isLength({ max: 65 }).withMessage('itemName should only 65 char long'),
                body('itemDescription')
                    .exists().withMessage('itemDescription does not exists')
                    .isLength({ max: 200 }).withMessage('itemDescription should only 200 char long')
                    .notEmpty().withMessage('itemDescription is empty'),
                body('userFieldValues')
                    .exists().withMessage('itemFieldValues does not exists')
                    .notEmpty().withMessage('itemFieldValues is empty')
                    .isArray({ min: 1 }).withMessage('itemFieldValues is not a array')
            ]
        }

        case 'blockItem': {
            return [
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),

                body('isActive')
                    .exists().withMessage('isActive does not exists')
                    .notEmpty().withMessage('isActive is empty')
                    .isBoolean().withMessage('isActive is not a boolean'),
            ]
        }
        case 'getItems': {
            return [
                query('skip')
                    .exists().withMessage('skip does not exists')
                    .notEmpty().withMessage('skip is empty')
                    .isNumeric().withMessage('skip is not a number'),
            ]
        }
        case 'userItemId': {
            return [
                body('userItemId')
                    .exists().withMessage('userItemId does not exists')
                    .notEmpty().withMessage('userItemId is empty')
                    .isNumeric().withMessage('userItemId is not a number'),
            ]
        }

        case 'changeItemPrivacy': {
            return [
                body('userItemId')
                    .exists().withMessage('userItemId does not exists')
                    .notEmpty().withMessage('userItemId is empty')
                    .isNumeric().withMessage('userItemId is not a number'),
                body('private')
                    .exists().withMessage('private does not exists')
                    .notEmpty().withMessage('private is empty')
                    .isBoolean().withMessage('private is not a boolean'),
            ]
        }
        case 'transferItem': {
            return [
                body('transferToUserId')
                    .exists().withMessage('transferToUserId does not exists')
                    .notEmpty().withMessage('transferToUserId is empty')
                    .isNumeric().withMessage('transferToUserId is not a number'),
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
            ]
        }
        case 'acceptItemRequest': {
            return [
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
                body('ownershipTransferByUser')
                    .exists().withMessage('ownershipTransferByUser does not exists')
                    .notEmpty().withMessage('ownershipTransferByUser is empty')
                    .isNumeric().withMessage('ownershipTransferByUser is not a number'),
                body('ownershipTransferId')
                    .exists().withMessage('ownershipTransferId does not exists')
                    .notEmpty().withMessage('ownershipTransferId is empty')
                    .isNumeric().withMessage('ownershipTransferId is not a number'),
            ]
        }
        case 'rejectItemRequest': {
            return [
                body('ownershipTransferId')
                    .exists().withMessage('ownershipTransferId does not exists')
                    .notEmpty().withMessage('ownershipTransferId is empty')
                    .isNumeric().withMessage('ownershipTransferId is not a number'),
            ]
        }

        case 'deleteItemMedia': {
            return [
                param('mediaId')
                    .exists().withMessage('mediaId does not exists')
                    .notEmpty().withMessage('mediaId is empty')
                    .isNumeric().withMessage('mediaId is not a number'),
                param('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
            ]
        }

        case 'reportItem': {
            return [
                body('reason')
                    .exists().withMessage('reason does not exists')
                    .notEmpty().withMessage('reason is empty'),
                body('itemId')
                    .exists().withMessage('itemId does not exists')
                    .notEmpty().withMessage('itemId is empty')
                    .isNumeric().withMessage('itemId is not a number'),
            ]
        }

        case 'searchDashboardItem': {
            return [
                query('itemName')
                    .exists().withMessage('showroomTitle does not exists')
                    .notEmpty().withMessage('showroomTitle is empty'),
                query('skip')
                    .exists().withMessage('skip does not exists')
                    .notEmpty().withMessage('skip is empty')
                    .isNumeric().withMessage('skip is not a number'),
            ]
        }
    }
}