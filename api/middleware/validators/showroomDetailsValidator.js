const { body, query, param } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'checkShowroomId': {
            return [
                query('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
            ]
        }
        case 'showroomId': {
            return [
                param('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
            ]
        }
        case 'pinShowroom': {
            return [
                body('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
            ]
        }
        case 'deleteShowroomMedia': {
            return [
                param('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
                param('mediaId')
                    .exists().withMessage('mediaId does not exists')
                    .notEmpty().withMessage('mediaId is empty')
                    .isNumeric().withMessage('mediaId is not a number'),
            ]
        }
        case 'editShowroom': {
            return [
                body('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
                body('title')
                    .exists().withMessage('title does not exists')
                    .notEmpty().withMessage('title is empty'),
                body('description')
                    .exists().withMessage('description does not exists')
                    .notEmpty().withMessage('description is empty')
            ]
        }

        case 'reportShowroom': {
            return [
                body('showroomId')
                    .exists().withMessage('showroomId does not exists')
                    .notEmpty().withMessage('showroomId is empty')
                    .isNumeric().withMessage('showroomId is not a number'),
                body('reason')
                    .exists().withMessage('reason does not exists')
                    .notEmpty().withMessage('reason is empty')
            ]
        }

        case 'searchDashboardShowrooms': {
            return [
                query('showroomTitle')
                    .exists().withMessage('showroomTitle does not exists')
                    .notEmpty().withMessage('showroomTitle is empty'),
                query('skip')
                    .exists().withMessage('skip does not exists')
                    .notEmpty().withMessage('skip is empty')
                    .isNumeric().withMessage('skip is not a number'),
            ]
        }
    }
}
