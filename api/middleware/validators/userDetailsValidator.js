const { body, query, param } = require('express-validator');

exports.validate = (method) => {
    switch (method) {
        case 'editProfile': {
            return [
                body('userName')
                    .exists().withMessage('userName does not exists')
                    .notEmpty().withMessage('userName is empty'),

                body('name')
                    .exists().withMessage('name does not exists')
                    .notEmpty().withMessage('name is empty'),

                body('dateOfBirth')
                    .exists().withMessage('dateOfBbirth does not exists')
                    .notEmpty().withMessage('dateOfBbirth is empty'),

                body('email')
                    .exists().withMessage('email does not exists')
                    .notEmpty().withMessage('email is empty')
                    .isEmail().withMessage('Invalid email'),

                body('gender')
                    .exists().withMessage('gender does not exists')
                    .notEmpty().withMessage('gender is empty'),

                body('country')
                    .exists().withMessage('country does not exists')
                    .notEmpty().withMessage('country is empty'),

                body('city')
                    .exists().withMessage('city does not exists')
                    .notEmpty().withMessage('city is empty'),

                body('zipCode')
                    .exists().withMessage('zipCode does not exists')
                    .notEmpty().withMessage('zipCode is empty'),
            ]
        }
        case 'editProfileByAdmin': {
            return [
                body('userId')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number'),

            ]
        }
        case 'checkUserId': {
            return [
                query('userId')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number'),
            ]
        }

        case 'UserId': {
            return [
                param('userId')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number'),
            ]
        }

        case 'blockUserId': {
            return [
                body('userId')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number'),

                body('isActive')
                    .exists().withMessage('isActive does not exists')
                    .notEmpty().withMessage('isActive is empty')
                    .isBoolean().withMessage('isActive is not a boolean'),
            ]
        }

        case 'addInterests': {
            return [
                body('interestsArray').custom(async (value, { req }) => {
                    if ("interestsArray" in req.body) {
                        if (value.length > 0) {
                            return true;
                        } else {
                            throw new Error('interestsArray is empty');
                        }
                    } else {
                        throw new Error('interestsArray does not exists')
                    }
                })

            ]
        }
        case 'editInterests': {
            return [
                body('interestsArray').custom(async (value, { req }) => {
                    if ("interestsArray" in req.body) {
                        if (value.length > 0) {
                            return true;
                        } else {
                            throw new Error('interestsArray is empty');
                        }
                    } else {
                        throw new Error('interestsArray does not exists')
                    }
                })

            ]
        }
        case 'deleteInterests': {
            return [
                query('interestId')
                    .exists().withMessage('interestId does not exists')
                    .notEmpty().withMessage('interestId is empty')
            ]
        }

        case 'changePassword': {
            return [
                body('password')
                    .exists().withMessage('password does not exists')
                    .notEmpty().withMessage('password is empty')
                    .isLength({ min: 8 }).withMessage('password must be at least 8 chars long'),

                body('confirmPassword')
                    .exists().withMessage('confirmPassword does not exists')
                    .notEmpty().withMessage('confirmPassword is empty')
                    .custom((value, { req }) => {
                        if (value !== req.body.password) {
                            throw new Error('confirm password does not match password');
                        }
                        // Indicates the success of this synchronous custom validator
                        return true;
                    })
            ]
        }

        case 'searchUser': {
            return [
                param('userName')
                    .exists().withMessage('userName does not exists')
                    .notEmpty().withMessage('userName is empty')
            ]
        }

        case 'reportUser': {
            return [
                body('user_id')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number'),
                body('reason')
                    .exists().withMessage('reason does not exists')
                    .notEmpty().withMessage('reason is empty')
            ]
        }

        case 'searchDashboardUser': {
            return [
                query('userName')
                    .exists().withMessage('userName does not exists')
                    .notEmpty().withMessage('userName is empty')
            ]
        }

        case 'getUserNotifications': {
            return [
                query('user_id')
                    .exists().withMessage('userId does not exists')
                    .notEmpty().withMessage('userId is empty')
                    .isNumeric().withMessage('userId is not a number')
            ]
        }

    }
}



