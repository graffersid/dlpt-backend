const { body, header } = require('express-validator')

exports.validate = (method) => {
    switch (method) {
        case 'registerUser': {
            return [
                // body('userName', 'userName doesnot exists').exists().not().isEmpty(),
                body('email')
                    .exists().withMessage('email does not exists')
                    .notEmpty().withMessage('email is empty')
                    .isEmail().withMessage('Invalid email'),

                body('password')
                    .exists().withMessage('password does not exists')
                    .notEmpty().withMessage('password is empty')
                    .isLength({ min: 8 }).withMessage('password must be at least 5 chars long'),

                body('confirmPassword')
                    .exists().withMessage('confirmPassword does not exists')
                    .notEmpty().withMessage('confirmPassword is empty')
                    .custom((value, { req }) => {
                        if (value !== req.body.password) {
                            throw new Error('confirm password does not match password');
                        }
                        // Indicates the success of this synchronous custom validator
                        return true;
                    })
            ]
        }
        case 'loginUser': {
            return [
                // body('userName', 'userName doesnot exists').exists().not().isEmpty(),
                body('email')
                    .exists().withMessage('email does not exists')
                    .notEmpty().withMessage('email is empty')
                    .isEmail().withMessage('Invalid email'),

                body('password')
                    .exists().withMessage('password does not exists')
                    .notEmpty().withMessage('password is empty')
            ]
        }
        case 'forgotPassword': {
            return [
                body('email')
                    .exists().withMessage('email does not exists')
                    .notEmpty().withMessage('email is empty')
                    .isEmail().withMessage('Invalid email')
            ]
        }
        case 'resetPassword': {
            return [
                body('token')
                    .exists().withMessage('token does not exists')
                    .notEmpty().withMessage('token is empty'),

                body('password')
                    .exists().withMessage('password does not exists')
                    .notEmpty().withMessage('password is empty')
                    .isLength({ min: 8 }).withMessage('password must be at least 8 chars long'),

                body('confirmPassword')
                    .exists().withMessage('confirmPassword does not exists')
                    .notEmpty().withMessage('confirmPassword is empty')
                    .custom((value, { req }) => {
                        if (value !== req.body.password) {
                            throw new Error('confirm password does not match password');
                        }
                        // Indicates the success of this synchronous custom validator
                        return true;
                    })
            ]
        }
        case 'headers': {
            return [
                header('access-token')
                    .exists().withMessage('Access token does not exists')
                    .notEmpty().withMessage('Access token is empty'),
            ]
        }
    }
}