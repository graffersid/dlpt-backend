const { validationResult } = require('express-validator');
const db = require('../../config/database');
const { Users } = require('../models')
const { logger } = require("../../config/logger");

const jwt = require('../lib/jwt');

exports.isUserValid = (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    const accessToken = req.headers['access-token'];

    Users.findOne({ attributes: ['access_token'], where: { access_token: accessToken } })
        .then(user => {
            if (user !== null) {
                next();
            } else {
                res.status(401).send({ status: false, message: "Unauthorized user" });
            }
        }).catch((error) => {
            logger.error('userAuthcheck.js -> isUserValid -> internal server error');
            res.status(500).send({ status: false, message: "Internal server error" })
        })

}

exports.isAdminValid = (req, res, next) => {
    jwt.verifyToken(req.headers['access-token']).then((result) => {
        if (!result.userId && !result.isAdmin) {
            res.status(401).send({ status: false, message: "Unauthorized user." });
        } else if (result.isAdmin == false) {
            res.status(401).send({ status: false, message: "Unauthorized user" });
        } else {
            next();
        }
    }).catch((error) => {
        logger.error(`${req.originalUrl} -> Error in fetching userId from token`, { obj: error });
        res.status(500).send({ status: false, message: error })
    });
}