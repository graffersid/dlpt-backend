var express = require('express');
var router = express.Router();
// require validators and middlewares
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const categoryDetailsValidator = require('../../middleware/validators/categoryDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers
const categoryDetailsController = require('../controllers/categoryDetailsController')

// -> get the list of all the categories 
router.get('/get_all_categories', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, categoryDetailsController.getAllcategories);

// -> Add Category
router.post('/add_category', userAuthValidator.validate("headers"), categoryDetailsValidator.validate("addCategory"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, categoryDetailsController.addCategory);

// -> get category details
router.get('/get_category_details', userAuthValidator.validate("headers"), categoryDetailsValidator.validate("getCategoryDetails"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, categoryDetailsController.getCategoryDetails);

// -> edit category
router.post('/edit_category', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, categoryDetailsController.editCategory);

module.exports = router;