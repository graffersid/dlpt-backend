var express = require('express');
var router = express.Router();
// require validatiors
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const userDetailsValidator = require('../../middleware/validators/userDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers 
const userDetailsController = require('../controllers/userDetailsController')

// -> Get the list of all the users 
router.get('/get_all_users', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.getAllUsers);

router.get('/profile/:userId', userAuthValidator.validate("headers"), userAuthCheck.isUserValid,
    userDetailsController.getProfileDetails)

// -> Update user profile 
router.put('/update_user_profile', userAuthValidator.validate("headers"), userDetailsValidator.validate("editProfile"),
    userDetailsValidator.validate("editProfileByAdmin"), userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.updateUsersProfile);

// -> Update user profile picture 
router.patch('/update_user_profile_picture', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.updateUsersProfilePicture);

router.patch('/block_user', userAuthValidator.validate("headers"), userDetailsValidator.validate("blockUserId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.blockUser);

// -> Get user current collections 
router.get('/get_user_current_collection', userAuthValidator.validate("headers"), userDetailsValidator.validate("checkUserId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.getUserCurrentCollection);

// -> Get user past collections
router.get('/get_user_past_collection', userAuthValidator.validate("headers"), userDetailsValidator.validate("checkUserId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.getUserpastCollection);

// -> Get user collection requests
router.get('/get_user_collection_request', userAuthValidator.validate("headers"), userDetailsValidator.validate("checkUserId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.getUserCollectionRequests);

// -> Get user showrooms
router.get('/get_user_showrooms', userAuthValidator.validate("headers"), userDetailsValidator.validate("checkUserId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, userDetailsController.getUserShowrooms);

module.exports = router;