var express = require('express');
var router = express.Router();
// require validatiors
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const showroomDetailsValidator = require('../../middleware/validators/showroomDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers 
const showroomDetailsController = require('../controllers/showroomDetailsController')


// -> Get user showrooms
router.get('/get_showroom_details', userAuthValidator.validate("headers"), showroomDetailsValidator.validate("checkShowroomId"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, showroomDetailsController.getShowroomDetails);

module.exports = router;