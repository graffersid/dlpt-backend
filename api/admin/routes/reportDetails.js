var express = require('express');
var router = express.Router();
// require validators and middlewares
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers
const reportDetailsController = require('../controllers/reportDetailsController')

// -> Get the list of all reported users 
router.get('/get_all_reported_users', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, reportDetailsController.getAllReportedUsers);

// -> Get the list of all reported items
router.get('/get_all_reported_items', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, reportDetailsController.getAllReportedItems);

// -> Get the list of all reported showrooms
router.get('/get_all_reported_showrooms', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, reportDetailsController.getAllReportedShowrooms);

router.patch('/change_report_status', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, reportDetailsController.changeReportStatus);

module.exports = router;