var express = require('express');
var router = express.Router();
// require validators and middlewares
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const itemDetailsValidator = require('../../middleware/validators/itemDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers
const itemDetailsController = require('../controllers/itemDetailsController')

// -> get the list of all the items 
router.get('/get_all_items', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, itemDetailsController.getAllItems);

router.get('/search_items_by_name/:itemName', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, itemDetailsValidator.validate("searchItemsByName"), userAuthCheck.isAdminValid, itemDetailsController.searchItemsByName);

// -> get specific item details
router.get('/get_item_detail', userAuthValidator.validate("headers"), userAuthCheck.isUserValid, userAuthCheck.isAdminValid,
    itemDetailsValidator.validate("getItemDetails"), itemDetailsController.getItemDetails);

// -> edit items
router.put('/edit_item', userAuthValidator.validate("headers"), itemDetailsValidator.validate("editItem"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, itemDetailsController.editItem);

// -> add item media
router.post('/add_item_media', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, itemDetailsController.addItemMedia);

// -> delete items media
router.delete('/delete_item_media/:mediaId', userAuthValidator.validate("headers"), itemDetailsValidator.validate("deleteItemDetails"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, itemDetailsController.deleteItemMedia);

router.patch('/block_item', userAuthValidator.validate("headers"), itemDetailsValidator.validate("blockItem"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, itemDetailsController.blockItem);

module.exports = router;