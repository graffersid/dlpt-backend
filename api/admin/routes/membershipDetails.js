var express = require('express');
var router = express.Router();
// require validators and middlewares
const userAuthValidator = require('../../middleware/validators/userAuthValidator')
const membershipDetailsValidator = require('../../middleware/validators/membershipDetailsValidator')
const userAuthCheck = require('../../middleware/userAuthCheck')
// require controllers
const membershipDetailsController = require('../controllers/membershipDetailsController')

// -> Get the list of all memberships 
router.get('/get_all_memberhships', userAuthValidator.validate("headers"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, membershipDetailsController.getAllMemberships);

// -> Add memberships 
router.post('/add_memberhship', userAuthValidator.validate("headers"), membershipDetailsValidator.validate("addMembership"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, membershipDetailsController.addMembership);

// -> Update memberships
router.put('/update_memberhship', userAuthValidator.validate("headers"), membershipDetailsValidator.validate("addMembership"),
    membershipDetailsValidator.validate("updateMembership"), userAuthCheck.isUserValid, userAuthCheck.isAdminValid, membershipDetailsController.updateMembership);

// -> restrict memberships
router.patch('/ban_membership', userAuthValidator.validate("headers"), membershipDetailsValidator.validate("banMembership"),
    userAuthCheck.isUserValid, userAuthCheck.isAdminValid, membershipDetailsController.banMembership);

module.exports = router;