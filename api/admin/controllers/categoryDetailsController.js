const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
// require services
const categoryDetailsService = require('../../services/categoryDetailsService')


exports.getAllcategories = async (req, res, next) => {

    categoryDetailsService.getAllCategories(req.query)
        .then(categories => {
            if (categories !== null) {
                res.status(200).send({ categories: categories, status: true, message: "Categories Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the categories." });
            }
        }).catch((error) => {
            logger.error('/admin/categories/get_all_categories -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });

}

exports.addCategory = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    categoryDetailsService.addCategory(req.body)
        .then(categories => {
            if (categories[0] !== null && categories[0] !== null) {
                res.status(200).send({ status: true, message: "Category added successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to add the category." });
            }
        }).catch((error) => {
            logger.error('/admin/categories/add_category -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });
}

exports.getCategoryDetails = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    categoryDetailsService.getCategoryDetails(req.query)
        .then(categoryDetails => {
            if (categoryDetails !== null) {
                res.status(200).send({ categoryDetails: categoryDetails, status: true, message: "Category details Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the categories details." });
            }
        }).catch((error) => {
            logger.error('/admin/categories/get_category_details -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });

}

exports.editCategory = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    if (req.body.itemFields.length == 0 && req.body.userFields.length == 0) {
        res.status(422).json({
            errors: [{
                "msg": "itemFields is empty",
                "param": "itemFields",
                "location": "body"
            },
            {
                "msg": "userFields is empty",
                "param": "userFields",
                "location": "body"
            }]
        });
    }

    categoryDetailsService.editCategory(req.body)
        .then(categoryDetails => {
            console.log(categoryDetails)
            if (categoryDetails !== null) {
                res.status(200).send({ status: true, message: "Category updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Categories is not update." });
            }
        }).catch((error) => {
            logger.error('/admin/categories/edit_category -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: 'Oops...Something Went Wrong!  Internal Server Error' })
        });

}
