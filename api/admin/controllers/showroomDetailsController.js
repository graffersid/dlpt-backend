const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

// require services
const showroomDetailsService = require('../../services/showroomDetailsService')

exports.getShowroomDetails = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    showroomDetailsService.getShowroomDetails(req.query)
        .then(showroomDetails => {
            if (showroomDetails !== null) {
                res.status(200).send({ showroomDetails: showroomDetails, status: true, message: "Showroom post details fetched successfully." });
            } else {
                res.status(400).send({ showroomDetails: {}, status: false, message: "Showroom post not Found" });
            }
        }).catch((error) => {
            logger.error('/admin/showrooms/get_showroom_details -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}
