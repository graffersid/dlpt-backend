const { validationResult } = require('express-validator');
const itemDetailsService = require('../../services/itemDetailsService')
const mediaDetailsService = require('../../services/mediaDetailsService')
const jwt = require('../../lib/jwt');
var formidable = require('formidable');
var path = require('path');
const fs = require('fs');
const { logger } = require("../../../config/logger");


exports.getAllItems = async (req, res, next) => {
    if (req.query.categoryId || req.query.searchString) {
        logger.info('/admin/items/get_all_items', { obj: req.query });
    } else {
        logger.info('/admin/items/get_all_items', { obj: req.query });
    }

    itemDetailsService.getAllItems(req.query)
        .then(items => {
            if (items.length > 0) {
                res.status(200).send({ Items: items, status: true, message: "Items Fetched successfully." });
            } else {
                res.status(200).send({ Items: [], status: false, message: "Items not found." });
            }
        }).catch((error) => {
            logger.error('/admin/items/get_all_items -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}

exports.searchItemsByName = async (req, res, next) => {

    itemDetailsService.searchItemsByName(req.params)
        .then(items => {
            if (items.length > 0) {
                res.status(200).send({ items: items, status: true, message: "Items Fetched successfully." });
            } else {
                res.status(200).send({ items: [], status: false, message: "Items not found." });
            }
        }).catch((error) => {
            logger.error('/admin/items/get_all_items -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}



exports.getItemDetails = async (req, res, next) => {
    logger.info('/admin/items/get_item_detail', { obj: req.query });
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    itemDetailsService.getItemDetails(req.query)
        .then(itemDetails => {
            if (itemDetails !== null) {
                res.status(200).send({ itemDetails: itemDetails, status: true, message: "Item details fetched successfully." });
            } else {
                res.status(400).send({ itemDetails: {}, status: false, message: "Item not found." });
            }
        }).catch((error) => {
            logger.error('/admin/items/get_item_detail -> Internal server error.');
            res.status(500).send({ status: false, message: error })
        });
}

exports.editItem = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    itemDetailsService.editItem(req.body)
        .then(itemDetails => {
            console.log(itemDetails)
            if (itemDetails == 1) {
                res.status(200).send({ status: true, message: "Item updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Item not updated." });
            }
        }).catch((error) => {
            logger.error('/admin/items/edit_item -> Internal server error.');
            res.status(500).send({ status: false, message: error })
        });
}

exports.addItemMedia = async (req, res, next) => {
    console.log("wdaesrgdtfyg")
    // -> Uploading Image
    // var form = new formidable.IncomingForm({ multiples: true });
    var files = [];
    var itemId;
    var form = new formidable.IncomingForm({ maxFileSize: 2000 * 1024 * 1024 });
    // form.maxFileSize = 3000 * 1024 * 1024;

    form.on('field', function (field, value) {
        if (field == 'itemId') {
            itemId = value;
        }
    });
    form.on('file', async function (field, file) {
        console.log("file                ", file)
        let fileType = file.type
        fileType = fileType.split("/", 1)[0]
        if (fileType) {
            if (!(fileType == "image" || fileType == "video")) {
                res.status(422).json({ status: false, message: 'fileType should be image or video.' });
                return;
            }
        } else {
            res.status(422).json({ status: false, message: 'Invalid file' });
            return;
        }


        function isImage(fileType) {
            switch (fileType) {
                case 'image/jpeg':
                case 'image/gif':
                case 'image/bmp':
                case 'image/png':
                case 'image/x-icon':
                    //etc
                    return true;
            }
            return false;
        }

        function isVideo(fileType) {
            switch (fileType) {
                case 'video/m4v':
                case 'video/avi':
                case 'video/mpg':
                case 'video/mp4':
                case 'video/x-matroska':
                    // etc
                    return true;
            }
            return false;
        }
        if (isImage(file.type) || isVideo(file.type)) {
            var oldpath = file.path;
            var extension = path.extname(file.name);
            var File = path.basename(file.name, extension);
            File = File + '-' + Date.now() + extension;
            files.push({ 'item_id': itemId, 'media_url': File, 'file_type': fileType })
            var newpath = `${path.join(__dirname, "../../../", "uploads/items")}/${File}`;
            fs.readFile(oldpath, (err, data) => {
                fs.writeFile(newpath, data, function (err) {
                    if (err) {
                        logger.error('/admin/items/add_item_media -> Something went wrong!', { obj: err });
                        res.status(500).send({ status: false, message: "Something went wrong!" });
                    }
                });
            });
        }
    });
    form.on('end', function () {
        console.log('done');
        files.filter((element) => {
            element.item_id = itemId
        })
        console.log(files)
        console.log("itemId             " + itemId)

        if (itemId) {
            if (isNaN(parseInt(itemId))) {
                res.status(422).json({ status: false, message: 'itemId is not a number.' });
                return;
            } else { itemId = parseInt(itemId) }
        } else {
            res.status(422).json({ status: false, message: 'itemId does not exist.' });
            return;
        }

        mediaDetailsService.addMedia(files)
            .then(itemMedia => {
                if (itemMedia !== null) {
                    res.status(200).send({ itemMedia: itemMedia, status: true, message: "Item media uploaded successfully" });
                } else {
                    res.status(400).send({ status: false, message: "Item media is not Uploaded" });
                }
            }).catch((error) => {
                logger.error('/admin/items/add_item_media -> Internal server error.', { obj: error });
                res.status(500).send({ status: false, message: error })
            })
    });
    form.parse(req);
}

exports.deleteItemMedia = async (req, res, next) => {

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    mediaDetailsService.deleteMedia(req.params)
        .then(deletedMedia => {
            if (deletedMedia.result == 1) {
                var filePath = `${path.join(__dirname, "../../../", "uploads/items")}/${deletedMedia.media.media_url}`;
                fs.unlink(filePath, (err) => {
                    if (err) {
                        res.status(404).send({ status: false, message: "Item media not found" });
                        console.error(err)
                        return
                    }
                    res.status(200).send({ status: true, message: "Item media deleted successfully" });
                })
            } else {
                res.status(404).send({ status: false, message: "Item media not found" });
            }
        }).catch((error) => {
            logger.error('/admin/items/delete_item_media -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}

exports.blockItem = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    itemDetailsService.blockItem(req.body)
        .then(item => {
            if (item !== null) {
                console.log(item.is_active)
                if (item.is_active == true) {
                    res.status(200).send({ status: true, message: "Item Unblock successfully." });
                } else {
                    res.status(200).send({ status: true, message: "Item block successfully." });
                }
            } else {
                res.status(400).send({ status: false, message: "Unable to block the Item" });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/update_memberhship -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}