const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const reportDetailsService = require('../../services/reportDetailsService')

exports.getAllReportedUsers = async (req, res, next) => {

    reportDetailsService.getAllReportedUsers()
        .then(reportedUsers => {
            if (reportedUsers !== null) {
                res.status(200).send({ reportedUsers: reportedUsers, status: true, message: "Reported users Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to Fetch the reported users." });
            }
        }).catch((error) => {
            logger.error('/admin/reports/get_all_reported_users -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

exports.getAllReportedItems = async (req, res, next) => {

    reportDetailsService.getAllReportedItems()
        .then(reportedItems => {
            if (reportedItems !== null) {
                res.status(200).send({ reportedItems: reportedItems, status: true, message: "Reported items Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to Fetch the reported items." });
            }
        }).catch((error) => {
            console.log(error)
            logger.error('/admin/reports/get_all_reported_items -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

exports.getAllReportedShowrooms = async (req, res, next) => {

    reportDetailsService.getAllReportedShowrooms()
        .then(reportedShowrooms => {
            if (reportedShowrooms !== null) {
                res.status(200).send({ reportedShowrooms: reportedShowrooms, status: true, message: "Reported showrooms Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to Fetch the reported showrooms." });
            }
        }).catch((error) => {
            logger.error('/admin/reports/get_all_reported_showrooms -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

exports.changeReportStatus = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    reportDetailsService.changeReportStatus(req.body)
        .then(report => {
            if (report === 1) {
                res.status(200).send({ status: true, message: "Report updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Report is not found" });
            }
        }).catch((error) => {
            logger.error('/admin/reports/get_all_reported_showrooms -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}