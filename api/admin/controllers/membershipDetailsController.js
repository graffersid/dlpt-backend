const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");

const membershipDetailsService = require('../../services/membershipDetailsService')

exports.getAllMemberships = async (req, res, next) => {

    membershipDetailsService.getAllMemberships(req.query)
        .then(memberships => {
            if (memberships !== null) {
                res.status(200).send({ memberships: memberships, status: true, message: "Memberships Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the memberships." });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/get_all_memberhships -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

exports.addMembership = async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    await asyncForEach(req.body.marketingFieldDetails, async function (element, index) {
        if (!("membership_id" in element)) {
            res.status(422).json({
                errors: [{
                    "value": element.membership_id,
                    "msg": "membership_id does not exists",
                    "param": "marketingFieldDetails.membership_id",
                    "location": "body"
                }]
            });
        } else if (!("marketing_field_name" in element)) {
            res.status(422).json({
                errors: [{
                    "value": element.marketing_field_name,
                    "msg": "marketing_field_name does not exists",
                    "param": "marketingFieldDetails.marketing_field_name",
                    "location": "body"
                }]
            });
        }
    })

    membershipDetailsService.addMembership(req.body)
        .then(memberships => {
            if (memberships !== null) {
                res.status(200).send({ status: true, message: "Membership added successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to add the membership." });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/add_memberhship -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}

exports.updateMembership = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    await asyncForEach(req.body.marketingFieldDetails, async function (element, index) {
        if (!("membership_id" in element)) {
            res.status(422).json({
                errors: [{
                    "value": element.membership_id,
                    "msg": "membership_id does not exists",
                    "param": "marketingFieldDetails.membership_id",
                    "location": "body"
                }]
            });
        } else if (!("marketing_field_name" in element)) {
            res.status(422).json({
                errors: [{
                    "value": element.marketing_field_name,
                    "msg": "marketing_field_name does not exists",
                    "param": "marketingFieldDetails.marketing_field_name",
                    "location": "body"
                }]
            });
        } else if (!("marketing_field_id" in element)) {
            res.status(422).json({
                errors: [{
                    "value": element.marketing_field_id,
                    "msg": "marketing_field_id does not exists",
                    "param": "marketingFieldDetails.marketing_field_id",
                    "location": "body"
                }]
            });
        }
    })

    membershipDetailsService.updateMembership(req.body)
        .then(memberships => {
            if (memberships !== null) {
                res.status(200).send({ status: true, message: "Membership updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to updated the membership." });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/update_memberhship -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

exports.banMembership = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    membershipDetailsService.banMembership(req.body)
        .then(memberships => {
            if (memberships !== null) {
                res.status(200).send({ status: true, message: "Membership updated successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to updated the membership." });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/update_memberhship -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });

}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};