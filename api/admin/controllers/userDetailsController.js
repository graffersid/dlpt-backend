const { validationResult } = require('express-validator');
const { logger } = require("../../../config/logger");
var formidable = require('formidable');
var path = require('path');
const fs = require('fs');
// require services
const userDetailsService = require('../../services/userDetailsService')
const showroomDetailsService = require('../../services/showroomDetailsService')


exports.getAllUsers = async (req, res, next) => {

    userDetailsService.getAllUsers(req.query)
        .then(users => {
            if (users !== null) {
                res.status(200).send({ users: users, status: true, message: "Users Fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to get the users." });
            }
        }).catch((error) => {
            console.log(error);
            logger.error('/admin/users/get_all_users -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}
exports.getProfileDetails = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getUserProfileDetails(req.params.userId)
        .then(user => {
            if (user !== null) {
                res.status(200).send({ userDetails: user, status: true, message: "Fetched profile details successfully." });
            } else {
                res.status(400).send({ status: false, message: "unable to get the profile Details" });
            }
        }).catch((error) => {
            logger.error('admin/users/profile/:userId -> Get API -> Internal server error. ', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}


exports.updateUsersProfile = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    const { userId, userName, email } = req.body;

    // -> Check for Unique User email
    const isUserEmailUnique = userDetailsService.isUserEmailUnique(email, userId)
    // -> Check for Unique User name
    const isUsernameUnique = userDetailsService.isUsernameUnique(userName, userId)

    Promise.all([isUserEmailUnique, isUsernameUnique]).then((values) => {
        if (values[0]) {
            res.status(400).send({ status: false, message: "Email is already in use." });
        } else if (values[1]) {
            res.status(400).send({ status: false, message: "UserName is already in use." });
        } else {
            // -> Saving the new details of user 
            userDetailsService.setUserProfileDetails(userId, req.body)
                .then(user => {
                    if (user !== null) {
                        res.status(200).send({ userDetails: user, status: true, message: "user profile updated successfully." });
                    } else {
                        res.status(400).send({ status: false, message: "user profile is not updated." });
                    }
                }).catch((error) => {
                    logger.error('/admin/users/update_user_profile -> Internal server error. ', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        }
    }).catch((error) => {
        logger.error('/admin/users/update_user_profile -> Internal server error. ', { obj: error });
        res.status(500).send({ status: false, message: error })
    })
}

exports.updateUsersProfilePicture = async (req, res, next) => {

    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    // -> Uploading Image
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        let { userId } = fields;
        if (userId) {
            if (isNaN(parseInt(userId))) {
                res.status(422).json({ status: false, message: 'userId is not a number.' });
                return;
            } else { userId = parseInt(userId) }
        } else {
            res.status(422).json({ status: false, message: 'userId does not exist.' });
            return;
        }
        if ("profilePicture" in files) {
            if (!files.profilePicture.name) {
                res.status(422).json({ status: false, message: 'Upload the image' });
                return;
            }
        } else {
            res.status(422).json({ status: false, message: 'Upload the image' });
            return;
        }
        var oldpath = files.profilePicture.path;
        var extension = path.extname(files.profilePicture.name);
        var file = path.basename(files.profilePicture.name, extension);
        file = file + '-' + Date.now() + extension;

        var newpath = `${path.join(__dirname, "../../../", "uploads/profilePicture")}/${file
            }`;
        var rawData = fs.readFileSync(oldpath)

        fs.writeFile(newpath, rawData, function (err) {
            if (err) {
                logger.error('/admin/users/update_user_profile_picture -> Something went wrong!', { obj: err });
                res.status(400).send({ status: false, message: "Something went wrong!" });
            }
            userDetailsService.updateUserImage(file, userId)
                .then(user => {
                    if (user !== null) {
                        res.status(200).send({ userDetails: user, status: true, message: "profile picture updated successfully" });
                    } else {
                        res.status(400).send({ status: false, message: "profile picture is not Updated" });
                    }
                }).catch((error) => {
                    logger.error('/admin/users/update_user_profile_picture -> Internal server error.', { obj: error });
                    res.status(500).send({ status: false, message: error })
                })
        });
    })

}

exports.blockUser = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }
    userDetailsService.blockUser(req.body)
        .then(user => {
            if (user !== null) {
                console.log(user.is_active)
                if (user.is_active == true) {
                    res.status(200).send({ status: true, message: "User Unblock successfully." });
                } else {
                    res.status(200).send({ status: true, message: "User block successfully." });
                }
            } else {
                res.status(400).send({ status: false, message: "Unable to block the user" });
            }
        }).catch((error) => {
            logger.error('/admin/memberships/update_memberhship -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        });
}

exports.getUserCurrentCollection = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getUserCurrentCollection(req.query, 0, req.query)
        .then(userCurrentCollection => {
            if (userCurrentCollection !== null) {
                res.status(200).send({ userCurrentCollection: userCurrentCollection.items, status: true, message: "User current collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User current collection." });
            }
        }).catch((error) => {
            logger.error('/admin/users/get_user_current_collection -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}

exports.getUserpastCollection = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getUserpastCollection(req.query, 0, req.query)
        .then(userPastCollection => {
            if (userPastCollection !== null) {
                res.status(200).send({ userPastCollection: userPastCollection.items, status: true, message: "User past collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User past collection." });
            }
        }).catch((error) => {
            logger.error('/admin/users/get_user_past_collection -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getUserShowrooms = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        logger.error('/admin/users/get_user_showrooms', { obj: errors });
        res.status(422).json({ errors: errors.array() });
        return;
    }

    showroomDetailsService.getUserShowrooms(req.query, 0, req.query)
        .then(userShowrooms => {
            if (userShowrooms !== null) {
                res.status(200).send({ userShowrooms: userShowrooms.showrooms, status: true, message: "User showrooms fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User showrooms." });
            }
        }).catch((error) => {
            logger.error('/admin/users/get_user_showrooms -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })

}

exports.getUserCollectionRequests = async (req, res, next) => {
    // -> request validation check
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
    }

    userDetailsService.getUserCollectionRequests(req.query)
        .then(userCollectionRequests => {
            if (userCollectionRequests !== null) {
                res.status(200).send({ userCollectionRequests: userCollectionRequests, status: true, message: "User current collection fetched successfully." });
            } else {
                res.status(400).send({ status: false, message: "Unable to fetched the User current collection." });
            }
        }).catch((error) => {
            logger.error('/admin/users/get_user_collection_request -> Internal server error.', { obj: error });
            res.status(500).send({ status: false, message: error })
        })
}