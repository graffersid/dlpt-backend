var express = require('express');
var router = express.Router();
// require routes
const itemDetails = require('./routes/itemDetails');
const userDetails = require('./routes/userDetails');
const categoryDetails = require('./routes/categoryDetails');
const membershipDetails = require('./routes/membershipDetails');
const reportDetails = require('./routes/reportDetails');
const showroomDetails = require('./routes/showroomDetails');

// ->  Items details
router.use('/items', itemDetails)

// ->  Users details
router.use('/users', userDetails)

// ->  Categories details
router.use('/categories', categoryDetails)

// ->  Memberships details
router.use('/memberships', membershipDetails)

// ->  Reports details
router.use('/reports', reportDetails)

// ->  Showrooms details
router.use('/showrooms', showroomDetails)

module.exports = router;
