module.exports = (db, Sequelize) => {
    const Likes = db.define('likes', {

        like_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_item_id: {
            type: Sequelize.INTEGER
        },
        showroom_id: {
            type: Sequelize.INTEGER
        },
        liked_by: {
            type: Sequelize.INTEGER
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }

    })

    return Likes;
}