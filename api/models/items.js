module.exports = (db, Sequelize) => {
    const Items = db.define('items', {
        item_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        current_user_id: {
            type: Sequelize.INTEGER
        },
        category_id: {
            type: Sequelize.INTEGER
        },
        item_name: {
            type: Sequelize.STRING
        },
        item_description: {
            type: Sequelize.STRING
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })

    return Items;
}