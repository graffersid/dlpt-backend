module.exports = (db, Sequelize) => {
    const Categories = db.define('categories', {
        category_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category_name: {
            type: Sequelize.STRING
        },
        parent_category_id: {
            type: Sequelize.INTEGER
        },
        // hierarchy_level: {
        //     type: Sequelize.INTEGER
        // },
        is_last_child: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
        // hierarchy: true
    });

    return Categories;
}