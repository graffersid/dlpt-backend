module.exports = (db, Sequelize) => {
    const OwnershipTransfer = db.define('ownership_transfers', {
        ownership_transfer_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ownership_transfer_to_user: {
            type: Sequelize.INTEGER
        },
        ownership_transfer_by_user: {
            type: Sequelize.INTEGER
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        request_status_id: {
            type: Sequelize.INTEGER
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return OwnershipTransfer;
}