module.exports = (db, Sequelize) => {
    const Followers = db.define('followers', {
        follower_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        following_user_id: {
            type: Sequelize.INTEGER
        },
        followed_by_user_id: {
            type: Sequelize.INTEGER
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    })

    return Followers;
}