module.exports = (db, Sequelize) => {
    const UserItems = db.define('user_items', {
        user_item_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        total_views_count: {
            type: Sequelize.INTEGER
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        },
        is_past_item: {
            type: Sequelize.BOOLEAN
        },
        is_private: {
            type: Sequelize.BOOLEAN
        },
        is_item_completed: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },

    })
    return UserItems;
}