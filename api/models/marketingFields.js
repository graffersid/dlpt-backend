module.exports = (db, Sequelize) => {
    const MarketingFields = db.define('marketing_fields', {
        marketing_field_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        membership_id: {
            type: Sequelize.INTEGER
        },
        marketing_field_name: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        },
    })
    return MarketingFields;
}