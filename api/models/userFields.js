module.exports = (db, Sequelize) => {
    const UserFields = db.define('user_fields', {
        user_field_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category_id: {
            type: Sequelize.INTEGER
        },
        user_field_name: {
            type: Sequelize.INTEGER
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return UserFields;
}