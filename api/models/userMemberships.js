module.exports = (db, Sequelize) => {
    const UserMemberships = db.define('user_memberships', {
        user_membership_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        membership_id: {
            type: Sequelize.INTEGER
        },
        membership_name: {
            type: Sequelize.STRING
        },
        membership_plan_cost: {
            type: Sequelize.INTEGER
        },
        membership_plan_duration: {
            type: Sequelize.INTEGER
        },
        pics_per_item_inventory: {
            type: Sequelize.INTEGER
        },
        videos_per_item_inventory: {
            type: Sequelize.INTEGER
        },
        total_item_inventory: {
            type: Sequelize.INTEGER
        },
        inventory_image_size_limit: {
            type: Sequelize.INTEGER
        },
        inventory_video_size_limit: {
            type: Sequelize.INTEGER
        },
        total_post_showroom: {
            type: Sequelize.INTEGER
        },
        showroom_image_size_limit: {
            type: Sequelize.INTEGER
        },
        showroom_video_size_limit: {
            type: Sequelize.INTEGER
        },
        ranking: {
            type: Sequelize.BOOLEAN
        },
        show_interests: {
            type: Sequelize.BOOLEAN
        },
        membership_expiration_date: {
            type: Sequelize.DATE
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    })

    return UserMemberships;
}