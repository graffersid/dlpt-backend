module.exports = (db, Sequelize) => {
    const ItemFieldValues = db.define('item_field_values', {

        item_field_value_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        item_field_id: {
            type: Sequelize.INTEGER
        },
        item_field_value: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return ItemFieldValues;
}