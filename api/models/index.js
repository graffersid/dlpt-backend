const Sequelize = require('sequelize')
// require('sequelize-hierarchy')(Sequelize);
const db = require('../../config/database');

// -> Import Models
const Users = require('./users')(db, Sequelize);
const Memberships = require('./memberships')(db, Sequelize);
const MarketingFields = require('./marketingFields')(db, Sequelize);
const UserMemberships = require('./userMemberships')(db, Sequelize);
const Followers = require('./followers')(db, Sequelize);
const Showrooms = require('./showrooms')(db, Sequelize);
const Media = require('./media')(db, Sequelize);
const UserInterests = require('./userInterests')(db, Sequelize);
const Categories = require('./categories')(db, Sequelize);
const Items = require('./items')(db, Sequelize);
const UserItems = require('./userItems')(db, Sequelize);
const Likes = require('./likes')(db, Sequelize);
const ItemFields = require('./itemFields')(db, Sequelize);
const ItemFieldValues = require('./itemFieldValues')(db, Sequelize);
const UserFields = require('./userFields')(db, Sequelize);
const UserFieldValues = require('./userFieldValues')(db, Sequelize);
const ReportToAdmin = require('./reportToAdmin')(db, Sequelize);
const OwnershipTransfer = require('./ownershipTransfer')(db, Sequelize);
const OwnershipTransferStatus = require('./ownershipTransferStatus')(db, Sequelize);
const Notifications = require('./notifications')(db, Sequelize);


// -> Relationship Between The Tables

// -> Associtaion Relationship of memberships table
Memberships.hasMany(MarketingFields, { as: "marketingFields", foreignKey: 'membership_id' });
MarketingFields.belongsTo(Memberships, { foreignKey: 'membership_id', as: 'membership' })

// -> Associtaion Relationship of user_memberships table
Users.belongsToMany(Memberships, { through: UserMemberships, foreignKey: 'membership_id' });
Memberships.belongsToMany(Users, { through: UserMemberships, foreignKey: 'user_id' });

Users.hasMany(UserMemberships, { foreignKey: 'user_id', as: 'userMemberships' });

UserMemberships.belongsTo(Users, { foreignKey: 'user_id', as: 'membershipUserDetails' })
UserMemberships.belongsTo(Memberships, { foreignKey: 'membership_id', as: 'membership' })

// -> Associtaion Relationship of followers table
Users.hasMany(Followers, { as: "followers", foreignKey: 'following_user_id' });
Users.hasMany(Followers, { as: "following", foreignKey: 'followed_by_user_id' });

Followers.belongsTo(Users, { foreignKey: 'following_user_id', as: 'followingUsers' })
Followers.belongsTo(Users, { foreignKey: 'followed_by_user_id', as: 'followedByUsers' })

// -> Associtaion Relationship of showrooms table
Users.hasMany(Showrooms, { foreignKey: 'user_id' });
Showrooms.belongsTo(Users, { foreignKey: 'user_id', as: "showroomUserDetails" })

// -> Associtaion Relationship of Ccategories table
Categories.hasMany(Categories, { as: 'subCategories', foreignKey: 'parent_category_id' })
Categories.belongsTo(Categories, { as: 'parentCategory', foreignKey: 'parent_category_id' })
// Categories.isHierarchy();

// -> Associtaion Relationship of items table
Users.hasMany(Items, { as: "userItems", foreignKey: 'current_user_id' })
Items.belongsTo(Users, { as: "currentUser", foreignKey: 'current_user_id' })
Categories.hasMany(Items, { foreignKey: 'category_id' })
Items.belongsTo(Categories, { as: 'category', foreignKey: 'category_id' })

// -> Associtaion Relationship of media table
Showrooms.hasMany(Media, { as: "showroomMedia", foreignKey: 'showroom_id' });
Media.belongsTo(Showrooms, { onDelete: 'cascade', hooks: true, foreignKey: 'showroom_id' })

Items.hasMany(Media, { as: "itemMedia", foreignKey: 'item_id' });
Media.belongsTo(Items, { foreignKey: 'item_id' })

// -> Associtaion Relationship of user_interests table
Users.belongsToMany(Categories, { through: UserInterests, foreignKey: 'category_id' });
Categories.belongsToMany(Users, { through: UserInterests, foreignKey: 'user_id' });

UserInterests.belongsTo(Users, { foreignKey: 'user_id' })
UserInterests.belongsTo(Categories, { foreignKey: 'category_id' })

// -> Associtaion Relationship of Likes table
Showrooms.hasMany(Likes, { foreignKey: 'showroom_id', as: "showroomLikes" })
Likes.belongsTo(Showrooms, { onDelete: 'cascade', hooks: true, foreignKey: 'showroom_id' })

UserItems.hasMany(Likes, { foreignKey: 'user_item_id', as: "userItemLikes" })
Likes.belongsTo(UserItems, { foreignKey: 'user_item_id' })

Users.hasMany(Likes, { foreignKey: 'liked_by' })
Likes.belongsTo(Users, { foreignKey: 'liked_by' })

// -> Associtaion Relationship of user_items table
Users.belongsToMany(Items, { as: "userItems1", through: UserItems, foreignKey: 'item_id' });
Items.belongsToMany(Users, { as: "ItemUser", through: UserItems, foreignKey: 'user_id' });

Items.hasMany(UserItems, { as: "itemUsers", foreignKey: 'item_id' })

UserItems.belongsTo(Users, { as: 'itemUserDetails', foreignKey: 'user_id' })
UserItems.belongsTo(Items, { as: "itemsDetails", foreignKey: 'item_id' })


// -> Associtaion Relationship of item_fields table
Categories.hasMany(ItemFields, { as: "itemFields", foreignKey: 'category_id' })
ItemFields.belongsTo(Categories, { foreignKey: 'category_id' })

// -> Associtaion Relationship of item_field_values table
Items.belongsToMany(ItemFields, { as: "itemField", through: ItemFieldValues, foreignKey: 'item_field_id' });
ItemFields.belongsToMany(Items, { through: ItemFieldValues, foreignKey: 'item_id' });

ItemFields.hasMany(ItemFieldValues, { as: "itemFieldValue", foreignKey: 'item_field_id' });

ItemFieldValues.belongsTo(Items, { foreignKey: 'item_id' })
ItemFieldValues.belongsTo(ItemFields, { foreignKey: 'item_field_id' })

// -> Associtaion Relationship of user_fields table
Categories.hasMany(UserFields, { as: "userFields", foreignKey: 'category_id' })
UserFields.belongsTo(Categories, { foreignKey: 'category_id' })

// -> Associtaion Relationship of user_field_values table
UserItems.belongsToMany(UserFields, { as: "userField", through: UserFieldValues, foreignKey: 'user_field_value_id' });
UserFields.belongsToMany(UserItems, { through: UserFieldValues, foreignKey: 'user_item_id' });

UserFieldValues.belongsTo(UserItems, { foreignKey: 'user_item_id' })

UserFields.hasMany(UserFieldValues, { as: "userFieldvalue", foreignKey: 'user_field_id' })
UserFieldValues.belongsTo(UserFields, { foreignKey: 'user_field_id' })

// -> Associtaion Relationship of report_to_admin table
Users.hasMany(ReportToAdmin, { foreignKey: 'reported_user_id' })
ReportToAdmin.belongsTo(Users, { as: "reportedUserDetails", foreignKey: 'reported_user_id' })

Users.hasMany(ReportToAdmin, { foreignKey: 'reported_by_user' })
ReportToAdmin.belongsTo(Users, { as: "reportedByUserDetails", foreignKey: 'reported_by_user' })

Showrooms.hasMany(ReportToAdmin, { foreignKey: 'reported_showroom_id' })
ReportToAdmin.belongsTo(Showrooms, { onDelete: 'cascade', hooks: true, as: "reportedShowroomDetails", foreignKey: 'reported_showroom_id' })

Items.hasMany(ReportToAdmin, { foreignKey: 'reported_item_id' })
ReportToAdmin.belongsTo(Items, { as: "reportedItemDetails", foreignKey: 'reported_item_id' })

// -> Associtaion Relationship of ownership_transfer table
OwnershipTransferStatus.hasMany(OwnershipTransfer, { foreignKey: 'request_status_id' })
OwnershipTransfer.belongsTo(OwnershipTransferStatus, { as: "requestStatus", foreignKey: 'request_status_id' })

Users.hasMany(OwnershipTransfer, { foreignKey: 'ownership_transfer_to_user' })
OwnershipTransfer.belongsTo(Users, { foreignKey: 'ownership_transfer_to_user' })

Users.hasMany(OwnershipTransfer, { foreignKey: 'ownership_transfer_by_user' })
OwnershipTransfer.belongsTo(Users, { foreignKey: 'ownership_transfer_by_user' })

Items.hasMany(OwnershipTransfer, { foreignKey: 'item_id' })//
OwnershipTransfer.belongsTo(Items, { as: "requestItem", foreignKey: 'item_id' })//

// -> Associtaion Relationship of ownership_transfer table
Users.hasMany(Notifications, { foreignKey: 'user_id' });
Notifications.belongsTo(Users, { foreignKey: 'user_id' });

Items.hasMany(Notifications, { foreignKey: 'item_id' });
Notifications.belongsTo(Items, { as: "notificationItemDetails", foreignKey: 'item_id' });

Showrooms.hasMany(Notifications, { foreignKey: 'showroom_id' });
Notifications.belongsTo(Showrooms, { as: "notificationShowroomDetails", foreignKey: 'showroom_id' });

UserMemberships.hasMany(Notifications, { foreignKey: 'user_membership_id' });
Notifications.belongsTo(UserMemberships, { foreignKey: 'user_membership_id' });


module.exports = {
    Users,
    Memberships,
    MarketingFields,
    UserMemberships,
    Followers,
    Showrooms,
    Media,
    UserInterests,
    Categories,
    Items,
    UserItems,
    Likes,
    ItemFields,
    ItemFieldValues,
    UserFields,
    UserFieldValues,
    ReportToAdmin,
    OwnershipTransfer,
    OwnershipTransferStatus,
    Notifications
}