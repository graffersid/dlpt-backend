module.exports = (db, Sequelize) => {
    const Memberships = db.define('memberships', {
        membership_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        membership_name: {
            type: Sequelize.STRING
        },
        membership_plan_cost: {
            type: Sequelize.INTEGER
        },
        membership_plan_duration: {
            type: Sequelize.INTEGER
        },
        pics_per_item_inventory: {
            type: Sequelize.INTEGER
        },
        videos_per_item_inventory: {
            type: Sequelize.INTEGER
        },
        total_item_inventory: {
            type: Sequelize.INTEGER
        },
        inventory_image_size_limit: {
            type: Sequelize.INTEGER
        },
        inventory_video_size_limit: {
            type: Sequelize.INTEGER
        },
        total_post_showroom: {
            type: Sequelize.INTEGER
        },
        showroom_image_size_limit: {
            type: Sequelize.INTEGER
        },
        showroom_video_size_limit: {
            type: Sequelize.INTEGER
        },
        ranking: {
            type: Sequelize.BOOLEAN
        },
        description: {
            type: Sequelize.STRING
        },
        show_interests: {
            type: Sequelize.BOOLEAN
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    })

    return Memberships;
}