module.exports = (db, Sequelize) => {
    const Showrooms = db.define('showrooms', {
        showroom_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        showroom_title: {
            type: Sequelize.STRING
        },
        showroom_description: {
            type: Sequelize.STRING
        },
        total_views_count: {
            type: Sequelize.INTEGER
        },
        is_pinned: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        },
        is_active: {
            type: Sequelize.BOOLEAN
        }
    }, {
        underscored: true,
    });

    return Showrooms;
}