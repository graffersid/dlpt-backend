module.exports = (db, Sequelize) => {
    const Users = db.define('users', {
        user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_name: {
            type: Sequelize.STRING
        },
        name: {
            type: Sequelize.STRING
        },
        user_email: {
            type: Sequelize.STRING
        },
        user_password: {
            type: Sequelize.STRING
        },
        user_image: {
            type: Sequelize.STRING
        },
        user_gender: {
            type: Sequelize.STRING
        },
        user_date_of_birth: {
            type: Sequelize.DATEONLY
        },
        user_city: {
            type: Sequelize.STRING
        },
        user_country: {
            type: Sequelize.STRING
        },
        user_zip_code: {
            type: Sequelize.STRING
        },
        is_active: {
            type: Sequelize.BOOLEAN
        },
        is_admin: {
            type: Sequelize.BOOLEAN
        },
        access_token: {
            type: Sequelize.STRING
        },
        reset_password_token: {
            type: Sequelize.STRING
        },
        reset_password_expires: {
            type: Sequelize.DATE
        },
        is_profile_completed: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    }, {
        underscored: true,
    });

    return Users;
}