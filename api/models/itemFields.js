module.exports = (db, Sequelize) => {
    const ItemFields = db.define('item_fields', {

        item_field_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category_id: {
            type: Sequelize.INTEGER
        },
        item_field_name: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return ItemFields;
}