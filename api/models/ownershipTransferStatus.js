module.exports = (db, Sequelize) => {
    const OwnershipTransferStatus = db.define('ownership_transfer_statuses', {

        ownership_transfer_status_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status_name: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return OwnershipTransferStatus;
}