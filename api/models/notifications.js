module.exports = (db, Sequelize) => {
    const Notifications = db.define('notifications', {

        notification_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        showroom_id: {
            type: Sequelize.INTEGER
        },
        user_membership_id: {
            type: Sequelize.INTEGER
        },
        notification_message: {
            type: Sequelize.STRING
        },
        is_read: {
            type: Sequelize.BOOLEAN
        },
        notification_type: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }

    })
    return Notifications;
}