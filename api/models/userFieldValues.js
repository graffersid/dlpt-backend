module.exports = (db, Sequelize) => {
    const UserFieldValues = db.define('user_field_values', {
        user_field_value_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_item_id: {
            type: Sequelize.INTEGER
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        user_field_id: {
            type: Sequelize.INTEGER
        },
        user_field_value: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })
    return UserFieldValues;
}