module.exports = (db, Sequelize) => {
    const Media = db.define('media', {
        media_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        item_id: {
            type: Sequelize.INTEGER
        },
        showroom_id: {
            type: Sequelize.INTEGER
        },
        media_url: {
            type: Sequelize.STRING
        },
        file_type: {
            type: Sequelize.STRING
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }
    })

    return Media;
}