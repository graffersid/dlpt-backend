module.exports = (db, Sequelize) => {
    const ReportToAdmin = db.define('report_to_admins', {

        report_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        reported_user_id: {
            type: Sequelize.INTEGER
        },
        reported_showroom_id: {
            type: Sequelize.INTEGER
        },
        reported_item_id: {
            type: Sequelize.INTEGER
        },
        reported_by_user: {
            type: Sequelize.INTEGER
        },
        reason: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN
        },
        created_on: {
            type: Sequelize.DATE
        },
        updated_on: {
            type: Sequelize.DATE
        }

    })
    return ReportToAdmin;
}