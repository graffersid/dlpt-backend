const winston = require('winston');
myFormat = () => {
    return new Date(Date.now()).toUTCString()
}
exports.logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            level: 'debug',
            timestamp: function () {
                return (new Date()).toISOString();
            }
        }),
        new winston.transports.File({
            level: 'info',
            filename: './logs/info.log',
            json: true,
            format: winston.format.printf((info) => {
                let message = `${myFormat()} | ${info.level.toUpperCase()} | info.log | ${info.message} | `
                message = info.obj ? message + `data:${JSON.stringify(info.obj)}` : message
                return message
            })
        }),
        new winston.transports.File({
            level: 'error',
            filename: './logs/error.log',
            json: true,
            format: winston.format.printf((info) => {
                let message = `${myFormat()} | ${info.level.toUpperCase()} | error.log | ${info.message} | `
                message = info.obj ? message + `data:${JSON.stringify(info.obj)}` : message
                return message
            })
        })
    ]
});
