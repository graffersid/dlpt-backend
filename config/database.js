const Sequelize = require('sequelize')
const db = new Sequelize('show_trove1', 'postgres', '1234', {
    host: 'localhost',
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false // true by default
    },
    operatorsAliases: 0,
    logging: false

});

module.exports = db