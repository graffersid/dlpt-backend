PGDMP     ,    8                 y            show_trove1    12.3    12.3 �    Y           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            Z           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            [           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            \           1262    35105    show_trove1    DATABASE     �   CREATE DATABASE show_trove1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';
    DROP DATABASE show_trove1;
                postgres    false            �            1255    35106    timestamp_trigger()    FUNCTION     �   CREATE FUNCTION public.timestamp_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
   NEW.updated_on = NOW(); 
   RETURN NEW;
END;$$;
 *   DROP FUNCTION public.timestamp_trigger();
       public          postgres    false            �            1259    35107 
   categories    TABLE     G  CREATE TABLE public.categories (
    category_id integer NOT NULL,
    category_name character varying(65),
    parent_category_id integer DEFAULT 0,
    is_last_child boolean DEFAULT false,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.categories;
       public         heap    postgres    false            �            1259    35114    categories_category_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categories_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.categories_category_id_seq;
       public          postgres    false    202            ]           0    0    categories_category_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.categories_category_id_seq OWNED BY public.categories.category_id;
          public          postgres    false    203            �            1259    35116 	   followers    TABLE       CREATE TABLE public.followers (
    follower_id integer NOT NULL,
    following_user_id integer NOT NULL,
    followed_by_user_id integer NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.followers;
       public         heap    postgres    false            �            1259    35121    followers_follower_id_seq    SEQUENCE     �   CREATE SEQUENCE public.followers_follower_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.followers_follower_id_seq;
       public          postgres    false    204            ^           0    0    followers_follower_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.followers_follower_id_seq OWNED BY public.followers.follower_id;
          public          postgres    false    205            �            1259    35123    item_field_values    TABLE     H  CREATE TABLE public.item_field_values (
    item_field_value_id integer NOT NULL,
    item_id integer NOT NULL,
    item_field_id integer NOT NULL,
    item_field_value character varying(70),
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 %   DROP TABLE public.item_field_values;
       public         heap    postgres    false            �            1259    35128 )   item_field_values_item_field_value_id_seq    SEQUENCE     �   CREATE SEQUENCE public.item_field_values_item_field_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.item_field_values_item_field_value_id_seq;
       public          postgres    false    206            _           0    0 )   item_field_values_item_field_value_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.item_field_values_item_field_value_id_seq OWNED BY public.item_field_values.item_field_value_id;
          public          postgres    false    207            �            1259    35130    item_fields    TABLE     $  CREATE TABLE public.item_fields (
    item_field_id integer NOT NULL,
    category_id integer NOT NULL,
    item_field_name character varying(65) NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.item_fields;
       public         heap    postgres    false            �            1259    35135    item_fields_item_field_id_seq    SEQUENCE     �   CREATE SEQUENCE public.item_fields_item_field_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.item_fields_item_field_id_seq;
       public          postgres    false    208            `           0    0    item_fields_item_field_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.item_fields_item_field_id_seq OWNED BY public.item_fields.item_field_id;
          public          postgres    false    209            �            1259    35137    items    TABLE     e  CREATE TABLE public.items (
    item_id integer NOT NULL,
    current_user_id integer NOT NULL,
    category_id integer,
    item_name character varying(70),
    item_description text,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_active boolean DEFAULT true
);
    DROP TABLE public.items;
       public         heap    postgres    false            �            1259    35146    items_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.items_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.items_item_id_seq;
       public          postgres    false    210            a           0    0    items_item_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.items_item_id_seq OWNED BY public.items.item_id;
          public          postgres    false    211            �            1259    35148    likes    TABLE       CREATE TABLE public.likes (
    like_id integer NOT NULL,
    user_item_id integer,
    showroom_id integer,
    liked_by integer NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.likes;
       public         heap    postgres    false            �            1259    35153    likes_like_id_seq    SEQUENCE     �   CREATE SEQUENCE public.likes_like_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.likes_like_id_seq;
       public          postgres    false    212            b           0    0    likes_like_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.likes_like_id_seq OWNED BY public.likes.like_id;
          public          postgres    false    213            �            1259    35155    marketing_fields    TABLE     ,  CREATE TABLE public.marketing_fields (
    marketing_field_id integer NOT NULL,
    membership_id integer NOT NULL,
    marketing_field_name character varying(60),
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 $   DROP TABLE public.marketing_fields;
       public         heap    postgres    false            �            1259    35160 '   marketing_fields_marketing_field_id_seq    SEQUENCE     �   CREATE SEQUENCE public.marketing_fields_marketing_field_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.marketing_fields_marketing_field_id_seq;
       public          postgres    false    214            c           0    0 '   marketing_fields_marketing_field_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.marketing_fields_marketing_field_id_seq OWNED BY public.marketing_fields.marketing_field_id;
          public          postgres    false    215            �            1259    35162    media    TABLE     *  CREATE TABLE public.media (
    media_id integer NOT NULL,
    item_id integer,
    showroom_id integer,
    media_url text,
    file_type character varying(30),
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.media;
       public         heap    postgres    false            �            1259    35170    media_media_id_seq    SEQUENCE     �   CREATE SEQUENCE public.media_media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.media_media_id_seq;
       public          postgres    false    216            d           0    0    media_media_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.media_media_id_seq OWNED BY public.media.media_id;
          public          postgres    false    217            �            1259    35172    memberships    TABLE     �  CREATE TABLE public.memberships (
    membership_id integer NOT NULL,
    membership_name character varying(65),
    membership_plan_cost integer,
    membership_plan_duration smallint,
    pics_per_item_inventory smallint,
    videos_per_item_inventory smallint,
    total_item_inventory smallint,
    inventory_image_size_limit integer,
    inventory_video_size_limit integer,
    total_post_showroom smallint,
    showroom_image_size_limit integer,
    showroom_video_size_limit integer,
    ranking boolean,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_active boolean DEFAULT true,
    description text,
    show_interests boolean
);
    DROP TABLE public.memberships;
       public         heap    postgres    false            �            1259    35181    memberships_membership_id_seq    SEQUENCE     �   CREATE SEQUENCE public.memberships_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.memberships_membership_id_seq;
       public          postgres    false    218            e           0    0    memberships_membership_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.memberships_membership_id_seq OWNED BY public.memberships.membership_id;
          public          postgres    false    219            �            1259    35183    notifications    TABLE     �  CREATE TABLE public.notifications (
    notification_id integer NOT NULL,
    user_id integer,
    item_id integer,
    showroom_id integer,
    user_membership_id integer,
    notification_message character varying(70) NOT NULL,
    is_read boolean DEFAULT false,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    notification_type character varying(100) NOT NULL
);
 !   DROP TABLE public.notifications;
       public         heap    postgres    false            �            1259    35189 !   notifications_notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notifications_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.notifications_notification_id_seq;
       public          postgres    false    220            f           0    0 !   notifications_notification_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.notifications_notification_id_seq OWNED BY public.notifications.notification_id;
          public          postgres    false    221            �            1259    35191    ownership_transfers    TABLE     �  CREATE TABLE public.ownership_transfers (
    ownership_transfer_id integer NOT NULL,
    ownership_transfer_to_user integer NOT NULL,
    ownership_transfer_by_user integer NOT NULL,
    item_id integer NOT NULL,
    request_status_id integer NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 '   DROP TABLE public.ownership_transfers;
       public         heap    postgres    false            �            1259    35196 ,   ownership_transfer_ownership_transfer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ownership_transfer_ownership_transfer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.ownership_transfer_ownership_transfer_id_seq;
       public          postgres    false    222            g           0    0 ,   ownership_transfer_ownership_transfer_id_seq    SEQUENCE OWNED BY     ~   ALTER SEQUENCE public.ownership_transfer_ownership_transfer_id_seq OWNED BY public.ownership_transfers.ownership_transfer_id;
          public          postgres    false    223            �            1259    35198    ownership_transfer_statuses    TABLE       CREATE TABLE public.ownership_transfer_statuses (
    ownership_transfer_status_id integer NOT NULL,
    status_name character varying(20) NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 /   DROP TABLE public.ownership_transfer_statuses;
       public         heap    postgres    false            �            1259    35203 ;   ownership_transfer_status _ownership_transfer_status_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ownership_transfer_status _ownership_transfer_status_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 T   DROP SEQUENCE public."ownership_transfer_status _ownership_transfer_status_id_seq";
       public          postgres    false    224            h           0    0 ;   ownership_transfer_status _ownership_transfer_status_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."ownership_transfer_status _ownership_transfer_status_id_seq" OWNED BY public.ownership_transfer_statuses.ownership_transfer_status_id;
          public          postgres    false    225            �            1259    35205    report_to_admins    TABLE     �  CREATE TABLE public.report_to_admins (
    report_id integer NOT NULL,
    reported_user_id integer,
    reported_showroom_id integer,
    reported_item_id integer,
    reported_by_user integer,
    reason character varying(70),
    status boolean DEFAULT false,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 $   DROP TABLE public.report_to_admins;
       public         heap    postgres    false            �            1259    35211    report_to_admin_report_id_seq    SEQUENCE     �   CREATE SEQUENCE public.report_to_admin_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.report_to_admin_report_id_seq;
       public          postgres    false    226            i           0    0    report_to_admin_report_id_seq    SEQUENCE OWNED BY     `   ALTER SEQUENCE public.report_to_admin_report_id_seq OWNED BY public.report_to_admins.report_id;
          public          postgres    false    227            �            1259    35213 	   showrooms    TABLE     �  CREATE TABLE public.showrooms (
    showroom_id integer NOT NULL,
    user_id integer NOT NULL,
    showroom_title character varying(70),
    showroom_description text,
    total_views_count integer DEFAULT 0,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_pinned boolean DEFAULT false,
    is_active boolean DEFAULT true
);
    DROP TABLE public.showrooms;
       public         heap    postgres    false            �            1259    35224    showrooms_showroom_id_seq    SEQUENCE     �   CREATE SEQUENCE public.showrooms_showroom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.showrooms_showroom_id_seq;
       public          postgres    false    228            j           0    0    showrooms_showroom_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.showrooms_showroom_id_seq OWNED BY public.showrooms.showroom_id;
          public          postgres    false    229            �            1259    35226    user_field_values    TABLE     k  CREATE TABLE public.user_field_values (
    user_field_value_id integer NOT NULL,
    user_item_id integer NOT NULL,
    item_id integer NOT NULL,
    user_field_id integer NOT NULL,
    user_field_value character varying(70),
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 %   DROP TABLE public.user_field_values;
       public         heap    postgres    false            �            1259    35231 )   user_field_values_user_field_value_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_field_values_user_field_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.user_field_values_user_field_value_id_seq;
       public          postgres    false    230            k           0    0 )   user_field_values_user_field_value_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.user_field_values_user_field_value_id_seq OWNED BY public.user_field_values.user_field_value_id;
          public          postgres    false    231            �            1259    35233    user_fields    TABLE     $  CREATE TABLE public.user_fields (
    user_field_id integer NOT NULL,
    category_id integer NOT NULL,
    user_field_name character varying(65) NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.user_fields;
       public         heap    postgres    false            �            1259    35238    user_fields_user_field_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_fields_user_field_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.user_fields_user_field_id_seq;
       public          postgres    false    232            l           0    0    user_fields_user_field_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.user_fields_user_field_id_seq OWNED BY public.user_fields.user_field_id;
          public          postgres    false    233            �            1259    35240    user_interests    TABLE       CREATE TABLE public.user_interests (
    interest_id integer NOT NULL,
    user_id integer NOT NULL,
    category_id integer NOT NULL,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
 "   DROP TABLE public.user_interests;
       public         heap    postgres    false            �            1259    35245    user_interests_interest_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_interests_interest_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.user_interests_interest_id_seq;
       public          postgres    false    234            m           0    0    user_interests_interest_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.user_interests_interest_id_seq OWNED BY public.user_interests.interest_id;
          public          postgres    false    235            �            1259    35247 
   user_items    TABLE     �  CREATE TABLE public.user_items (
    user_item_id integer NOT NULL,
    user_id integer NOT NULL,
    item_id integer NOT NULL,
    total_views_count integer DEFAULT 0,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_active boolean DEFAULT true,
    is_past_item boolean DEFAULT false,
    is_private boolean DEFAULT false,
    is_item_completed boolean DEFAULT true NOT NULL
);
    DROP TABLE public.user_items;
       public         heap    postgres    false            �            1259    35257    user_items_user_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_items_user_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.user_items_user_item_id_seq;
       public          postgres    false    236            n           0    0    user_items_user_item_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.user_items_user_item_id_seq OWNED BY public.user_items.user_item_id;
          public          postgres    false    237            �            1259    35259    user_memberships    TABLE       CREATE TABLE public.user_memberships (
    user_membership_id integer NOT NULL,
    user_id integer NOT NULL,
    membership_id integer,
    membership_name character varying,
    membership_plan_cost integer,
    membership_plan_duration smallint,
    pics_per_item_inventory smallint,
    videos_per_item_inventory smallint,
    total_item_inventory smallint,
    inventory_image_size_limit integer,
    inventory_video_size_limit integer,
    total_post_showroom smallint,
    showroom_image_size_limit integer,
    showroom_video_size_limit integer,
    ranking boolean,
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_active boolean DEFAULT true,
    show_interests boolean DEFAULT true
);
 $   DROP TABLE public.user_memberships;
       public         heap    postgres    false            �            1259    35269 &   user_membership_user_membership_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_membership_user_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.user_membership_user_membership_id_seq;
       public          postgres    false    238            o           0    0 &   user_membership_user_membership_id_seq    SEQUENCE OWNED BY     r   ALTER SEQUENCE public.user_membership_user_membership_id_seq OWNED BY public.user_memberships.user_membership_id;
          public          postgres    false    239            �            1259    35271    users    TABLE     "  CREATE TABLE public.users (
    user_id integer NOT NULL,
    user_name character varying(65),
    name character varying(65),
    user_email character varying(65) NOT NULL,
    user_password character varying(90),
    user_image text,
    user_gender character varying(20),
    user_date_of_birth date,
    user_city character varying(65),
    user_country character varying(65),
    user_zip_code character varying(65),
    created_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    is_active boolean DEFAULT true,
    is_admin boolean DEFAULT false,
    access_token text,
    reset_password_token character varying(65),
    reset_password_expires timestamp with time zone,
    is_profile_completed boolean DEFAULT false
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    35282    users_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.users_user_id_seq;
       public          postgres    false    240            p           0    0    users_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;
          public          postgres    false    241            �
           2604    35284    categories category_id    DEFAULT     �   ALTER TABLE ONLY public.categories ALTER COLUMN category_id SET DEFAULT nextval('public.categories_category_id_seq'::regclass);
 E   ALTER TABLE public.categories ALTER COLUMN category_id DROP DEFAULT;
       public          postgres    false    203    202            �
           2604    35285    followers follower_id    DEFAULT     ~   ALTER TABLE ONLY public.followers ALTER COLUMN follower_id SET DEFAULT nextval('public.followers_follower_id_seq'::regclass);
 D   ALTER TABLE public.followers ALTER COLUMN follower_id DROP DEFAULT;
       public          postgres    false    205    204                       2604    35286 %   item_field_values item_field_value_id    DEFAULT     �   ALTER TABLE ONLY public.item_field_values ALTER COLUMN item_field_value_id SET DEFAULT nextval('public.item_field_values_item_field_value_id_seq'::regclass);
 T   ALTER TABLE public.item_field_values ALTER COLUMN item_field_value_id DROP DEFAULT;
       public          postgres    false    207    206                       2604    35287    item_fields item_field_id    DEFAULT     �   ALTER TABLE ONLY public.item_fields ALTER COLUMN item_field_id SET DEFAULT nextval('public.item_fields_item_field_id_seq'::regclass);
 H   ALTER TABLE public.item_fields ALTER COLUMN item_field_id DROP DEFAULT;
       public          postgres    false    209    208            	           2604    35288    items item_id    DEFAULT     n   ALTER TABLE ONLY public.items ALTER COLUMN item_id SET DEFAULT nextval('public.items_item_id_seq'::regclass);
 <   ALTER TABLE public.items ALTER COLUMN item_id DROP DEFAULT;
       public          postgres    false    211    210                       2604    35289    likes like_id    DEFAULT     n   ALTER TABLE ONLY public.likes ALTER COLUMN like_id SET DEFAULT nextval('public.likes_like_id_seq'::regclass);
 <   ALTER TABLE public.likes ALTER COLUMN like_id DROP DEFAULT;
       public          postgres    false    213    212                       2604    35290 #   marketing_fields marketing_field_id    DEFAULT     �   ALTER TABLE ONLY public.marketing_fields ALTER COLUMN marketing_field_id SET DEFAULT nextval('public.marketing_fields_marketing_field_id_seq'::regclass);
 R   ALTER TABLE public.marketing_fields ALTER COLUMN marketing_field_id DROP DEFAULT;
       public          postgres    false    215    214                       2604    35291    media media_id    DEFAULT     p   ALTER TABLE ONLY public.media ALTER COLUMN media_id SET DEFAULT nextval('public.media_media_id_seq'::regclass);
 =   ALTER TABLE public.media ALTER COLUMN media_id DROP DEFAULT;
       public          postgres    false    217    216                       2604    35292    memberships membership_id    DEFAULT     �   ALTER TABLE ONLY public.memberships ALTER COLUMN membership_id SET DEFAULT nextval('public.memberships_membership_id_seq'::regclass);
 H   ALTER TABLE public.memberships ALTER COLUMN membership_id DROP DEFAULT;
       public          postgres    false    219    218                       2604    35293    notifications notification_id    DEFAULT     �   ALTER TABLE ONLY public.notifications ALTER COLUMN notification_id SET DEFAULT nextval('public.notifications_notification_id_seq'::regclass);
 L   ALTER TABLE public.notifications ALTER COLUMN notification_id DROP DEFAULT;
       public          postgres    false    221    220                        2604    35294 8   ownership_transfer_statuses ownership_transfer_status_id    DEFAULT     �   ALTER TABLE ONLY public.ownership_transfer_statuses ALTER COLUMN ownership_transfer_status_id SET DEFAULT nextval('public."ownership_transfer_status _ownership_transfer_status_id_seq"'::regclass);
 g   ALTER TABLE public.ownership_transfer_statuses ALTER COLUMN ownership_transfer_status_id DROP DEFAULT;
       public          postgres    false    225    224                       2604    35295 )   ownership_transfers ownership_transfer_id    DEFAULT     �   ALTER TABLE ONLY public.ownership_transfers ALTER COLUMN ownership_transfer_id SET DEFAULT nextval('public.ownership_transfer_ownership_transfer_id_seq'::regclass);
 X   ALTER TABLE public.ownership_transfers ALTER COLUMN ownership_transfer_id DROP DEFAULT;
       public          postgres    false    223    222            $           2604    35296    report_to_admins report_id    DEFAULT     �   ALTER TABLE ONLY public.report_to_admins ALTER COLUMN report_id SET DEFAULT nextval('public.report_to_admin_report_id_seq'::regclass);
 I   ALTER TABLE public.report_to_admins ALTER COLUMN report_id DROP DEFAULT;
       public          postgres    false    227    226            *           2604    35297    showrooms showroom_id    DEFAULT     ~   ALTER TABLE ONLY public.showrooms ALTER COLUMN showroom_id SET DEFAULT nextval('public.showrooms_showroom_id_seq'::regclass);
 D   ALTER TABLE public.showrooms ALTER COLUMN showroom_id DROP DEFAULT;
       public          postgres    false    229    228            -           2604    35298 %   user_field_values user_field_value_id    DEFAULT     �   ALTER TABLE ONLY public.user_field_values ALTER COLUMN user_field_value_id SET DEFAULT nextval('public.user_field_values_user_field_value_id_seq'::regclass);
 T   ALTER TABLE public.user_field_values ALTER COLUMN user_field_value_id DROP DEFAULT;
       public          postgres    false    231    230            0           2604    35299    user_fields user_field_id    DEFAULT     �   ALTER TABLE ONLY public.user_fields ALTER COLUMN user_field_id SET DEFAULT nextval('public.user_fields_user_field_id_seq'::regclass);
 H   ALTER TABLE public.user_fields ALTER COLUMN user_field_id DROP DEFAULT;
       public          postgres    false    233    232            3           2604    35300    user_interests interest_id    DEFAULT     �   ALTER TABLE ONLY public.user_interests ALTER COLUMN interest_id SET DEFAULT nextval('public.user_interests_interest_id_seq'::regclass);
 I   ALTER TABLE public.user_interests ALTER COLUMN interest_id DROP DEFAULT;
       public          postgres    false    235    234            ;           2604    35301    user_items user_item_id    DEFAULT     �   ALTER TABLE ONLY public.user_items ALTER COLUMN user_item_id SET DEFAULT nextval('public.user_items_user_item_id_seq'::regclass);
 F   ALTER TABLE public.user_items ALTER COLUMN user_item_id DROP DEFAULT;
       public          postgres    false    237    236            @           2604    35302 #   user_memberships user_membership_id    DEFAULT     �   ALTER TABLE ONLY public.user_memberships ALTER COLUMN user_membership_id SET DEFAULT nextval('public.user_membership_user_membership_id_seq'::regclass);
 R   ALTER TABLE public.user_memberships ALTER COLUMN user_membership_id DROP DEFAULT;
       public          postgres    false    239    238            F           2604    35303    users user_id    DEFAULT     n   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    241    240            /          0    35107 
   categories 
   TABLE DATA           {   COPY public.categories (category_id, category_name, parent_category_id, is_last_child, created_on, updated_on) FROM stdin;
    public          postgres    false    202   k(      1          0    35116 	   followers 
   TABLE DATA           p   COPY public.followers (follower_id, following_user_id, followed_by_user_id, created_on, updated_on) FROM stdin;
    public          postgres    false    204   �(      3          0    35123    item_field_values 
   TABLE DATA           �   COPY public.item_field_values (item_field_value_id, item_id, item_field_id, item_field_value, created_on, updated_on) FROM stdin;
    public          postgres    false    206   �)      5          0    35130    item_fields 
   TABLE DATA           j   COPY public.item_fields (item_field_id, category_id, item_field_name, created_on, updated_on) FROM stdin;
    public          postgres    false    208   �)      7          0    35137    items 
   TABLE DATA           �   COPY public.items (item_id, current_user_id, category_id, item_name, item_description, created_on, updated_on, is_active) FROM stdin;
    public          postgres    false    210   �)      9          0    35148    likes 
   TABLE DATA           e   COPY public.likes (like_id, user_item_id, showroom_id, liked_by, created_on, updated_on) FROM stdin;
    public          postgres    false    212   �)      ;          0    35155    marketing_fields 
   TABLE DATA           {   COPY public.marketing_fields (marketing_field_id, membership_id, marketing_field_name, created_on, updated_on) FROM stdin;
    public          postgres    false    214   *      =          0    35162    media 
   TABLE DATA           m   COPY public.media (media_id, item_id, showroom_id, media_url, file_type, created_on, updated_on) FROM stdin;
    public          postgres    false    216   R+      ?          0    35172    memberships 
   TABLE DATA           �  COPY public.memberships (membership_id, membership_name, membership_plan_cost, membership_plan_duration, pics_per_item_inventory, videos_per_item_inventory, total_item_inventory, inventory_image_size_limit, inventory_video_size_limit, total_post_showroom, showroom_image_size_limit, showroom_video_size_limit, ranking, created_on, updated_on, is_active, description, show_interests) FROM stdin;
    public          postgres    false    218   o+      A          0    35183    notifications 
   TABLE DATA           �   COPY public.notifications (notification_id, user_id, item_id, showroom_id, user_membership_id, notification_message, is_read, created_on, updated_on, notification_type) FROM stdin;
    public          postgres    false    220   n,      E          0    35198    ownership_transfer_statuses 
   TABLE DATA           x   COPY public.ownership_transfer_statuses (ownership_transfer_status_id, status_name, created_on, updated_on) FROM stdin;
    public          postgres    false    224   �,      C          0    35191    ownership_transfers 
   TABLE DATA           �   COPY public.ownership_transfers (ownership_transfer_id, ownership_transfer_to_user, ownership_transfer_by_user, item_id, request_status_id, created_on, updated_on) FROM stdin;
    public          postgres    false    222   �,      G          0    35205    report_to_admins 
   TABLE DATA           �   COPY public.report_to_admins (report_id, reported_user_id, reported_showroom_id, reported_item_id, reported_by_user, reason, status, created_on, updated_on) FROM stdin;
    public          postgres    false    226   -      I          0    35213 	   showrooms 
   TABLE DATA           �   COPY public.showrooms (showroom_id, user_id, showroom_title, showroom_description, total_views_count, created_on, updated_on, is_pinned, is_active) FROM stdin;
    public          postgres    false    228   7-      K          0    35226    user_field_values 
   TABLE DATA           �   COPY public.user_field_values (user_field_value_id, user_item_id, item_id, user_field_id, user_field_value, created_on, updated_on) FROM stdin;
    public          postgres    false    230   T-      M          0    35233    user_fields 
   TABLE DATA           j   COPY public.user_fields (user_field_id, category_id, user_field_name, created_on, updated_on) FROM stdin;
    public          postgres    false    232   q-      O          0    35240    user_interests 
   TABLE DATA           c   COPY public.user_interests (interest_id, user_id, category_id, created_on, updated_on) FROM stdin;
    public          postgres    false    234   �-      Q          0    35247 
   user_items 
   TABLE DATA           �   COPY public.user_items (user_item_id, user_id, item_id, total_views_count, created_on, updated_on, is_active, is_past_item, is_private, is_item_completed) FROM stdin;
    public          postgres    false    236   �-      S          0    35259    user_memberships 
   TABLE DATA           �  COPY public.user_memberships (user_membership_id, user_id, membership_id, membership_name, membership_plan_cost, membership_plan_duration, pics_per_item_inventory, videos_per_item_inventory, total_item_inventory, inventory_image_size_limit, inventory_video_size_limit, total_post_showroom, showroom_image_size_limit, showroom_video_size_limit, ranking, created_on, updated_on, is_active, show_interests) FROM stdin;
    public          postgres    false    238   �-      U          0    35271    users 
   TABLE DATA           (  COPY public.users (user_id, user_name, name, user_email, user_password, user_image, user_gender, user_date_of_birth, user_city, user_country, user_zip_code, created_on, updated_on, is_active, is_admin, access_token, reset_password_token, reset_password_expires, is_profile_completed) FROM stdin;
    public          postgres    false    240   	/      q           0    0    categories_category_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.categories_category_id_seq', 74, true);
          public          postgres    false    203            r           0    0    followers_follower_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.followers_follower_id_seq', 64, true);
          public          postgres    false    205            s           0    0 )   item_field_values_item_field_value_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.item_field_values_item_field_value_id_seq', 217, true);
          public          postgres    false    207            t           0    0    item_fields_item_field_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.item_fields_item_field_id_seq', 113, true);
          public          postgres    false    209            u           0    0    items_item_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.items_item_id_seq', 110, true);
          public          postgres    false    211            v           0    0    likes_like_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.likes_like_id_seq', 126, true);
          public          postgres    false    213            w           0    0 '   marketing_fields_marketing_field_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.marketing_fields_marketing_field_id_seq', 100, true);
          public          postgres    false    215            x           0    0    media_media_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.media_media_id_seq', 236, true);
          public          postgres    false    217            y           0    0    memberships_membership_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.memberships_membership_id_seq', 31, true);
          public          postgres    false    219            z           0    0 !   notifications_notification_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.notifications_notification_id_seq', 6, true);
          public          postgres    false    221            {           0    0 ,   ownership_transfer_ownership_transfer_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.ownership_transfer_ownership_transfer_id_seq', 16, true);
          public          postgres    false    223            |           0    0 ;   ownership_transfer_status _ownership_transfer_status_id_seq    SEQUENCE SET     k   SELECT pg_catalog.setval('public."ownership_transfer_status _ownership_transfer_status_id_seq"', 3, true);
          public          postgres    false    225            }           0    0    report_to_admin_report_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.report_to_admin_report_id_seq', 44, true);
          public          postgres    false    227            ~           0    0    showrooms_showroom_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.showrooms_showroom_id_seq', 61, true);
          public          postgres    false    229                       0    0 )   user_field_values_user_field_value_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.user_field_values_user_field_value_id_seq', 171, true);
          public          postgres    false    231            �           0    0    user_fields_user_field_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.user_fields_user_field_id_seq', 75, true);
          public          postgres    false    233            �           0    0    user_interests_interest_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.user_interests_interest_id_seq', 43, true);
          public          postgres    false    235            �           0    0    user_items_user_item_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.user_items_user_item_id_seq', 112, true);
          public          postgres    false    237            �           0    0 &   user_membership_user_membership_id_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.user_membership_user_membership_id_seq', 122, true);
          public          postgres    false    239            �           0    0    users_user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.users_user_id_seq', 128, true);
          public          postgres    false    241            H           2606    35305 '   categories categories_category_name_key 
   CONSTRAINT     k   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_category_name_key UNIQUE (category_name);
 Q   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_category_name_key;
       public            postgres    false    202            J           2606    35307    categories categories_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (category_id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public            postgres    false    202            L           2606    35309    followers followers_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.followers
    ADD CONSTRAINT followers_pkey PRIMARY KEY (follower_id);
 B   ALTER TABLE ONLY public.followers DROP CONSTRAINT followers_pkey;
       public            postgres    false    204            N           2606    35311 (   item_field_values item_field_values_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.item_field_values
    ADD CONSTRAINT item_field_values_pkey PRIMARY KEY (item_field_value_id);
 R   ALTER TABLE ONLY public.item_field_values DROP CONSTRAINT item_field_values_pkey;
       public            postgres    false    206            P           2606    35313    item_fields item_fields_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.item_fields
    ADD CONSTRAINT item_fields_pkey PRIMARY KEY (item_field_id);
 F   ALTER TABLE ONLY public.item_fields DROP CONSTRAINT item_fields_pkey;
       public            postgres    false    208            R           2606    35315    items items_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (item_id);
 :   ALTER TABLE ONLY public.items DROP CONSTRAINT items_pkey;
       public            postgres    false    210            T           2606    35317    likes likes_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.likes
    ADD CONSTRAINT likes_pkey PRIMARY KEY (like_id);
 :   ALTER TABLE ONLY public.likes DROP CONSTRAINT likes_pkey;
       public            postgres    false    212            V           2606    35319 &   marketing_fields marketing_fields_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.marketing_fields
    ADD CONSTRAINT marketing_fields_pkey PRIMARY KEY (marketing_field_id);
 P   ALTER TABLE ONLY public.marketing_fields DROP CONSTRAINT marketing_fields_pkey;
       public            postgres    false    214            X           2606    35321    media media_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_pkey PRIMARY KEY (media_id);
 :   ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
       public            postgres    false    216            Z           2606    35323 +   memberships memberships_membership_name_key 
   CONSTRAINT     q   ALTER TABLE ONLY public.memberships
    ADD CONSTRAINT memberships_membership_name_key UNIQUE (membership_name);
 U   ALTER TABLE ONLY public.memberships DROP CONSTRAINT memberships_membership_name_key;
       public            postgres    false    218            \           2606    35325    memberships memberships_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.memberships
    ADD CONSTRAINT memberships_pkey PRIMARY KEY (membership_id);
 F   ALTER TABLE ONLY public.memberships DROP CONSTRAINT memberships_pkey;
       public            postgres    false    218            ^           2606    35327     notifications notifications_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notification_id);
 J   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_pkey;
       public            postgres    false    220            `           2606    35329 +   ownership_transfers ownership_transfer_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.ownership_transfers
    ADD CONSTRAINT ownership_transfer_pkey PRIMARY KEY (ownership_transfer_id);
 U   ALTER TABLE ONLY public.ownership_transfers DROP CONSTRAINT ownership_transfer_pkey;
       public            postgres    false    222            b           2606    35331 :   ownership_transfer_statuses ownership_transfer_status_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.ownership_transfer_statuses
    ADD CONSTRAINT ownership_transfer_status_pkey PRIMARY KEY (ownership_transfer_status_id);
 d   ALTER TABLE ONLY public.ownership_transfer_statuses DROP CONSTRAINT ownership_transfer_status_pkey;
       public            postgres    false    224            d           2606    35333 %   report_to_admins report_to_admin_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.report_to_admins
    ADD CONSTRAINT report_to_admin_pkey PRIMARY KEY (report_id);
 O   ALTER TABLE ONLY public.report_to_admins DROP CONSTRAINT report_to_admin_pkey;
       public            postgres    false    226            f           2606    35335    showrooms showrooms_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.showrooms
    ADD CONSTRAINT showrooms_pkey PRIMARY KEY (showroom_id);
 B   ALTER TABLE ONLY public.showrooms DROP CONSTRAINT showrooms_pkey;
       public            postgres    false    228            h           2606    35337 (   user_field_values user_field_values_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.user_field_values
    ADD CONSTRAINT user_field_values_pkey PRIMARY KEY (user_field_value_id);
 R   ALTER TABLE ONLY public.user_field_values DROP CONSTRAINT user_field_values_pkey;
       public            postgres    false    230            j           2606    35339    user_fields user_fields_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.user_fields
    ADD CONSTRAINT user_fields_pkey PRIMARY KEY (user_field_id);
 F   ALTER TABLE ONLY public.user_fields DROP CONSTRAINT user_fields_pkey;
       public            postgres    false    232            l           2606    35341 "   user_interests user_interests_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.user_interests
    ADD CONSTRAINT user_interests_pkey PRIMARY KEY (interest_id);
 L   ALTER TABLE ONLY public.user_interests DROP CONSTRAINT user_interests_pkey;
       public            postgres    false    234            n           2606    35343    user_items user_items_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.user_items
    ADD CONSTRAINT user_items_pkey PRIMARY KEY (user_item_id);
 D   ALTER TABLE ONLY public.user_items DROP CONSTRAINT user_items_pkey;
       public            postgres    false    236            p           2606    35345 %   user_memberships user_membership_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY public.user_memberships
    ADD CONSTRAINT user_membership_pkey PRIMARY KEY (user_membership_id);
 O   ALTER TABLE ONLY public.user_memberships DROP CONSTRAINT user_membership_pkey;
       public            postgres    false    238            r           2606    35347    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    240            t           2606    35349    users users_user_email_key 
   CONSTRAINT     [   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_email_key UNIQUE (user_email);
 D   ALTER TABLE ONLY public.users DROP CONSTRAINT users_user_email_key;
       public            postgres    false    240            v           2606    35351    users users_user_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_name_key UNIQUE (user_name);
 C   ALTER TABLE ONLY public.users DROP CONSTRAINT users_user_name_key;
       public            postgres    false    240            x           2606    35353    users users_user_name_key1 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_name_key1 UNIQUE (user_name);
 D   ALTER TABLE ONLY public.users DROP CONSTRAINT users_user_name_key1;
       public            postgres    false    240            �           2620    35354    categories category_trigger    TRIGGER     }   CREATE TRIGGER category_trigger BEFORE UPDATE ON public.categories FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 4   DROP TRIGGER category_trigger ON public.categories;
       public          postgres    false    202    242            �           2620    35355    followers follower_trigger    TRIGGER     |   CREATE TRIGGER follower_trigger BEFORE UPDATE ON public.followers FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 3   DROP TRIGGER follower_trigger ON public.followers;
       public          postgres    false    242    204            �           2620    35356    item_fields item_field_trigger    TRIGGER     �   CREATE TRIGGER item_field_trigger BEFORE UPDATE ON public.item_fields FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 7   DROP TRIGGER item_field_trigger ON public.item_fields;
       public          postgres    false    208    242            �           2620    35357 *   item_field_values item_field_value_trigger    TRIGGER     �   CREATE TRIGGER item_field_value_trigger BEFORE UPDATE ON public.item_field_values FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 C   DROP TRIGGER item_field_value_trigger ON public.item_field_values;
       public          postgres    false    206    242            �           2620    35358    items item_trigger    TRIGGER     t   CREATE TRIGGER item_trigger BEFORE UPDATE ON public.items FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 +   DROP TRIGGER item_trigger ON public.items;
       public          postgres    false    210    242            �           2620    35359    likes like_trigger    TRIGGER     t   CREATE TRIGGER like_trigger BEFORE UPDATE ON public.likes FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 +   DROP TRIGGER like_trigger ON public.likes;
       public          postgres    false    242    212            �           2620    35360 (   marketing_fields marketing_field_trigger    TRIGGER     �   CREATE TRIGGER marketing_field_trigger BEFORE UPDATE ON public.marketing_fields FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 A   DROP TRIGGER marketing_field_trigger ON public.marketing_fields;
       public          postgres    false    242    214            �           2620    35361    media media_trigger    TRIGGER     u   CREATE TRIGGER media_trigger BEFORE UPDATE ON public.media FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 ,   DROP TRIGGER media_trigger ON public.media;
       public          postgres    false    242    216            �           2620    35362    memberships membership_trigger    TRIGGER     �   CREATE TRIGGER membership_trigger BEFORE UPDATE ON public.memberships FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 7   DROP TRIGGER membership_trigger ON public.memberships;
       public          postgres    false    218    242            �           2620    35363 "   notifications notification_trigger    TRIGGER     �   CREATE TRIGGER notification_trigger BEFORE UPDATE ON public.notifications FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 ;   DROP TRIGGER notification_trigger ON public.notifications;
       public          postgres    false    220    242            �           2620    35364 =   ownership_transfer_statuses ownership_transfer_status_trigger    TRIGGER     �   CREATE TRIGGER ownership_transfer_status_trigger BEFORE UPDATE ON public.ownership_transfer_statuses FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 V   DROP TRIGGER ownership_transfer_status_trigger ON public.ownership_transfer_statuses;
       public          postgres    false    224    242            �           2620    35365 .   ownership_transfers ownership_transfer_trigger    TRIGGER     �   CREATE TRIGGER ownership_transfer_trigger BEFORE UPDATE ON public.ownership_transfers FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 G   DROP TRIGGER ownership_transfer_trigger ON public.ownership_transfers;
       public          postgres    false    222    242            �           2620    35366 (   report_to_admins report_to_admin_trigger    TRIGGER     �   CREATE TRIGGER report_to_admin_trigger BEFORE UPDATE ON public.report_to_admins FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 A   DROP TRIGGER report_to_admin_trigger ON public.report_to_admins;
       public          postgres    false    226    242            �           2620    35367    showrooms showroom_trigger    TRIGGER     |   CREATE TRIGGER showroom_trigger BEFORE UPDATE ON public.showrooms FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 3   DROP TRIGGER showroom_trigger ON public.showrooms;
       public          postgres    false    242    228            �           2620    35368    user_fields user_field_trigger    TRIGGER     �   CREATE TRIGGER user_field_trigger BEFORE UPDATE ON public.user_fields FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 7   DROP TRIGGER user_field_trigger ON public.user_fields;
       public          postgres    false    242    232            �           2620    35369 *   user_field_values user_field_value_trigger    TRIGGER     �   CREATE TRIGGER user_field_value_trigger BEFORE UPDATE ON public.user_field_values FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 C   DROP TRIGGER user_field_value_trigger ON public.user_field_values;
       public          postgres    false    230    242            �           2620    35370 $   user_interests user_interest_trigger    TRIGGER     �   CREATE TRIGGER user_interest_trigger BEFORE UPDATE ON public.user_interests FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 =   DROP TRIGGER user_interest_trigger ON public.user_interests;
       public          postgres    false    234    242            �           2620    35371    user_items user_item_trigger    TRIGGER     ~   CREATE TRIGGER user_item_trigger BEFORE UPDATE ON public.user_items FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 5   DROP TRIGGER user_item_trigger ON public.user_items;
       public          postgres    false    242    236            �           2620    35372 (   user_memberships user_membership_trigger    TRIGGER     �   CREATE TRIGGER user_membership_trigger BEFORE UPDATE ON public.user_memberships FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 A   DROP TRIGGER user_membership_trigger ON public.user_memberships;
       public          postgres    false    242    238            �           2620    35373    users user_trigger    TRIGGER     t   CREATE TRIGGER user_trigger BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.timestamp_trigger();
 +   DROP TRIGGER user_trigger ON public.users;
       public          postgres    false    240    242            y           2606    35374 ,   followers followers_followed_by_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.followers
    ADD CONSTRAINT followers_followed_by_user_id_fkey FOREIGN KEY (followed_by_user_id) REFERENCES public.users(user_id);
 V   ALTER TABLE ONLY public.followers DROP CONSTRAINT followers_followed_by_user_id_fkey;
       public          postgres    false    2930    240    204            z           2606    35379 *   followers followers_following_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.followers
    ADD CONSTRAINT followers_following_user_id_fkey FOREIGN KEY (following_user_id) REFERENCES public.users(user_id);
 T   ALTER TABLE ONLY public.followers DROP CONSTRAINT followers_following_user_id_fkey;
       public          postgres    false    204    2930    240            {           2606    35384 6   item_field_values item_field_values_item_field_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.item_field_values
    ADD CONSTRAINT item_field_values_item_field_id_fkey FOREIGN KEY (item_field_id) REFERENCES public.item_fields(item_field_id);
 `   ALTER TABLE ONLY public.item_field_values DROP CONSTRAINT item_field_values_item_field_id_fkey;
       public          postgres    false    206    2896    208            |           2606    35389 0   item_field_values item_field_values_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.item_field_values
    ADD CONSTRAINT item_field_values_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);
 Z   ALTER TABLE ONLY public.item_field_values DROP CONSTRAINT item_field_values_item_id_fkey;
       public          postgres    false    206    210    2898            }           2606    35394 (   item_fields item_fields_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.item_fields
    ADD CONSTRAINT item_fields_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(category_id);
 R   ALTER TABLE ONLY public.item_fields DROP CONSTRAINT item_fields_category_id_fkey;
       public          postgres    false    208    202    2890            ~           2606    35399    items items_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(category_id) NOT VALID;
 F   ALTER TABLE ONLY public.items DROP CONSTRAINT items_category_id_fkey;
       public          postgres    false    202    2890    210                       2606    35404     items items_current_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_current_user_id_fkey FOREIGN KEY (current_user_id) REFERENCES public.users(user_id) NOT VALID;
 J   ALTER TABLE ONLY public.items DROP CONSTRAINT items_current_user_id_fkey;
       public          postgres    false    240    2930    210            �           2606    35409    likes likes_liked_by_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.likes
    ADD CONSTRAINT likes_liked_by_fkey FOREIGN KEY (liked_by) REFERENCES public.users(user_id) NOT VALID;
 C   ALTER TABLE ONLY public.likes DROP CONSTRAINT likes_liked_by_fkey;
       public          postgres    false    240    212    2930            �           2606    35414    likes likes_showroom_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.likes
    ADD CONSTRAINT likes_showroom_id_fkey FOREIGN KEY (showroom_id) REFERENCES public.showrooms(showroom_id) ON DELETE CASCADE NOT VALID;
 F   ALTER TABLE ONLY public.likes DROP CONSTRAINT likes_showroom_id_fkey;
       public          postgres    false    228    2918    212            �           2606    35419    likes likes_user_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.likes
    ADD CONSTRAINT likes_user_item_id_fkey FOREIGN KEY (user_item_id) REFERENCES public.user_items(user_item_id) NOT VALID;
 G   ALTER TABLE ONLY public.likes DROP CONSTRAINT likes_user_item_id_fkey;
       public          postgres    false    212    2926    236            �           2606    35424 4   marketing_fields marketing_fields_membership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.marketing_fields
    ADD CONSTRAINT marketing_fields_membership_id_fkey FOREIGN KEY (membership_id) REFERENCES public.memberships(membership_id);
 ^   ALTER TABLE ONLY public.marketing_fields DROP CONSTRAINT marketing_fields_membership_id_fkey;
       public          postgres    false    214    2908    218            �           2606    35429    media media_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id) NOT VALID;
 B   ALTER TABLE ONLY public.media DROP CONSTRAINT media_item_id_fkey;
       public          postgres    false    210    216    2898            �           2606    35434    media media_showroom_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.media
    ADD CONSTRAINT media_showroom_id_fkey FOREIGN KEY (showroom_id) REFERENCES public.showrooms(showroom_id) ON DELETE CASCADE NOT VALID;
 F   ALTER TABLE ONLY public.media DROP CONSTRAINT media_showroom_id_fkey;
       public          postgres    false    228    2918    216            �           2606    35439 (   notifications notifications_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id) NOT VALID;
 R   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_item_id_fkey;
       public          postgres    false    2898    210    220            �           2606    35444 ,   notifications notifications_showroom_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_showroom_id_fkey FOREIGN KEY (showroom_id) REFERENCES public.showrooms(showroom_id) ON DELETE CASCADE NOT VALID;
 V   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_showroom_id_fkey;
       public          postgres    false    228    2918    220            �           2606    35449 (   notifications notifications_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id) NOT VALID;
 R   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_user_id_fkey;
       public          postgres    false    240    2930    220            �           2606    35454 3   notifications notifications_user_membership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_user_membership_id_fkey FOREIGN KEY (user_membership_id) REFERENCES public.user_memberships(user_membership_id) NOT VALID;
 ]   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_user_membership_id_fkey;
       public          postgres    false    220    2928    238            �           2606    35459 3   ownership_transfers ownership_transfer_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ownership_transfers
    ADD CONSTRAINT ownership_transfer_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);
 ]   ALTER TABLE ONLY public.ownership_transfers DROP CONSTRAINT ownership_transfer_item_id_fkey;
       public          postgres    false    222    2898    210            �           2606    35464 F   ownership_transfers ownership_transfer_ownership_transfer_by_user_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ownership_transfers
    ADD CONSTRAINT ownership_transfer_ownership_transfer_by_user_fkey FOREIGN KEY (ownership_transfer_by_user) REFERENCES public.users(user_id);
 p   ALTER TABLE ONLY public.ownership_transfers DROP CONSTRAINT ownership_transfer_ownership_transfer_by_user_fkey;
       public          postgres    false    2930    240    222            �           2606    35469 F   ownership_transfers ownership_transfer_ownership_transfer_to_user_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ownership_transfers
    ADD CONSTRAINT ownership_transfer_ownership_transfer_to_user_fkey FOREIGN KEY (ownership_transfer_to_user) REFERENCES public.users(user_id);
 p   ALTER TABLE ONLY public.ownership_transfers DROP CONSTRAINT ownership_transfer_ownership_transfer_to_user_fkey;
       public          postgres    false    240    2930    222            �           2606    35474 =   ownership_transfers ownership_transfer_request_status_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ownership_transfers
    ADD CONSTRAINT ownership_transfer_request_status_id_fkey FOREIGN KEY (request_status_id) REFERENCES public.ownership_transfer_statuses(ownership_transfer_status_id);
 g   ALTER TABLE ONLY public.ownership_transfers DROP CONSTRAINT ownership_transfer_request_status_id_fkey;
       public          postgres    false    222    224    2914            �           2606    35479 7   report_to_admins report_to_admins_reported_by_user_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.report_to_admins
    ADD CONSTRAINT report_to_admins_reported_by_user_fkey FOREIGN KEY (reported_by_user) REFERENCES public.users(user_id) NOT VALID;
 a   ALTER TABLE ONLY public.report_to_admins DROP CONSTRAINT report_to_admins_reported_by_user_fkey;
       public          postgres    false    2930    240    226            �           2606    35484 7   report_to_admins report_to_admins_reported_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.report_to_admins
    ADD CONSTRAINT report_to_admins_reported_item_id_fkey FOREIGN KEY (reported_item_id) REFERENCES public.items(item_id) NOT VALID;
 a   ALTER TABLE ONLY public.report_to_admins DROP CONSTRAINT report_to_admins_reported_item_id_fkey;
       public          postgres    false    226    210    2898            �           2606    35489 ;   report_to_admins report_to_admins_reported_showroom_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.report_to_admins
    ADD CONSTRAINT report_to_admins_reported_showroom_id_fkey FOREIGN KEY (reported_showroom_id) REFERENCES public.showrooms(showroom_id) ON DELETE CASCADE NOT VALID;
 e   ALTER TABLE ONLY public.report_to_admins DROP CONSTRAINT report_to_admins_reported_showroom_id_fkey;
       public          postgres    false    2918    226    228            �           2606    35494 7   report_to_admins report_to_admins_reported_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.report_to_admins
    ADD CONSTRAINT report_to_admins_reported_user_id_fkey FOREIGN KEY (reported_user_id) REFERENCES public.users(user_id) NOT VALID;
 a   ALTER TABLE ONLY public.report_to_admins DROP CONSTRAINT report_to_admins_reported_user_id_fkey;
       public          postgres    false    2930    226    240            �           2606    35499     showrooms showrooms_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.showrooms
    ADD CONSTRAINT showrooms_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);
 J   ALTER TABLE ONLY public.showrooms DROP CONSTRAINT showrooms_user_id_fkey;
       public          postgres    false    240    2930    228            �           2606    35504 0   user_field_values user_field_values_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_field_values
    ADD CONSTRAINT user_field_values_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id) NOT VALID;
 Z   ALTER TABLE ONLY public.user_field_values DROP CONSTRAINT user_field_values_item_id_fkey;
       public          postgres    false    210    2898    230            �           2606    35509 6   user_field_values user_field_values_user_field_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_field_values
    ADD CONSTRAINT user_field_values_user_field_id_fkey FOREIGN KEY (user_field_id) REFERENCES public.user_fields(user_field_id) NOT VALID;
 `   ALTER TABLE ONLY public.user_field_values DROP CONSTRAINT user_field_values_user_field_id_fkey;
       public          postgres    false    230    232    2922            �           2606    35514 5   user_field_values user_field_values_user_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_field_values
    ADD CONSTRAINT user_field_values_user_item_id_fkey FOREIGN KEY (user_item_id) REFERENCES public.user_items(user_item_id) NOT VALID;
 _   ALTER TABLE ONLY public.user_field_values DROP CONSTRAINT user_field_values_user_item_id_fkey;
       public          postgres    false    230    2926    236            �           2606    35519 (   user_fields user_fields_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_fields
    ADD CONSTRAINT user_fields_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(category_id);
 R   ALTER TABLE ONLY public.user_fields DROP CONSTRAINT user_fields_category_id_fkey;
       public          postgres    false    232    2890    202            �           2606    35524 .   user_interests user_interests_category_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_interests
    ADD CONSTRAINT user_interests_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(category_id);
 X   ALTER TABLE ONLY public.user_interests DROP CONSTRAINT user_interests_category_id_fkey;
       public          postgres    false    234    2890    202            �           2606    35529 *   user_interests user_interests_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_interests
    ADD CONSTRAINT user_interests_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);
 T   ALTER TABLE ONLY public.user_interests DROP CONSTRAINT user_interests_user_id_fkey;
       public          postgres    false    2930    234    240            �           2606    35534 "   user_items user_items_item_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_items
    ADD CONSTRAINT user_items_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id) NOT VALID;
 L   ALTER TABLE ONLY public.user_items DROP CONSTRAINT user_items_item_id_fkey;
       public          postgres    false    236    2898    210            �           2606    35539 "   user_items user_items_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_items
    ADD CONSTRAINT user_items_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id) NOT VALID;
 L   ALTER TABLE ONLY public.user_items DROP CONSTRAINT user_items_user_id_fkey;
       public          postgres    false    2930    236    240            �           2606    35544 3   user_memberships user_membership_membership_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_memberships
    ADD CONSTRAINT user_membership_membership_id_fkey FOREIGN KEY (membership_id) REFERENCES public.memberships(membership_id);
 ]   ALTER TABLE ONLY public.user_memberships DROP CONSTRAINT user_membership_membership_id_fkey;
       public          postgres    false    238    218    2908            �           2606    35549 -   user_memberships user_membership_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_memberships
    ADD CONSTRAINT user_membership_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);
 W   ALTER TABLE ONLY public.user_memberships DROP CONSTRAINT user_membership_user_id_fkey;
       public          postgres    false    240    2930    238            /      x������ � �      1     x���;�!Cc8E�n��w���9օ6�`48�^�3�Q����r˸e]�[l�<�]��Hl���:���g8��>� y��P P�3@ ��6b�oG��ſj�g��F� uԈ�_@�:b���?2��o�g� �e�7��H�5b,�MԈ��@�. Q��y ��H�w��=/-ÿw���Y#�"��F�q>$����D2�ǙHF�'�р8זR��$�*ũc�z������j��o�g���@����Zr�,J      3      x������ � �      5      x������ � �      7      x������ � �      9      x������ � �      ;   )  x����N�0е�٣�����[ذZ�T��3�v�YE�&��H�s���e?�_���:F����$8a�IT��|��g@&���������,�2B�^3������尾Χq9ϟTC�ԋ&C���u4x����������d#I�P�C�TK�'ҒL����v�&����z�n'Iv����V�ԭ(ɦ�M�5sv�ka
�H%|ێ(��-R�ִ;�����8�X�F��`���DG�Q��l�b`�U�^���O)�@6�Y�fȉk=��b�f9�W��z�g��k�      =      x������ � �      ?   �   x����n� D��Wpo���í�ыӂ��Ƒ�R�����zH#���JO���<�v`4��4m^����)r����`�rlk�<�s"�bk1�=P��XN�~�:�S�H�2-uy�ג�~���]�y</9Ͳ���(��8��q\ʛ��Ii=~���u$�	d��iH!�s�qLyN	����
�j��@5w2��n���eA�v׈ܭ�.~�����b)(�Ć�-�W%���"�*      A      x������ � �      E   b   x���1� Й���@>-�,.�8��?�*qy�\�7�`X$�y�SA(����~�Yjգ���ѻ�Rȡ�= �9u����8�9��� ͎�n#�/�      C      x������ � �      G      x������ � �      I      x������ � �      K      x������ � �      M      x������ � �      O      x������ � �      Q      x������ � �      S   1  x����j�0E��Wd_bt%˯o�-�e����f�CJ(!J��=�J
���|>�9���d��3u�cb���tD��§�
<�V�d���C誛J�&C�Gh]�H|�wue/���e':`/H�#����:��V�ח��7ӷ��*F�C���%\U|,���(O�#QWV/�A�"-�Y���pQ>�fj|�['j�$�k�����ES����V�����zRA�hD�bk��A�3�f����!6�;P��=/�b[�{ǷZ�W���t���d�k y��!��g<�a��?q      U   T  x��X�r��}�����Bt��x���\��VkhB����D�%7�F���S'��$2���2��Z{�- �1|7�pz�}�z���7�}ҷ���.�p ��`��izf#O�3���UO9��=�K�Zn����d��!� ��l،O<�A �u���#,���i�I��b�&#Ȣ����NwK/E��:kPj�b��� ������5(7!�1�� ��킔�������;q������$J0�z[�c�]��2K/�:Z���̝����]���}i�"�!�_c�Pa1><q/�jn���Y�H]M8��ZZ����n���']�pV�8Pw6����H ���)��V���ɟI�����M�q�=?��M�I����h�[�y����%+��t^�����?^�O�ɉMNb����	뀫A��sM�X
 IoxZ?����{���h�(�����.W;*P/J� k�����Dx1��r���r��ukvqE�������� c�Y��2�3=72k�[�n��	�$�,+�crt�z�`��sox,���o��Y�	�04�Y��"r��>��4脥P��q�4j6F����ۼ�dB���
����:�ˀ�!1��pB�g�i��e�f�F�H/�A��Bc����YP���&�X��yߑ���Uo��?����8�\ϬW|}�KW1"  ����K�{I���ү��@����@s
��������U Ti'�"o�c�,
X����E��^ S��=�&`�a��;8�����oD��?uS�q^��������P������kO�(� �j�Y�i��5+�Z�,�� �j�����p��L뜯#�Q#�M��0<���{� �9x�&��|m���ٶr���l3OɆw'���c�=�v��֑�9��ī�����bK�UXWM��;Y�R_x
��Tط���
q	�xv	m�&�+lD��rLR^��vL,ˌ��yM~1���������֡A�{�hS�\����&�X	C$���`^��-u�������5A�=A{��%R���xu�bG;r��b%iq?�1���������:D��#�[& ��$q�e|�L�2��ƅ���ش����E�Jl�4k��л���JV�_�U���N0ݎ�^�Z"�f�R���t���ؒ/���ު�X~9��,`�������y�$o"侪�� q�w֊�C�B��ev�N�D
���E�}p���`������e}߲����uvW?r�$tq�ںZ��c3Ki��W��$u@�r��,>�#��~���li�#U�����s�`�O�V�<����� ��Ȩ���?5�9���/�0�ĦkP��yf@hI���-p��H�H�̘��Wh<bi�e�n��-��ڗ��a���.)��!��E��Z����ِ���T4?j`� 
����E��A%Y@�4�I@���C`��$f�����6��[�߀�F#a:�;��0�a�ړ��aKh��]|�r�e�n�=��;rƧ����!X�`Y<=^Ax+ߗ�n��RMfd�@Adb+�l3�9	�Z�Z�u/��#�y_,��w��狣�F�b�=6�	K���?[3c�}���X�i��T����LH* ��!�?(❗	�S��)8�&�v�_"	��5J�Ȩr�M�Z몉��E�)ků�`&�E��?m�2�,�[��?D����4����i"�]��Üd��0�5��{�x'D��_�3�U���<�Z��[u�����cfi�j�+2[x�#u�w3�B+5}��φ������9�S��>xN7���~V*<, ++�DיF��i׫M<�LbfF��h�����O3&Ը��R���w;�vڞ�o���|1h���9��r]�g�2ss'X̟�ނ<�mWi����Lw��T���;lT�agU𔚖En`&/�����ŵ��w�6^�"�).�_�� �����e�s��k��$��~"Z��l�o���m�Sk�r�_!��]�s��h��ims[�m<&a���{�����=Ƃ��O���sl8-c��")[����i�ض)��>�����j� �y��qZ��Oފ���}�xc�	a;R     